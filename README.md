# MOBILES RecSys

The **MOBILES RecSys** is an autonomous recommendation system designed to enhance the user experience of international students using the **MOBILES** app. By providing personalized recommendations based on user behavior, interactions, and preferences, the system leverages advanced data analytics and geolocated data. **MOBILES RecSys** integrates seamlessly with the MOBILES app, which allows users to explore the city, collect geolocated data, and share annotations, enriching their urban experiences. This system uses behavioral indicators to generate personalized recommendations that are tailored to each user's unique interactions with the city.

## Features

- **Personalized Recommendations**: The system generates recommendations based on user interactions and behavioral indicators, tailoring suggestions for each user profile.
  
- **Geolocated Annotations**: Users can create detailed, geolocated annotations, enhancing their urban exploration experience and sharing personal insights about their environment.

- **Behavioral Indicators**: These indicators are calculated in four main stages: Onboarding and Integration, Active Usage Support,  Quality Improvement, and Urban Discovery.
  
- **Containerized Architecture**: MOBILES RecSys is deployed using **Docker** and **Docker Compose**, which ensures optimal resource management and simplifies deployment.

- **Data Storage and Processing**: 
  - **MariaDB** is used for user data storage.
  - **kTBS** records digital traces of user interactions.
  - **Logstash** processes user data and interaction logs, which are then indexed into **Elasticsearch**.

- **Dashboard**: A user-friendly interface, developed in **Ruby**, allows users to configure and automate system actions such as data retrieval, indicator calculation, recommendation generation, and notification sending.

## Installation

### 1. Modify Configuration Files

Update the `.env` file with your environment-specific variables:

```plaintext
ELASTIC_VERSION=8.11.3
ELASTIC_PASSWORD='mobilespassword'
LOGSTASH_INTERNAL_PASSWORD='mobilespassword'
MONITORING_INTERNAL_PASSWORD=''
BEATS_SYSTEM_PASSWORD=''
MYSQL_USER='mobiles'
MYSQL_PASSWORD='mobilespassword'
MYSQL_ROOT_PASSWORD='mobilespassword'
MYSQL_DB='mobiles'
KTBS_USER='mobiles'
KTBS_PASSWORD='mobilespassword'
KTBS_URL='https://KTBS_URL/tracesmobiles/@obsels'
```

### 2. Modify Configuration for Recommendation System

Update the `rs/config/settings.py` file to reflect the correct database and Elasticsearch configurations, ensuring that the system connects to the right services.

### 3. Start the Setup and Run Containers

First, run the setup to initialize the necessary services:

```bash
docker-compose up setup
```

Then, launch the application in detached mode:

```bash
docker-compose up -d
```

### 4. Access the Dashboard

Once the application is running, open your web browser and go to [http://localhost:8080](http://localhost:8080) to access the **MOBILES RecSys** dashboard. The dashboard provides a user interface to manage system settings, automate actions like data retrieval, indicator calculation, recommendation generation, and notification dispatch.

## Project Structure

```
setup/:                        Contains setup scripts and service initialization configurations.
elasticsearch/:                Configuration for the Elasticsearch service.
logstash/:                     Configuration and pipelines for Logstash.
db/:                           Database initialization scripts.
rs/:                           Source code for the recommendation system and Flask-based web application.
config/:                       Configuration files for application settings.
```

## Technical Details

### Setup

- **Initialization**: The `setup/` directory includes scripts for initializing the necessary services (Elasticsearch, Logstash, MariaDB, kTBS). These scripts configure and set up the environment, ensuring that all required services are in place before starting the application.

### Elasticsearch

- **Search and Indexing**: **Elasticsearch** is used for storing, searching, and indexing user interaction data. It enables efficient retrieval of logs and other data necessary for generating accurate and personalized recommendations.

### Logstash

- **Data Processing**: **Logstash** plays a crucial role in the system's ETL (Extract, Transform, Load) pipeline. It processes interaction logs, user data, and other inputs, transforming them into a format suitable for storage in **Elasticsearch**. It extracts data from **MariaDB** and **kTBS**, processes it, and sends it to Elasticsearch for indexing.

### MariaDB

- **Database Storage**: All user data, including collected logs and interactions, is stored in a **MariaDB** database. MariaDB serves as the persistent storage solution for user information and application interactions, ensuring that data is available for analysis and recommendation generation.

### kTBS

- **Trace Management**: The **kTBS** instance records digital traces of user interactions within the app. These traces are crucial for understanding user behavior and usage patterns. **Logstash** processes the data from **kTBS**, transforming it into actionable insights that contribute to the recommendation system.

### Recommendation System (rs)

- **Python and Flask**: The recommendation system is implemented in **Python** and utilizes **Flask** to create a web application. This system calculates behavioral indicators based on user interactions and generates personalized recommendations based on these indicators.

- **Configuration**: The `rs/config/settings.py` file contains all the necessary configurations for connecting to external services such as the database and Elasticsearch. It also defines settings for recommendation algorithms and data processing.

### Dashboard

- **User Interface**: The **MOBILES RecSys** dashboard is developed in **Ruby** and provides a rich, interactive interface for managing system configurations. Users can configure the behavior of the system, automate tasks such as data retrieval, manage recommendation generation, and send notifications.

## Running the System

To run the system, follow these steps:

1. **Modify Configuration Files**: Ensure that the `.env` file and `rs/config/settings.py` are correctly updated with environment variables and configurations.

2. **Run Setup**: Initialize necessary services with the following command:

   ```bash
   docker-compose up setup
   ```

3. **Start the Application**: After setup, start the application in detached mode:

   ```bash
   docker-compose up -d
   ```

4. **Access the Dashboard**: Open your browser and navigate to [http://localhost:8080](http://localhost:8080) to access the **MOBILES RecSys** dashboard, where you can manage the system and monitor the recommendation processes.


## Contribution
Contributions are welcome! Please reach out to us if you are interested in contributing to this project.

## License
This project is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Acknowledgements
This project is supported by the ANR under grant number ANR-20-CE38-0009. We thank the members of the partner laboratories for their contributions to the project's conception and ideas.


### Contact
- Madjid Sadallah
- Marie Lefevre

---

If you have any questions or need assistance, please contact the project members via their respective emails.
