-- Crée la base de données si elle n'existe pas
CREATE DATABASE IF NOT EXISTS mobiles;

-- Utilise la base de données mobiles
USE mobiles;

-- Crée la table scheduled_task
CREATE TABLE IF NOT EXISTS scheduled_task (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    action VARCHAR(50) NOT NULL,
    scheduled_time DATETIME NOT NULL,
    creation_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status VARCHAR(20) NOT NULL DEFAULT 'Not Executed',
    last_run DATETIME NULL,
    recurrence VARCHAR(50) NULL
);
