require 'set'

if event.get('[obsels]')
  event.get('[obsels]').each { |k, v|
    if k.is_a?(String)
      new_key = k.gsub(/^m:|@/, '')
      if new_key == 'shared' && v.is_a?(String)
        event.set(new_key, v.to_s.downcase == 'public')
      elsif ['dateModif', 'dateDisplay'].include?(new_key) && v.is_a?(String)
        v = v.include?(':') ? v : v + ':00'
        v = v.include?(' ') ? v.gsub(' ', 'T') + ':00' : v
        event.set(new_key, v)
      elsif ['coordinates', 'prev_coordinates'].include?(new_key)
        json_data = v.is_a?(String) ? JSON.parse(v) : v
        if json_data['lat'] && json_data['lon']
          lat = json_data['lat'].to_s.gsub(',', '.').to_f
          lon = json_data['lon'].to_s.gsub(',', '.').to_f
          # Check if lat and lon are within the valid range
          if lat.between?(-90, 90) && lon.between?(-180, 180)
            event.set(new_key, {'lat' => lat, 'lon' => lon})
          else
            event.remove(new_key)
          end
        else
          event.remove(new_key)
        end
      elsif new_key == 'position' && v.is_a?(String)
        lon, lat = v.split('-').map { |x| x.split(':').last.gsub(',', '.').to_f }
        # Check if lat and lon are within the valid range
        if lat.between?(-90, 90) && lon.between?(-180, 180)
          event.set(new_key, {'lat' => lat, 'lon' => lon})
        else
          event.remove(new_key)
        end
      elsif new_key == 'tourBody' && v.is_a?(String)
        coordinates_array = JSON.parse(v)
        if coordinates_array.length >= 4 && coordinates_array.uniq.length >= 4
          linestring_wkt = 'LINESTRING (' + coordinates_array.each_slice(2).map { |lon, lat| lon.to_s + ' ' + lat.to_s }.join(', ') + ')'
          event.set('coordinates_list', linestring_wkt)
        end
      elsif new_key == 'icons' && v.is_a?(String)
        event.set(new_key, v.split(',').map(&:strip))
      elsif new_key == 'tags' && v.is_a?(String)
        event.set(new_key, v.split(' ').map(&:strip))
      else
        event.set(new_key, v)
      end
    end
  }
end



# Convert 'userId' to string
if event.get('userId')
  event.set('userId', event.get('userId').to_s)
end

# Parse 'begin' and 'end' as UNIX_MS dates
['begin', 'end'].each do |field|
  if event.get(field)
    event.set(field, Time.at(event.get(field).to_i / 1000.0).utc)
  end
end

# Parse 'dateModif' as an ISO8601 date
if event.get('dateModif')
  event.set('dateModif', Time.parse(event.get('dateModif').to_s))
end

# Parse 'dateDisplay' as an ISO8601 date
if event.get('dateDisplay')
  event.set('dateDisplay', Time.parse(event.get('dateDisplay').to_s))
end
