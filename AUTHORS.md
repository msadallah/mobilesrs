# AUTHORS

This file lists all the individuals who have contributed to the development of the MOBILES RecSys project.

## Principal Investigators
- **Madjid Sadallah** 
- **Marie Lefevre** 

## Institutional Support
- **University Lyon 1 / LIRIS, CNRS** - Lyon Research Center for Images and Intelligent Information Systems

## Funding
This project is supported by the ANR (Agence Nationale de la Recherche, France) under grant number ANR-20-CE38-0009.