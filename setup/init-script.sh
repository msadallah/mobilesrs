#!/bin/bash
# Change file permissions
chmod +x /usr/share/elasticsearch/config/init-script.sh
# Wait for Elasticsearch to start
until curl -s http://elasticsearch:9200/_cat/health -o /dev/null; do
    sleep 1
done
# Set up the mapping
curl -XPUT es:9200/_template/template_1 -H 'Content-Type: application/json' -d'
{
  "index_patterns": ["your_index_pattern"],
  "mappings": {
    "properties": {
      "m:position": { "type": "geo_point" },
      "m:coordinates": { "type": "geo_point" },
      "m:prev_coordinates": { "type": "geo_point" }
      // Add other field mappings as needed
    }
  }
}'