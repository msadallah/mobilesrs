from collections import defaultdict
import json
import pandas as pd
from geopy.distance import great_circle
from sklearn.cluster import DBSCAN

from modules.db_operations import fetch_annotation_comments, fetch_annotations, fetch_annotations_around_location, fetch_new_annotations, fetch_tours, fetch_user_annotations, fetch_user_profile, fetch_user_tours
from modules.es_operations import fetch_annotation_popularity, fetch_interaction_count_for_content, fetch_user_filters
from modules.metrics.urban_discovery_m import calculate_harv_distance
from web_app.models import UserProfile

import json
import pandas as pd
from geopy.distance import geodesic

class Suggester:
    
    def __init__(self, user_id):
        self.user_id = user_id
        self.user_profile = self.fetch_user_profile(user_id)
        self.user_annotations = self.fetch_user_annotations(user_id)
        self.others_annotations = self.fetch_others_annotations(user_id)
        self.user_tours = self.fetch_user_tours(user_id)
        self.others_tours = self.fetch_others_tours(user_id)

        self.user_filters = self.analyze_filters(user_id)

        self.user_locations = self.process_user_locations()

        self.unexplored_zones = self.get_unexplored_zones()

    # Méthodes pour récupérer les données nécessaires
    def fetch_user_profile(self, user_id):
        """Récupérer le profil de l'utilisateur."""
        return fetch_user_profile(user_id)

    def fetch_user_annotations(self, user_id):
        """Récupérer toutes les annotations de l'utilisateur."""
        return fetch_user_annotations(user_id)
    
    def process_user_locations(self):
        # Assurez-vous que la colonne 'coords' existe dans le DataFrame
        if 'coords' in self.user_annotations.columns:
            # Récupérer toutes les coordonnées valides (non nulles)
            self.user_locations = self.user_annotations['coords'].dropna().tolist()
        else:
            print("La colonne 'coords' n'existe pas dans self.user_annotations.")
            self.user_locations = []

    
    def fetch_others_annotations(self, user_id):
        """Récupérer toutes les annotations des autres utilisateurs."""
        return fetch_annotations(field_filters_to_exclude={'user': [user_id]})

    def fetch_user_tours(self, user_id):
        """Récupérer tous les parcours de l'utilisateur."""
        return fetch_user_tours(user_id)
    
    def fetch_others_tours(self, user_id):
        """Récupérer toutes les annotations des autres utilisateurs."""
        return fetch_tours(field_filters_to_exclude={'user': [user_id]})


    def analyze_filters(self, user_id, since_date=None):
        """
        Calcule les Préférences de Filtres (PF) pour un utilisateur donné en identifiant les motifs spécifiques récurrents.

        Args:
            user_id (int): L'ID de l'utilisateur.
            since_date (str, optional): Date à partir de laquelle récupérer les filtres.

        Returns:
            dict: Dictionnaire contenant la PF et les motifs trouvés.
        """
        # Récupérer les filtres appliqués par l'utilisateur
        filters = fetch_user_filters(user_id, since_date)

        # Initialiser les compteurs pour chaque motif de filtre
        institution_counts = defaultdict(int)
        nationality_counts = defaultdict(int)
        icon_counts = defaultdict(int)
        emoticon_counts = defaultdict(int)
        tag_counts = defaultdict(int)
        date_range_counts = defaultdict(int)

        # Parcourir chaque filtre appliqué
        for _, row in filters.iterrows():
            # Compter les institutions
            institution = row.get('institution')
            if isinstance(institution, list):
                for inst in institution:
                    institution_counts[inst] += 1

            # Compter les nationalités
            nationality = row.get('nationality')
            if isinstance(nationality, list):
                for nat in nationality:
                    nationality_counts[nat] += 1

            # Compter les icônes
            icons = row.get('icons', [])
            if isinstance(icons, list):
                for icon in icons:
                    icon_counts[icon] += 1

            # Compter les émoticônes
            emoticons = row.get('emoticon', [])
            if isinstance(emoticons, list):      
                for emoticon in emoticons:
                    emoticon_counts[emoticon] += 1

            # Compter les tags
            tags = row.get('tags', [])
            if isinstance(tags, list):           
                for tag in tags:
                    tag_counts[tag] += 1

            # Compter les plages de dates
            begin_date = row.get('beginDate')
            end_date = row.get('endDate')
            if begin_date and end_date:
                date_range = f"{begin_date} - {end_date}"
                date_range_counts[date_range] += 1

        # Identifier les motifs les plus fréquents
        top_institutions = [k for k, v in institution_counts.items() if v > 1]
        top_nationalities = [k for k, v in nationality_counts.items() if v > 1]
        top_icons = [k for k, v in icon_counts.items() if v > 1]
        top_emoticons = [k for k, v in emoticon_counts.items() if v > 1]
        top_tags = [k for k, v in tag_counts.items() if v > 1]
        top_date_ranges = [k for k, v in date_range_counts.items() if v > 1]

        # Consolider les motifs
        motifs = {
            'institutions': top_institutions,
            'nationalities': top_nationalities,
            'icons': top_icons,
            'emoticons': top_emoticons,
            'tags': top_tags,
            'date_ranges': top_date_ranges
        }

        # Calculer la fréquence totale et la moyenne
        total_filters = (
            sum(institution_counts.values()) +
            sum(nationality_counts.values()) +
            sum(icon_counts.values()) +
            sum(emoticon_counts.values()) +
            sum(tag_counts.values()) +
            sum(date_range_counts.values())
        )

        total_motifs = len(top_institutions) + len(top_nationalities) + len(top_icons) + len(top_emoticons) + len(top_tags) + len(top_date_ranges)

        PF = total_filters / total_motifs if total_motifs > 0 else 0

        # Convertir les motifs en chaîne JSON
        motifs_json = json.dumps(motifs)
        
        return {
            'PF': PF,
            'motifs': motifs_json
        }
    

    import json

    def extract_interests_data(self, prefix):
        """Extrait et analyse les intérêts de l'utilisateur en fonction du préfixe donné."""
        profile = self.user_profile.iloc[0] if isinstance(self.user_profile, pd.DataFrame) else self.user_profile

        
        interests = {
            'user_id': profile.get('user_id', 0),
            'items_count': profile.get(f'{prefix}_items_count', 0),
            'avg_volume_lexical': profile.get(f'{prefix}_avg_volume_lexical', 0.0),
            'icon_usage': json.loads(profile.get(f'{prefix}_icon_type_ratio', '{}')),
            'tag_usage': json.loads(profile.get(f'{prefix}_tag_type_ratio', '{}')),
            'emoticon_usage': json.loads(profile.get(f'{prefix}_emoticon_top3', '[]')),
            'image_presence_percentage': profile.get(f'{prefix}_image_presence_percentage', 0.0),
            'place_types': json.loads(profile.get(f'{prefix}_place_type_usage', '{}')),
            'experience_frequency': json.loads(profile.get(f'{prefix}_experience_frequency', '{}')),
            'covered_zones': profile.get(f'{prefix}_covered_zones', '[]'),
            # 'covered_zones': json.loads(profile.get(f'{prefix}_covered_zones', '[]')),
        }
        
        # Si le préfixe est PCoA, ajouter des motifs récurrents
        if prefix == 'pcoa':
            interests['recurrent_motifs'] = profile.get(f'{prefix}_recurrent_motifs', '[]')
            # interests['recurrent_motifs'] = json.loads(profile.get(f'{prefix}_recurrent_motifs', '[]'))

        return interests

    
    def analyze_interests(self):
        """Analyse les intérêts de l'utilisateur pour PCA et PCoA."""
        pca_interests = self.extract_interests_data('pca')
        pcoa_interests = self.extract_interests_data('pcoa')
        
        return {
            'pca': pca_interests,
            'pcoa': pcoa_interests
        }

    
    def parse_json(self, json_string):
        """Parser une chaîne JSON en un objet Python, retourner un dictionnaire vide en cas d'erreur."""
        if isinstance(json_string, dict):
            return json_string
        elif isinstance(json_string, str):
            try:
                return json.loads(json_string) if json_string else {}
            except json.JSONDecodeError:
                return {}
        return {}

    def compute_annotation_score(self, annotation):
        """
        Calcule le score de qualité d'une annotation en fonction de plusieurs critères pondérés.

        Paramètres:
        - annotation (DataFrame): Une annotation individuelle sous forme de DataFrame.

        Retourne:
        - float: Le score de qualité de l'annotation.
        """
        score = 0
        total_weight = 0

        # Critères de qualité avec leurs poids respectifs
        criteria_weights = {
            'message_length': 0.3,  # Longueur du message
            'graphical': 0.2,       # Utilisation d'images (1 ou 0)
            'num_tags': 0.2,        # Nombre de tags
            'num_icons': 0.2,       # Nombre d'icônes
            'diversity_of_icons': 0.1  # Diversité des icônes (pourcentage ou ratio)
        }

        # Calculer le score pour chaque critère
        for criterion, weight in criteria_weights.items():
            if criterion == 'message_length':
                score += min(len(annotation['messages']) / 100, 1) * weight  # Normalisation de la longueur
            elif criterion == 'graphical':
                score += annotation['graphical'] * weight
            elif criterion == 'num_tags':
                score += min(len(annotation['tags']), 5) / 5 * weight  # Normalisation sur 5 tags max
            elif criterion == 'num_icons':
                score += min(annotation['total_icons_count'], 10) / 10 * weight  # Normalisation sur 10 icônes max
            elif criterion == 'diversity_of_icons':
                score += annotation['diversity_types_icons_used'] * weight  # Supposé normalisé à 1

            total_weight += weight

        # Retourner le score normalisé
        return score / total_weight if total_weight > 0 else 0


    def calculate_distance(self, loc1, loc2):
        """Calculer la distance entre deux coordonnées géographiques."""
        return geodesic((loc1['lat'], loc1['lon']), (loc2['lat'], loc2['lon'])).kilometers

    def correct_coordinates(self, lat, lon):
        """
        Corrige les coordonnées pour s'assurer qu'elles sont dans les plages valides.
        
        :param lat: Latitude à corriger
        :param lon: Longitude à corriger
        :return: Tuple contenant la latitude et la longitude corrigées
        """
        lat = max(min(lat, 90), -90)
        lon = max(min(lon, 180), -180)
        return lat, lon
    def calculate_min_distance_to_tour(self, user_location, tour_coordinates):
        """
        Calcule la distance minimale entre la localisation de l'utilisateur et un parcours.

        :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
        :param tour_coordinates: Liste de tuples représentant les points du parcours [(lat, lon), (lat, lon), ...].
        :return: Distance minimale en kilomètres.
        """
        min_distance = float('inf')
        if not isinstance(user_location, dict) or 'lat' not in user_location or 'lon' not in user_location:
            return None

        for coord in tour_coordinates:
            point = self.correct_coordinates(coord[1], coord[0])
            distance = geodesic((user_location['lat'], user_location['lon']), point).kilometers
            if distance < min_distance:
                min_distance = distance

        return min_distance

    # Méthodes de suggestion
    def suggest_rich_annotations(self):
        """Suggérer des annotations riches (avec beaucoup de détails)."""
        rich_annotations = []
        for _, annotation in self.others_annotations.iterrows():
            if self.compute_annotation_score(annotation) >= .5:
                rich_annotations.append(annotation)
        return rich_annotations

    def suggest_popular_annotations(self):
        """
        Suggérer les 5 annotations les plus populaires en fonction du nombre d'interactions.

        :return: Liste des 5 annotations les plus populaires
        """
        annotations_with_interactions = []

        # Parcourir les annotations
        for _, annotation in self.others_annotations.iterrows():
            content_id = annotation.get('id')

            # Récupérer le nombre d'interactions pour chaque annotation
            interaction_count = fetch_interaction_count_for_content(content_id=content_id)

            # Ajouter l'annotation et le nombre d'interactions à une liste
            annotations_with_interactions.append({
                'annotation': annotation,
                'interaction_count': interaction_count
            })

        # Trier les annotations par nombre d'interactions décroissant
        annotations_with_interactions.sort(key=lambda x: x['interaction_count'], reverse=True)

        # Renvoyer les 5 annotations avec le plus d'interactions
        return [item['annotation'] for item in annotations_with_interactions[:5]]

    def suggest_active_annotations(self):
        """
        Suggérer les 5 annotations ayant reçu le plus grand nombre de commentaires.

        :return: Liste des 5 annotations les plus actives.
        """
        annotations_with_comments = []

        # Parcourir les annotations
        for _, annotation in self.others_annotations.iterrows():
            annotation_id = annotation.get('content_id')

            # Récupérer les commentaires pour chaque annotation
            comments = fetch_annotation_comments(annotation_id)

            # Ajouter l'annotation et le nombre de commentaires à une liste
            annotations_with_comments.append({
                'annotation': annotation,
                'comment_count': len(comments)
            })

        # Trier les annotations par nombre de commentaires décroissant
        annotations_with_comments.sort(key=lambda x: x['comment_count'], reverse=True)

        # Renvoyer les 5 annotations avec le plus de commentaires
        return [item['annotation'] for item in annotations_with_comments[:5]]


    def suggest_new_annotations(self, days_threshold=7):
        """Suggérer des annotations récentes."""
        new_annotations = []
        current_date = pd.Timestamp.now()
        for _, annotation in self.others_annotations.iterrows():
            annotation_date = pd.to_datetime(annotation.get('date', current_date))
            if (current_date - annotation_date).days <= days_threshold:
                new_annotations.append(annotation)
        return new_annotations

    def suggest_nearby_annotations(self):
        """Suggérer des annotations à proximité des lieux annotés par l'utilisateur."""
        nearby_annotations = []
        for _, annotation in self.others_annotations.iterrows():
            coords = self.parse_json(annotation['coords'])
            annotation_location = {"lat": coords.get('lat'), "lon": coords.get('lon')}
            lat_valid = annotation_location['lat'] is not None and pd.notna(annotation_location['lat'])
            lon_valid = annotation_location['lon'] is not None and pd.notna(annotation_location['lon'])
            if lat_valid and lon_valid:
                if any(self.calculate_distance(annotation_location, user_loc) < 0.5 for user_loc in self.user_locations):
                    nearby_annotations.append(annotation)
        return nearby_annotations
    
    def suggest_annotations_based_on_interests(self, top_n=3):
        """Suggérer les annotations les plus pertinentes basées sur les intérêts de l'utilisateur."""
        interests = self.analyze_interests()
        interests = interests['pca']  # Accéder aux intérêts PCA
        annotations_with_matches = []
        
        for _, annotation in self.others_annotations.iterrows():
            matches = 0
            
            # Vérification des correspondances de tags
            tag_match_score = 0
            total_tags_count = annotation['positive_tags_count'] + annotation['negative_tags_count'] + annotation['aesthetic_tags_count'] + annotation['social_tags_count']
            if total_tags_count > 0:
                for tag_type, interest_tag_count in interests['tag_usage'].items():
                    annotation_tag_count = annotation[f'{tag_type}_tags_count'] if f'{tag_type}_tags_count' in annotation else 0
                    # Si le nombre de tags de ce type dans l'annotation est supérieur à 0, il y a une correspondance.
                    tag_match_score += (annotation_tag_count / total_tags_count) * interest_tag_count
            
            if tag_match_score > 0:
                matches += tag_match_score
            
            # Vérification des correspondances d'icônes
            icon_match_score = 0
            total_icons_count = annotation['total_icons_count'] if 'total_icons_count' in annotation else 0
            if total_icons_count > 0:
                for icon_type, interest_icon_count in interests['icon_usage'].items():
                    annotation_icon_count = annotation[f'{icon_type}_icons_count'] if f'{icon_type}_icons_count' in annotation else 0
                    icon_match_score += (annotation_icon_count / total_icons_count) * interest_icon_count
            
            if icon_match_score > 0:
                matches += icon_match_score
            
            # Vérification des types de lieux
            place_types = self.parse_json(annotation['placeType'])  # Assurez-vous que cette méthode renvoie une liste
            if any(place in interests['place_types'] for place in place_types):
                matches += 1
            
            # Comparaison du volume lexical
            if annotation['volume_lexical'] >= interests['avg_volume_lexical']:
                matches += 1
            
            # Comparaison de la présence d'images
            if annotation['graphical'] >= interests['image_presence_percentage']:
                matches += 1
            
            # Ajout des annotations avec correspondances
            if matches > 0:
                annotations_with_matches.append((annotation, matches))
        
        # Trier les annotations en fonction du nombre de correspondances
        annotations_with_matches.sort(key=lambda x: x[1], reverse=True)
        
        # Sélectionner les meilleures annotations
        top_annotations = [annotation for annotation, _ in annotations_with_matches[:top_n]]
        
        return top_annotations



    def correct_coordinates(self, lat, lon):
        """
        Corrige les coordonnées pour s'assurer qu'elles sont dans les plages valides.
        
        :param lat: Latitude à corriger
        :param lon: Longitude à corriger
        :return: Tuple contenant la latitude et la longitude corrigées
        """
        lat = max(min(lat, 90), -90)
        lon = max(min(lon, 180), -180)
        return lat, lon

    def calculate_min_distance_to_tour(self, user_location, tour_coordinates):
        """
        Calcule la distance minimale entre la localisation de l'utilisateur et un parcours.

        :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
        :param tour_coordinates: Liste de tuples représentant les points du parcours [(lat, lon), (lat, lon), ...].
        :return: Distance minimale en kilomètres.
        """
        min_distance = float('inf')
        if not isinstance(user_location, dict) or 'lat' not in user_location or 'lon' not in user_location:
            return None

        for coord in tour_coordinates:
            point = self.correct_coordinates(coord[1], coord[0])
            distance = geodesic((user_location['lat'], user_location['lon']), point).kilometers
            if distance < min_distance:
                min_distance = distance

        return min_distance

    def is_within_proximity(self, tour_coordinates, user_location, proximity_threshold_km=5):
        """
        Vérifie si l'une des coordonnées du tour est à proximité de la localisation de l'utilisateur.

        :param tour_coordinates: Liste de tuples représentant les points du parcours [(lat, lon), (lat, lon), ...].
        :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
        :param proximity_threshold_km: Distance maximale en kilomètres pour considérer le tour comme proche.
        :return: True si le tour est à proximité de l'utilisateur, False sinon.
        """
        min_distance = self.calculate_min_distance_to_tour(user_location, tour_coordinates)
        return min_distance is not None and min_distance <= proximity_threshold_km

    def is_opportunity_for_user(self, tour):
        """
        Détermine si un tour représente une opportunité pour l'utilisateur.
        
        :param tour: Dictionnaire représentant un tour, avec une clé 'body' contenant les coordonnées du parcours.
        :return: True si le tour représente une opportunité pour l'utilisateur, False sinon.
        """
        try:        
            body = json.loads(tour['body'])
        except json.JSONDecodeError as e:
            print(f"Erreur lors de la conversion du corps du tour en liste de coordonnées: {e}")
            return False

        tour_coordinates = [(body[i+1], body[i]) for i in range(0, len(body), 2)]

        for interest in self.user_interests:
            if self.is_within_proximity(tour_coordinates, interest['location']):
                return True

        return False

    def suggest_nearby_tours(self, proximity_threshold_km=5):
        """
        Suggère des tours qui sont à proximité des localisations de l'utilisateur.

        :param user_locations: Liste de dictionnaires contenant des coordonnées de localisation de l'utilisateur.
                              Ex: [{'lat': 48.8566, 'lon': 2.3522}, ...]
        :param all_tours: Liste de dictionnaires représentant les tours, chaque dictionnaire ayant une clé 'body' 
                          qui contient les coordonnées du parcours sous forme JSON.
        :param proximity_threshold_km: Distance maximale en kilomètres pour considérer un tour comme proche.
        :return: Liste des tours suggérés à proximité.
        """
        suggested_tours = []

        for user_location in self.user_locations:
            for tour in self.self.others_tours:
                # Vérifier si le tour représente une opportunité pour l'utilisateur
                if self.is_opportunity_for_user(tour):
                    # Vérifier la proximité avec l'utilisateur
                    if self.is_within_proximity(json.loads(tour['body']), user_location, proximity_threshold_km):
                        suggested_tours.append(tour)

        return suggested_tours
    
    def suggest_annotation_by_filter(self, top_n=3):
        """
        Suggérer les annotations qui correspondent le mieux aux filtres récurrents de l'utilisateur.

        Args:
            top_n (int): Nombre d'annotations à suggérer.

        Returns:
            list: Liste des annotations suggérées.
        """
        # Charger les motifs et les comptes des filtres
        filters = json.loads(self.user_filters['motifs'])
        annotations_with_scores = []

        # Parcourir chaque annotation dans les annotations disponibles
        for _, annotation in self.others_annotations.iterrows():
            score = 0
            
            # Comparer les institutions, seulement celles qui sont récurrentes
            if any(inst in filters['institutions'] for inst in annotation.get('institution', [])):
                score += 1
            
            # Comparer les nationalités
            if any(nat in filters['nationalities'] for nat in annotation.get('nationality', [])):
                score += 1
            
            # Comparer les icônes
            if any(icon in filters['icons'] for icon in annotation.get('icons', [])):
                score += 1
            
            # Comparer les émoticônes
            if any(emoticon in filters['emoticons'] for emoticon in annotation.get('emoticon', [])):
                score += 1
            
            # Comparer les tags
            if any(tag in filters['tags'] for tag in annotation.get('tags', [])):
                score += 1
            
            # Comparer les plages de dates (facultatif, si pertinent)
            begin_date = annotation.get('beginDate')
            end_date = annotation.get('endDate')
            date_range = f"{begin_date} - {end_date}" if begin_date and end_date else None
            if date_range in filters['date_ranges']:
                score += 1

            # Ajouter l'annotation et son score si elle a un score positif
            if score > 0:
                annotations_with_scores.append((annotation, score))

        # Trier les annotations par score, décroissant
        annotations_with_scores.sort(key=lambda x: x[1], reverse=True)
        
        # Récupérer les top_n annotations
        top_annotations = [annotation for annotation, _ in annotations_with_scores[:top_n]]
        
        return top_annotations
    
    def suggest_least_rich_annotation(self):
        """
        Suggérer l'annotation la moins riche de l'utilisateur, basée sur le score calculé par compute_annotation_score.
        
        Returns:
            dict: L'annotation la moins riche ou None s'il n'y a pas d'annotations.
        """
        if not self.user_annotations:
            return None
        
        least_rich_annotation = None
        min_score = float('inf')
        
        # Parcourir les annotations et calculer leur score
        for annotation in self.user_annotations:
            score = self.compute_annotation_score(annotation)
            
            # Rechercher l'annotation avec le score le plus bas
            if score < min_score:
                min_score = score
                least_rich_annotation = annotation

        return least_rich_annotation

    def get_unexplored_zones(self, min_distance=0.5):
        """
        Identifie les zones inexplorées autour des annotations existantes.
        
        :param user_id: ID de l'utilisateur.
        :param annotations: Liste des coordonnées (lat, lon) des annotations de l'utilisateur.
        :param min_distance: Distance minimale en kilomètres pour considérer une zone comme inexplorée.
        :return: Liste de coordonnées représentant des zones inexplorées.
        """
        if self.user_annotations.empty:
            return []
        
        coordinates = []
        # coordinates = [(json.loads(ann['coords'])['lat'], json.loads(ann['coords'])['lon']) for ann in annotations]
        
        for index, row in self.user_annotations.iterrows():
            coords = json.loads(row['coords'])
            lat = coords['lat']
            lon = coords['lon']
            coordinates.append((lat, lon))

        
        # Clustering des annotations pour définir les zones explorées
        clustering = DBSCAN(eps=min_distance, min_samples=5, metric=calculate_harv_distance).fit(coordinates)
        
        # Obtenir les clusters de zones explorées
        explored_zones = [coordinates[i] for i in range(len(coordinates)) if clustering.labels_[i] != -1]
        
        # Déterminer les zones inexplorées (en dehors des clusters)
        unexplored_zones = []
        
        for coord in coordinates:
            is_far = all(calculate_harv_distance(coord, explored) > min_distance for explored in explored_zones)
            if is_far:
                unexplored_zones.append(coord)
        
        return unexplored_zones



    def suggeste_uexplored_zone_annotation(self):
            """
            Récupère l'ID d'une annotation qui incite l'utilisateur à élargir sa couverture géographique.

            :param user_id: ID de l'utilisateur.
            :return: ID d'une annotation pertinente ou None.
            """
            #

            if not self.unexplored_zones:
                return None

            # Pour chaque zone inexplorée, récupérer les annotations autour
            for zone in self.unexplored_zones:
                location = {'lat': zone[0], 'lon': zone[1]}
                nearby_annotations = fetch_annotations_around_location(location, radius_km=5)
                
                if nearby_annotations:
                    # Retourner l'ID de la première annotation trouvée à proximité
                    return nearby_annotations[0][0]  # ID de l'annotation

            return None



    def execute_strategy(self, strategy):
        """Exécuter la stratégie de suggestion demandée et retourner les résultats."""
        if strategy == 'rich_annotations':
            return self.suggest_rich_annotations()
        elif strategy == 'popular_annotations':
            return self.suggest_popular_annotations()
        elif strategy == 'new_annotations':
            return self.suggest_new_annotations()
        elif strategy == 'nearby_annotations':
            user_locations = [annotation['coords'] for annotation in self.user_annotations]
            return self.suggest_nearby_annotations(user_locations)
        elif strategy == 'annotations_based_on_interests':
            return self.suggest_annotations_based_on_interests()
        elif strategy == 'annotations_by_filters':
            return self.suggest_annotation_by_filter()
        elif strategy == 'least_rich_user_annotation':
            return self.suggest_least_rich_annotation()
        elif strategy == 'annotations_based_on_reactions':
            return self.suggest_active_annotations()
        elif strategy == 'nearby_tours':
            user_locations = [annotation['coords'] for annotation in self.user_annotations]
            return self.suggest_nearby_tours(user_locations)
        elif strategy == 'uexplored_zone_annotation':
            return self.suggeste_uexplored_zone_annotation()
        else:
            raise ValueError("Strategy not recognized")

    def refine_suggestions(self, suggestions, filters):
        """Affiner les suggestions selon les filtres fournis."""
        refined_suggestions = []
        for suggestion in suggestions:
            match = True
            for key, value in filters.items():
                if key in suggestion and suggestion[key] != value:
                    match = False
                    break
            if match:
                refined_suggestions.append(suggestion)
        return refined_suggestions

    def suggest(self, strategies, filters=None):
        """Point d'entrée pour les suggestions exécutant un pipeline de stratégies."""
        filters = filters or {}
        all_suggestions = []
        
        # Exécuter chaque stratégie et collecter les suggestions
        for strategy in strategies:
            suggestions = self.execute_strategy(strategy)
            all_suggestions.extend(suggestions)

        # Affiner les suggestions combinées selon les filtres fournis
        refined_suggestions = self.refine_suggestions(all_suggestions, filters)
        return refined_suggestions