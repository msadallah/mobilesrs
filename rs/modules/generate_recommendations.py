import pandas as pd
from sqlalchemy import create_engine, Table, MetaData
from datetime import datetime

# Connexion à la base de données
import pandas as pd
from sqlalchemy import create_engine, MetaData, Table
from datetime import datetime

def recuperer_indicateurs(categorie: str, strategie: str) -> pd.DataFrame:
    """
    Récupère les indicateurs de la base de données pour une catégorie et une stratégie données.

    :param categorie: Catégorie des indicateurs.
    :param strategie: Stratégie des indicateurs.
    :return: DataFrame avec les indicateurs.
    """
    engine = create_engine('sqlite:///database.db')  # Remplacez par l'URL de votre base de données
    metadata = MetaData(bind=engine)
    indicators_table = Table('indicators', metadata, autoload=True)
    recommendations_table = Table('recommendations', metadata, autoload=True)

    with engine.connect() as connection:
        query = indicators_table.select().where(
            (indicators_table.c.categorie == categorie) & 
            (indicators_table.c.strategie == strategie)
        )
        result = connection.execute(query)
        return pd.DataFrame(result.fetchall(), columns=result.keys())

def enregistrer_recommandation(categorie: str, strategie: str, recommandation: str, date: str):
    """
    Enregistre une recommandation dans la table 'recommendations'.

    :param categorie: Catégorie de la recommandation.
    :param strategie: Stratégie associée à la recommandation.
    :param recommandation: Contenu de la recommandation.
    :param date: Date d'enregistrement.
    """
    engine = create_engine('sqlite:///database.db')  # Remplacez par l'URL de votre base de données
    metadata = MetaData(bind=engine)
    recommendations_table = Table('recommendations', metadata, autoload=True)

    with engine.connect() as connection:
        connection.execute(recommendations_table.insert().values(
            categorie=categorie,
            strategie=strategie,
            recommandation=recommandation,
            date=date
        ))

# Fonction pour vérifier les conditions de déclenchement pour chaque catégorie

# Catégorie 1: Engagement Utilisateur
def verifier_engagement_utilisateur():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Engagement Utilisateur'.
    """
    SEUILS = {
        'Engagement Rate': 50,
        'User Contribution Rate': 30,
        'Activity Frequency': 5
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Engagement Utilisateur', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Engagement Rate': "Augmentez les interactions avec les utilisateurs pour améliorer l'engagement.",
                'User Contribution Rate': "Encouragez les utilisateurs à contribuer davantage pour améliorer le taux de contribution.",
                'Activity Frequency': "Augmentez la fréquence des activités pour améliorer l'engagement."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Engagement Utilisateur', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

# Catégorie 2: Qualité des Contributions
def verifier_qualite_contributions():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Qualité des Contributions'.
    """
    SEUILS = {
        'Annotation Quality': 4,
        'Parcours Quality': 4,
        'Annotation Clarity': 4,
        'Annotation Relevance': 4,
        'Annotation Coverage': 80,
        'Parcours Completeness': 80,
        'Parcours Accuracy': 80,
        'Parcours Relevance': 80,
        'Parcours Clarity': 80,
        'Annotation Experience': 4,
        'Parcours Experience': 4
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Qualité des Contributions', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Annotation Quality': "Améliorez la qualité des annotations pour mieux répondre aux attentes des utilisateurs.",
                'Parcours Quality': "Améliorez la qualité des parcours pour offrir une meilleure expérience utilisateur.",
                'Annotation Clarity': "Augmentez la clarté des annotations pour une meilleure compréhension.",
                'Annotation Relevance': "Assurez-vous que les annotations sont plus pertinentes pour les utilisateurs.",
                'Annotation Coverage': "Augmentez la couverture des annotations pour inclure une plus grande diversité de contenu.",
                'Parcours Completeness': "Assurez-vous que les parcours sont plus complets pour une meilleure expérience.",
                'Parcours Accuracy': "Améliorez l'exactitude des parcours pour une meilleure précision.",
                'Parcours Relevance': "Augmentez la pertinence des parcours pour mieux répondre aux besoins des utilisateurs.",
                'Parcours Clarity': "Améliorez la clarté des parcours pour une meilleure compréhension.",
                'Annotation Experience': "Améliorez l'expérience des utilisateurs lors de l'annotation pour une meilleure satisfaction.",
                'Parcours Experience': "Améliorez l'expérience des utilisateurs lors de la navigation dans les parcours pour une meilleure satisfaction."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Qualité des Contributions', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

# Catégorie 3: Interaction et Réflexion
def verifier_interaction_reflexion():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Interaction et Réflexion'.
    """
    SEUILS = {
        'Annotation Revisions': 10,
        'Community Interaction': 5,
        'Commenting Rate': 3,
        'Annotation Quality Self-Reflection': 4,
        'Self-Reflection Rate': 3,
        'Improvement Rate': 10,
        'Community Reflection Rate': 5,
        'Community Engagement Depth': 4
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Interaction et Réflexion', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Annotation Revisions': "Augmentez les révisions des annotations pour améliorer la qualité globale.",
                'Community Interaction': "Favorisez davantage d'interactions communautaires pour stimuler l'engagement.",
                'Commenting Rate': "Encouragez les utilisateurs à commenter davantage pour améliorer l'interaction.",
                'Annotation Quality Self-Reflection': "Encouragez les annotateurs à réfléchir davantage sur la qualité de leurs annotations.",
                'Self-Reflection Rate': "Augmentez le taux de réflexion personnelle pour améliorer la qualité des annotations.",
                'Improvement Rate': "Encouragez les utilisateurs à améliorer leurs contributions pour une meilleure qualité globale.",
                'Community Reflection Rate': "Favorisez la réflexion communautaire pour améliorer les contributions globales.",
                'Community Engagement Depth': "Augmentez la profondeur de l'engagement communautaire pour une meilleure interaction."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Interaction et Réflexion', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

# Catégorie 4: Amélioration Continue et Performance de l'Application
def verifier_performance_application():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Amélioration Continue et Performance de l'Application'.
    """
    SEUILS = {
        'Recommendation Precision': 80,
        'Recommendation Coverage': 70,
        'Application Response Time': 2000,
        'Error Rate': 5
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Amélioration Continue et Performance de l\'Application', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Recommendation Precision': "Améliorez la précision des recommandations pour mieux répondre aux besoins des utilisateurs.",
                'Recommendation Coverage': "Augmentez la couverture des recommandations pour inclure une plus grande diversité de contenu.",
                'Application Response Time': "Optimisez le temps de réponse de l'application pour améliorer l'expérience utilisateur.",
                'Error Rate': "Réduisez le taux d'erreur de l'application pour améliorer la fiabilité."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Amélioration Continue et Performance de l\'Application', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

# Catégorie 5: Initiation et Intégration des Utilisateurs
def verifier_initiation_integration_utilisateur():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Initiation et Intégration des Utilisateurs'.
    """
    SEUILS = {
        'Nombre de Connecteurs par Jour': 5,
        'Durée de Session': 10,
        'Nombre de Contributions par Session': 3
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Initiation et Intégration des Utilisateurs', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Nombre de Connecteurs par Jour': "Augmentez le nombre de connecteurs quotidiens pour une meilleure initiation.",
                'Durée de Session': "Prolongez la durée des sessions pour améliorer l'engagement des utilisateurs.",
                'Nombre de Contributions par Session': "Encouragez les utilisateurs à faire plus de contributions par session."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Initiation et Intégration des Utilisateurs', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

# Catégorie 6: Évolution de l'Expérience Utilisateur
def verifier_evolution_experience_utilisateur():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Évolution de l'Expérience Utilisateur'.
    """
    SEUILS = {
        'Taux de Satisfaction des Utilisateurs': 4,
        'Taux de Répétition des Utilisateurs': 20,
        'Durée d\'Utilisation': 15
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Évolution de l\'Expérience Utilisateur', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Taux de Satisfaction des Utilisateurs': "Améliorez la satisfaction des utilisateurs pour une meilleure rétention.",
                'Taux de Répétition des Utilisateurs': "Augmentez le taux de répétition des utilisateurs pour améliorer la fidélité.",
                'Durée d\'Utilisation': "Prolongez la durée d'utilisation pour une meilleure expérience utilisateur."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Évolution de l\'Expérience Utilisateur', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

# Catégorie 7: Rendement des Tâches et Optimisation des Ressources
def verifier_rendement_taches():
    """
    Vérifie les conditions de déclenchement pour les stratégies de la catégorie 'Rendement des Tâches'.
    """
    SEUILS = {
        'Taux d\'Achèvement des Tâches': 80,
        'Durée d\'Achèvement des Tâches': 60,
        'Qualité des Résultats des Tâches': 4
    }

    strategies = list(SEUILS.keys())

    for strategie in strategies:
        df = recuperer_indicateurs('Rendement des Tâches', strategie)
        valeur = df['valeur'].mean()

        if valeur < SEUILS[strategie]:
            recommandations = {
                'Taux d\'Achèvement des Tâches': "Augmentez le taux d'achèvement des tâches pour améliorer le rendement.",
                'Durée d\'Achèvement des Tâches': "Réduisez la durée d'achèvement des tâches pour améliorer l'efficacité.",
                'Qualité des Résultats des Tâches': "Améliorez la qualité des résultats des tâches pour mieux répondre aux attentes."
            }
            recommandation = recommandations[strategie]
            enregistrer_recommandation('Rendement des Tâches', strategie, recommandation, datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

def generate_recommendations():
    """
    Fonction principale pour exécuter les vérifications et générer les recommandations pour chaque catégorie.
    """
    verifier_engagement_utilisateur()  # Catégorie 1
    verifier_qualite_contributions()  # Catégorie 2
    verifier_interaction_reflexion()  # Catégorie 3
    verifier_performance_application()  # Catégorie 4
    verifier_initiation_integration_utilisateur()  # Catégorie 5
    verifier_evolution_experience_utilisateur()  # Catégorie 6
    verifier_rendement_taches()  # Catégorie 7
