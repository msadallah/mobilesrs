from datetime import datetime, timedelta
from sqlalchemy import and_
from modules.db_operations import fetch_recommendations, fetch_users_with_pending_recommendations, get_user_registration_date, update_recommendation_status
from web_app.models import Recommendation
from sqlalchemy.exc import SQLAlchemyError
import logging
import pandas as pd

# Configuration de la journalisation
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Durée de la phase de découverte en jours
DISCOVERY_PERIOD_DAYS = 15
# Période minimale entre deux recommandations identiques (en jours)
MIN_RECO_FREQUENCY_DAYS = 30

def notify_user(user_id, recommendation):
    """
    Envoie une notification de recommandation à un utilisateur et met à jour le statut en 'Sent'.
    """
    try:
        logging.info(f"Sending notification for User {user_id}: {recommendation['strategy']}")
        
        # Mettre à jour le statut de la recommandation en 'Sent'
        update_recommendation_status(recommendation['id'], 'Sent')
        logging.info("Notification sent and status updated.")
    except Exception as e:
        logging.error(f"Error sending notification: {e}")

def is_in_discovery_phase(user_id):
    """
    Vérifie si l'utilisateur est encore dans sa phase de découverte.
    """
    user_signup_date = datetime.fromisoformat(get_user_registration_date(user_id))
    print('User:', user_id, ' - User_signup_date: ', user_signup_date)
    discovery_end_date = user_signup_date + timedelta(days=DISCOVERY_PERIOD_DAYS)
    return datetime.utcnow() < discovery_end_date

def get_recent_recommendations(recommendations_df, user_id):
    """
    Récupère les recommandations envoyées récemment à un utilisateur (dans les 30 derniers jours).
    """
    thirty_days_ago = datetime.utcnow() - timedelta(days=MIN_RECO_FREQUENCY_DAYS)
    
    # Filtrer les recommandations du DataFrame
    recent_recommendations = recommendations_df[
        (recommendations_df['user_id'] == user_id) &
        (recommendations_df['status'] == 'Sent') &
        (recommendations_df['created_at'] >= thirty_days_ago)
    ]
    
    return recent_recommendations['strategy'].tolist()

def get_recommendations_by_category(recommendations_df, strategy_list):
    """
    Récupère les recommandations par catégorie spécifiée.
    """
    return recommendations_df[
        (recommendations_df['strategy'].isin(strategy_list)) &
        (recommendations_df['status'] == 'Created')
    ]

def discard_recommendation(recommendation_id):
    """
    Marque une recommandation comme 'Discarded'.
    """
    try:
        update_recommendation_status(recommendation_id, 'Discarded')
        logging.info(f"Recommendation {recommendation_id} has been discarded.")
    except Exception as e:
        logging.error(f"Error discarding recommendation {recommendation_id}: {e}")

def discard_previous_recommendations(user_id, new_strategy, recommendations_df):
    """
    Écarte les recommandations précédentes du même type pour un utilisateur donné.
    """
    existing_recommendations = recommendations_df[
        (recommendations_df['user_id'] == user_id) & 
        (recommendations_df['strategy'] == new_strategy) & 
        (recommendations_df['status'].isin(['Ready', 'Created']))
    ]
    
    for _, row in existing_recommendations.iterrows():
        discard_recommendation(row['id'])

def should_discard_recommendation(strategy, user_id, recommendations_df):
    """
    Vérifie si une recommandation doit être écartée en fonction de son type et des recommandations récentes.
    """
    recent_recommendations = get_recent_recommendations(recommendations_df, user_id)
    
    if strategy in recent_recommendations:
        return True  # Une recommandation de même type a déjà été envoyée récemment

    existing_recommendations = recommendations_df[
        (recommendations_df['strategy'] == strategy) &
        (recommendations_df['user_id'] == user_id) &
        (recommendations_df['status'].isin(["Ready", "Sent"]))
    ]
    
    return len(existing_recommendations) > 0


def select_recommendations(user_id):
    """
    Sélectionne une seule recommandation appropriée pour un utilisateur donné en fonction des règles définies.
    """
    # Récupérer toutes les recommandations de l'utilisateur
    recommendations_df = fetch_recommendations(user_id)

    # Si l'utilisateur n'a pas de recommandations, retourner None
    if recommendations_df.empty:
        logging.info(f"Aucune recommandation trouvée pour l'utilisateur {user_id}")
        return None

    # Vérifier si l'utilisateur est encore dans sa phase de découverte
    if is_in_discovery_phase(user_id):
        discovery_strategies = ['InitSupport', 'FuncCreateShare', 'FuncContentExplore', 'FuncInteraction']
        recommendations = get_recommendations_by_category(recommendations_df, discovery_strategies)
    else:
        # Sélectionner les stratégies après la période de découverte
        corrective_strategies = ['QualityImprove', 'TextIncentive', 'SymbolEnrich', 'GraphicalExpress',
                                 'POIDiscover', 'CoverageExpand', 'ConcentrationReduce']
        recommendations = get_recommendations_by_category(recommendations_df, corrective_strategies)

        if recommendations.empty:
            engagement_strategies = ['AnnotationProfileEngage', 'AnnotationLocationEngage', 'RouteSuggest', 
                                     'UserActivityReengage', 'UserCreationReengage', 'UserExplorationReengage',
                                     'UserInteractionReengage', 'AnnotationHistoryReengage', 'TourHistoryReengage', 
                                     'CommunityReengage']
            recommendations = get_recommendations_by_category(recommendations_df, engagement_strategies)

    if recommendations.empty:
        logging.info(f"Aucune recommandation applicable pour l'utilisateur {user_id}.")
        return None

    # Écarter les recommandations précédentes du même type
    new_strategy = recommendations.iloc[0]['strategy']  # Sélectionner la stratégie de la première recommandation
    discard_previous_recommendations(user_id, new_strategy, recommendations_df)

    # Règles de priorité pour 'QualityImprove' et 'UserActivityReengage'
    if 'QualityImprove' in recommendations['strategy'].values:
        recommendations = recommendations[~recommendations['strategy'].isin(['TextIncentive', 'SymbolEnrich', 'GraphicalExpress'])]

    if 'UserActivityReengage' in recommendations['strategy'].values:
        recommendations = recommendations[~recommendations['strategy'].isin(['UserCreationReengage', 'UserExplorationReengage', 'UserInteractionReengage'])]

    # Filtrer les recommandations à écarter
    filtered_recommendations = recommendations[~recommendations['strategy'].apply(
        lambda x: should_discard_recommendation(x, user_id, recommendations_df))]

    if filtered_recommendations.empty:
        logging.info(f"Aucune recommandation à envoyer pour l'utilisateur {user_id} après filtrage.")
        return None

    # Ordre de préférence des recommandations après la période de découverte
    if not is_in_discovery_phase(user_id):
        preference_order = ['UserActivityReengage', 'QualityImprove', 'POIDiscover', 
                            'CoverageExpand', 'ConcentrationReduce', 
                            'TextIncentive', 'SymbolEnrich', 'GraphicalExpress']
        
        # Créer une colonne de priorité
        filtered_recommendations['priority'] = filtered_recommendations['strategy'].apply(
            lambda x: preference_order.index(x) if x in preference_order else float('inf')
        )
        
        # Filtrer les recommandations par priorité
        filtered_recommendations = filtered_recommendations.sort_values('priority')

    # Sélectionner UNE SEULE recommandation (en prenant la première par exemple après filtrage)
    selected_recommendation = filtered_recommendations.iloc[0]

    # Mettre à jour le statut de la recommandation sélectionnée à 'Ready'
    if selected_recommendation['status'] == 'Created':
        update_recommendation_status(selected_recommendation['id'], 'Ready')
        notify_user(user_id, selected_recommendation)

    return selected_recommendation




def send_recommendation_notifications():
    notifiable_users = fetch_users_with_pending_recommendations()
    if not notifiable_users.empty:
        for index, user in notifiable_users.iterrows():
            print('Managing recommendations of user:', user['user_id'])
            select_recommendations(user['user_id'])

    else:
        print("Aucun utilisateur trouvé avec des recommandations à analyser.")
