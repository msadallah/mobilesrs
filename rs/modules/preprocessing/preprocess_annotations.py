import spacy
import re
import string

# Charger le modèle de langue française
nlp = spacy.load('fr_core_news_sm')

MAX_ALLOWED_TAGS = 10
MAX_ALLOWED_ICONS = 10

def preprocess_text_french(text, keep_punctuation=False):
    """
    Prétraite le texte en français en le nettoyant, en supprimant la ponctuation (optionnelle),
    en le tokenisant, en supprimant les stopwords, et en appliquant la racinisation.

    :param text: Le texte à prétraiter.
    :param keep_punctuation: Boolean indiquant si la ponctuation doit être conservée.
    :return: Une liste de phrases prétraitées.
    """
    # Nettoyage du texte
    text = re.sub(r'[^a-zA-ZÀ-ÿ\s]', '', text).lower()

    # Traitement du texte avec spaCy
    doc = nlp(text)

    # Optionnel: suppression de la ponctuation
    if not keep_punctuation:
        tokens = [token.text for token in doc if not token.is_punct and not token.is_space]
    else:
        tokens = [token.text for token in doc if not token.is_space]

    # Suppression des stopwords et racinisation
    tokens = [token.lemma_ for token in doc if not token.is_stop and token.lemma_ != '-PRON-']

    return tokens


def calculate_volume_lexical(text):
    return len(' '.join(preprocess_text_french(text)).split())

def calculate_diversity_lexical(text):
    return len(set(' '.join(preprocess_text_french(text)).split()))

def calculate_ratio(value, total):
    return 0.0 if total == 0 else float(value / total)

def calculate_icons_metrics(used_icons, message):
    if not used_icons:
        return {key: 0.0 for key in [
            'diversity_types_icons_used', 'efficiency_icons_used', 'frequency_sensationnel',
            'activity_dynamics', 'atmosphere_environment', 'emphasis_social', 'resonance_memory'
        ]}

    ICON_MAPPING = {
        'Senses': ['senses', 'seasons', 'meteo', "daytime"],
        'Activity': ['shopping', 'shop', 'trip', 'restaurant', 'culture', 'sport', 'studies', 'ride', 'work', 'health', 'home', 'transport'],
        'Env': ['eyeview', 'site', 'nature', 'tourism'],
        'Social': ['date', 'friends', 'meet', 'community', 'social'],
        'Memory': ['memory', 'homesick', 'heartplace', 'homeheart']
    }

    icon_counts = {icon: used_icons.count(icon) for icon in set(used_icons)}

    used_icon_types = [icon_type for icon_type, icons in ICON_MAPPING.items() if any(icon in icons for icon in used_icons)]
    diversity_types_icons_used = calculate_ratio(len(set(used_icon_types)), len(ICON_MAPPING))
    efficiency_icons_used = calculate_ratio(len(re.findall(r'\w+', message)), len(used_icons))

    # print('used_icons: ', used_icons)
    # print('computed icon_counts: ', icon_counts)

    return {
        'diversity_types_icons_used': diversity_types_icons_used,
        'types_icons_count': len(set(used_icon_types)),
        'efficiency_icons_used': efficiency_icons_used,
        'senses_ratio': calculate_ratio(sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Senses']), len(used_icons)),
        'activity_ratio': calculate_ratio(sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Activity']), len(used_icons)),
        'environment_ratio': calculate_ratio(sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Env']), len(used_icons)),
        'social_ratio': calculate_ratio(sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Social']), len(used_icons)),
        'memory_ratio': calculate_ratio(sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Memory']), len(used_icons))
    }

def calculate_tags_metrics(used_tags):
    positive_tags = ['#utile', '#plaisir', '#beau', '#agréable']
    negative_tags = ['#laid', '#désagréable']
    aesthetic_tags = ['#beau', '#laid', '#agréable', '#désagréable']
    social_tags = ['#animé', '#isolé', '#social', '#personnel', '#sur', '#dangereux', '#familier', '#inhabituel']

    total_tags_used = len(used_tags)

    return {
        'tag_influence_index': calculate_ratio(total_tags_used, MAX_ALLOWED_TAGS),
        'positive_sentiment_ratio': calculate_ratio(sum(tag in positive_tags for tag in used_tags), total_tags_used),
        'negative_sentiment_ratio': calculate_ratio(sum(tag in negative_tags for tag in used_tags), total_tags_used),
        'aesthetic_tag_index': calculate_ratio(sum(tag in aesthetic_tags for tag in used_tags), total_tags_used),
        'social_interaction_index': calculate_ratio(sum(tag in social_tags for tag in used_tags), total_tags_used)
    }

def calculate_emoticon_metrics(emoticon):
    return 1 if emoticon and not emoticon.isspace() else 0

def calculate_graphical_expressiveness(imageId):
    return 1 if imageId and not imageId.isspace() else 0

def calculate_icons_count(used_icons):
    ICON_MAPPING = {
        'Senses': ['senses', 'seasons', 'meteo', "daytime"],
        'Activity': ['shopping', 'shop', 'trip', 'restaurant', 'culture', 'sport', 'studies', 'ride', 'work', 'health', 'home', 'transport'],
        'Env': ['eyeview', 'site', 'nature', 'tourism'],
        'Social': ['date', 'friends', 'meet', 'community', 'social'],
        'Memory': ['memory', 'homesick', 'heartplace', 'homeheart'],
        'Mission': ['mission01', 'mission02', 'mission03', 'mission04', 'mission05', 'mission06', 'mission07', 'mission08', 'mission09', 'mission10', 'mission11', 'mission12', 'mission13', 'mission14']
    }

    icon_counts = {icon: used_icons.count(icon) for icon in set(used_icons)}

    return {
        'total_icons_count': len(used_icons),
        'senses_icons_count': sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Senses']),
        'social_icons_count': sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Social']),
        'memory_icons_count': sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Memory']),
        'activity_icons_count': sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Activity']),
        'environment_icons_count': sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Env']),
        'mission_icons_count': sum(icon_counts.get(icon, 0) for icon in ICON_MAPPING['Mission'])
    }

def calculate_tags_count(used_tags):
    positive_tags = ['#utile', '#plaisir', '#beau', '#agréable']
    negative_tags = ['#laid', '#désagréable']
    aesthetic_tags = ['#beau', '#laid', '#agréable', '#désagréable']
    social_tags = ['#animé', '#isolé', '#social', '#personnel', '#sur', '#dangereux', '#familier', '#inhabituel']

    return {
        'total_tags_count': len(used_tags),
        'positive_tags_count': sum(tag in positive_tags for tag in used_tags),
        'negative_tags_count': sum(tag in negative_tags for tag in used_tags),
        'aesthetic_tags_count': sum(tag in aesthetic_tags for tag in used_tags),
        'social_tags_count': sum(tag in social_tags for tag in used_tags)
    }

def preprocess_annotations(annotations):
    for doc in annotations:
        # Récupérer le texte du commentaire, ou une chaîne vide si la colonne 'comment' est absente ou vide
        annotation_text = doc.get('comment', '')

        # Récupérer les icônes utilisées, ou une liste vide si la colonne 'place_icon' est absente ou vide
        used_icons = doc.get('placeIcon', '').split(',') if doc.get('placeIcon') else []

        # Récupérer les tags utilisés, ou une liste vide si la colonne 'tags' est absente ou vide
        used_tags = doc.get('tags', '').split() if doc.get('tags') else []

        # Récupérer l'emoticon, ou une chaîne vide si la colonne 'emoticon' est absente
        emoticon = doc.get('emoticon', '')

        # Récupérer l'ID de l'image, ou None si la colonne 'image_id' est absente
        imageId = doc.get('image')

        doc['volume_lexical'] = calculate_volume_lexical(annotation_text)
        
        doc['diversity_lexical'] = calculate_diversity_lexical(annotation_text)

        icon_metrics = calculate_icons_metrics(used_icons, annotation_text)
        doc.update(icon_metrics)

        tags_metrics = calculate_tags_metrics(used_tags)
        doc.update(tags_metrics)

        doc['emoticonality'] = calculate_emoticon_metrics(emoticon)
        
        doc['graphical'] = calculate_graphical_expressiveness(imageId)

        icons_count = calculate_icons_count(used_icons)
        doc.update(icons_count)
        tags_count = calculate_tags_count(used_tags)
        doc.update(tags_count)
    
    print("Finished processing annotations.")
    return annotations
