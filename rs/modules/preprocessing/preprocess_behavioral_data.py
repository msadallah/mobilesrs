from elasticsearch import Elasticsearch
from sqlalchemy import create_engine, MetaData, Table, Column, String
from sqlalchemy.orm import sessionmaker

def preprocess_behavioral_data(raw_data):
    preprocessed_data = []
    for entry in raw_data:
        # Votre logique de prétraitement ici
        processed_entry = {
            'user_id': entry['user_id'],
            'action': entry['action'],
            'timestamp': entry['timestamp'],
            # Ajoutez d'autres champs nécessaires après prétraitement
        }
        preprocessed_data.append(processed_entry)
    return preprocessed_data

def fetch_behavioral_data_from_elasticsearch():
    es = Elasticsearch(['http://elasticsearch:9200'])
    result = es.search(index="behavioral_data", body={"query": {"match_all": {}}})
    return [hit['_source'] for hit in result['hits']['hits']]

def connect_to_local_database():
    engine = create_engine('mysql+pymysql://mobiles:mobilespassword@localhost/gpstour', echo=False)
    Session = sessionmaker(bind=engine)
    return Session()

def save_behavioral_data_to_local_database(behavioral_data):
    try:
        db_session = connect_to_local_database()

        metadata = MetaData()

        behavioral_table = Table('behavioral_data', metadata,
                                 Column('user_id', String(length=255), primary_key=True),
                                 Column('action', String(length=255)),
                                 Column('timestamp', String(length=255)),
                                 extend_existing=True)

        metadata.create_all(db_session.bind, checkfirst=True)

        for data in behavioral_data:
            db_session.execute(behavioral_table.insert().prefix_with("IGNORE"), data)

        db_session.commit()
        print("Behavioral data saved to local database successfully.")

    except Exception as e:
        print(f"Error saving behavioral data to local database: {e}")
    finally:
        db_session.close()
