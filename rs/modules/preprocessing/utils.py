ICON_MAPPING = {
    'Senses': ['senses', 'seasons', 'meteo', "daytime"],
    'Activity': ['shopping', 'shop', 'trip', 'restaurant', 'culture', 'sport', 'studies', 'ride', 'work', 'health', 'home', 'transport'],
    'Env': ['eyeview', 'site', 'nature' , 'tourism'],
    'Social': ['date', 'friends', 'meet' , 'community', 'social'],
    'Memory': ['memory', 'homesick', 'heartplace', 'homeheart'],
    'Mission': ['mission01','mission02','mission03','mission04','mission05','mission06','mission07','mission08','mission09',
                'mission10','mission11','mission12','mission13','mission14']
}
