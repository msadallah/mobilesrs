from modules.db_operations import fetch_indicators, fetch_new_annotations, fetch_tours
from modules.es_operations import ( fetch_annotation_popularity, fetch_user_ids, fetch_user_locations)
from modules.metrics.adoption_integration_m import calculate_nc, calculate_nca, calculate_ncoa, calculate_ncr, calculate_ndl, calculate_nia, calculate_nis, calculate_nr, calculate_suf
from modules.metrics.content_quality_m import calculate_quality_indicators
from modules.metrics.engagement_reengagement_m import calculate_air, calculate_annotation_consultation_weight, calculate_annotation_creation_weight, calculate_aua, calculate_aun, calculate_id,  calculate_oa, calculate_ocp, calculate_os, calculate_path_consultation_weight, calculate_path_creation_weight,  calculate_pf, calculate_pla, calculate_pp, calculate_sca, calculate_scp, calculate_sea, calculate_sep, calculate_sis, calculate_tca, calculate_tcp, calculate_tel, calculate_tep, calculate_tpa, calculate_tru
from modules.metrics.interaction_reflection_m import calculate_tac, calculate_tcc, calculate_tec, calculate_trc
from modules.metrics.urban_discovery_m import calculate_dap, calculate_dau, calculate_dc, calculate_dmapp, calculate_dsa, calculate_prl, calculate_pze, calculate_spc



def compute_adoption_metrics():    
    user_ids = fetch_user_ids()

    for user in user_ids:        
        calculate_nia(user)
        calculate_nis(user)
        calculate_suf(user)
        calculate_nca(user)
        calculate_ncr(user)
        calculate_ndl(user)
        calculate_ncoa(user)
        calculate_nc(user)
        calculate_nr(user)
        

def compute_engagement_metrics():
    """
    Calcul des métriques pour la Catégorie 2 pour tous les utilisateurs.
    """
    user_ids = fetch_user_ids()
    
    # tours = fetch_tours()
    # popularity_data = fetch_annotation_popularity()
    # new_annotations = fetch_new_annotations()
    
    
    for user_id in user_ids:
        user_locations = fetch_user_locations(user_id)
        indicators = fetch_indicators(user_id)
        calculate_annotation_creation_weight(user_id)
        calculate_annotation_consultation_weight(user_id)
        calculate_pla(user_id, user_locations)
        calculate_path_creation_weight(user_id)
        calculate_path_consultation_weight(user_id)
        calculate_id(user_id, period_days=30)
        calculate_sea(user_id, indicators)
        calculate_sep(user_id, indicators)
        calculate_sca(user_id)
        calculate_scp(user_id)
        calculate_sis(user_id)

        # calculate_annotation_creation_weight(user_id)
        # calculate_annotation_consultation_weight(user_id)
        # calculate_path_creation_weight(user_id)
        # calculate_path_consultation_weight(user_id)
        # calculate_sea(user_id)
        # calculate_sep(user_id)
        # calculate_pla(user_id, user_location)
        # calculate_tel(user_id)
        # calculate_oa(user_id)        
        # calculate_pp(user_id, user_location)        
        # calculate_ocp(tours, user_id)        
        # calculate_id(user_id)        
        # calculate_air(user_id)
        # calculate_tpa(user_id)
        # calculate_pf(user_id)
        # calculate_os(user_id)
        # calculate_aua(user_id,popularity_data)
        # calculate_aun(user_id,new_annotations)
        # calculate_pc_and_auc(user_id)
        # calculate_ndg_and_aun(user_id)
        # calculate_tru()
        # calculate_tcp(user_id)
        # calculate_tep(user_id)
        # calculate_tca(user_id)


def compute_quality_metrics():   
    user_ids = fetch_user_ids()
    
    for user_id in user_ids:
        calculate_quality_indicators(user_id)
    
def compute_urban_discovery_metrics():
    user_ids = fetch_user_ids()
    
    for user_id in user_ids:         
        calculate_dap(user_id)
        calculate_prl(user_id)
        calculate_dmapp(user_id)
        calculate_spc(user_id)        
        calculate_dc(user_id)

        # calculate_pze(user_id)
        # # calculate_prl(user_id)
        
        # calculate_dau(user_id)
        
        # calculate_dsa(user_id)


def compute_interaction_reflection_metrics():
    user_ids = fetch_user_ids()
    for user_id in user_ids:        
        calculate_tac(user_id)
        calculate_tec(user_id)
        calculate_trc(user_id)
        calculate_tcc(user_id)


# def calculate_metrics():
    
#     print("Calcul des métriques de la catégorie 1 - adoption...")
#     compute_adoption_metrics()

#     print("Calcul des métriques de la catégorie 2 - engagement...")
#     compute_engagement_metrics()

#     print("Calcul des métriques de la catégorie 3 - qualité...")
#     compute_quality_metrics()


#     print("Calcul des métriques de la catégorie 4 - diversité urbaine...")
#     compute_urban_discovery_metrics()

#     print("Calcul des métriques de la catégorie 5 - interaction & reflexion...")
#     compute_interaction_reflection_metrics()
def calculate_metrics(category=None):
    """
    Calcule les métriques pour une catégorie spécifique ou pour toutes les catégories si aucune n'est spécifiée.
    
    :param category: (str) La catégorie de métriques à calculer. Peut être 'adoption', 'engagement', 'qualité', 
                     'diversité urbaine' ou 'interaction & reflexion'. Si None, toutes les catégories seront calculées.
    """

    if category is None:
        print("Calcul des métriques de toutes les catégories...")
        print("Calcul des métriques de la catégorie 1 - adoption...")
        compute_adoption_metrics()

        print("Calcul des métriques de la catégorie 2 - engagement...")
        compute_engagement_metrics()

        print("Calcul des métriques de la catégorie 3 - qualité...")
        compute_quality_metrics()

        print("Calcul des métriques de la catégorie 4 - diversité urbaine...")
        compute_urban_discovery_metrics()

        print("Calcul des métriques de la catégorie 5 - interaction & reflexion...")
        compute_interaction_reflection_metrics()
    
    elif category == "adoption":
        print("Calcul des métriques de la catégorie 1 - adoption...")
        compute_adoption_metrics()
        
    elif category == "engagement":
        print("Calcul des métriques de la catégorie 2 - engagement...")
        compute_engagement_metrics()
        
    elif category == "quality":
        print("Calcul des métriques de la catégorie 3 - qualité...")
        compute_quality_metrics()
        
    elif category == "urbain":
        print("Calcul des métriques de la catégorie 4 - diversité urbaine...")
        compute_urban_discovery_metrics()
        
    elif category == "reflexion":
        print("Calcul des métriques de la catégorie 5 - interaction & reflexion...")
        compute_interaction_reflection_metrics()
        
    else:
        print(f"Catégorie inconnue : {category}. Veuillez spécifier une catégorie valide.")
