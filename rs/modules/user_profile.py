from datetime import datetime
import json

import pandas as pd
from modules.db_operations import fetch_annotations_by_id, fetch_user_tours, fetch_user_annotations
from modules.es_operations import fetch_user_ids, fetch_user_viewed_annotations
from web_app import db
from web_app.models import UserProfile


def calculate_covered_zones(annotations):
    from geopy.distance import geodesic
    
    if annotations.empty:
        return 0  # Retourne 0 si le DataFrame est vide

    ZONE_RADIUS = 5.0  # Rayon en kilomètres pour regrouper les points géographiquement proches
    
    zones = []
    for _, annotation in annotations.iterrows():
        # Assurez-vous que 'coords' est bien un dictionnaire
        coords = annotation['coords']
        if isinstance(coords, str):
            coords = eval(coords)  # Convertir la chaîne en dictionnaire si nécessaire
        
        coord = (coords['lat'], coords['lon'])
        matched_zone = False
        for zone in zones:
            if geodesic(coord, zone).km <= ZONE_RADIUS:
                matched_zone = True
                break
        if not matched_zone:
            zones.append(coord)
    
    return len(zones)



def calculate_profile_metrics(annotations, prefix):
    # Structure par défaut pour les valeurs manquantes ou nulles
    default_structure = {
        f'{prefix}items_count': 0,
        f'{prefix}avg_volume_lexical': 0,
        f'{prefix}avg_icons_per_annotation': 0,
        f'{prefix}avg_types_icons': 0,
        f'{prefix}icon_type_ratio': {
            'senses': 0,
            'social': 0,
            'memory': 0,
            'activity': 0,
            'environment': 0,
            'mission': 0,
        },
        f'{prefix}avg_tags_per_annotation': 0,
        f'{prefix}tag_type_ratio': {
            'positive': 0,
            'negative': 0,
            'aesthetic': 0,
            'social': 0,
        },
        f'{prefix}emoticon_top3': [None, None, None],
        f'{prefix}emoticonality_avg': 0.0,
        f'{prefix}place_type_usage': {
            'here': 0,
            'close': 0,
            'district': 0,
            'city': 0,
            'tour': 0
        },
        f'{prefix}experience_frequency': {
            'UNIQUE': 0,
            'REGULAR': 0,
            'OCCASIONAL': 0
        },
        f'{prefix}covered_zones': None,
    }
    
    # Si aucune annotation, retourne la structure par défaut
    if len(annotations) == 0:
        print('No annotations found')
        return default_structure

    items_count = len(annotations)
    print(f'User has {items_count} annotations')

    # Calculs des métriques
    avg_volume_lexical = annotations['volume_lexical'].mean() if 'volume_lexical' in annotations and not annotations['volume_lexical'].isnull().all() else 0
    avg_icons_per_annotation = annotations['total_icons_count'].mean() if 'total_icons_count' in annotations and not annotations['total_icons_count'].isnull().all() else 0
    avg_types_icons = annotations['types_icons_count'].mean() if 'types_icons_count' in annotations and not annotations['types_icons_count'].isnull().all() else 0

    # Initialisation des ratios d'icônes avec les valeurs par défaut
    total_icons_by_type = {
        'senses': 0,
        'social': 0,
        'memory': 0,
        'activity': 0,
        'environment': 0,
        'mission': 0,
    }
    
    # Calcul des icônes par type si les colonnes existent
    for icon_type in total_icons_by_type.keys():
        icon_column = f'{icon_type}_icons_count'
        if icon_column in annotations:
            total_icons_by_type[icon_type] = annotations[icon_column].sum()

    # Calcul du ratio d'icônes
    total_icons_count = annotations['total_icons_count'].sum() if 'total_icons_count' in annotations else 1
    icon_type_ratio = {
        k: (v / total_icons_count * 100 if total_icons_count > 0 else 0)
        for k, v in total_icons_by_type.items()
    }

    # Calcul des ratios de tags
    total_tags_count = annotations['total_tags_count'].sum() if 'total_tags_count' in annotations else 1
    tag_type_ratio = {
        'positive': 0,
        'negative': 0,
        'aesthetic': 0,
        'social': 0,
    }
    for tag_type in tag_type_ratio.keys():
        tag_column = f'{tag_type}_tags_count'
        if tag_column in annotations:
            tag_type_ratio[tag_type] = annotations[tag_column].sum() / total_tags_count * 100 if total_tags_count > 0 else 0

    # Calcul des émoticônes
    emoticons = annotations['emoticon'].dropna().tolist() if 'emoticon' in annotations else []
    emoticon_top3 = sorted(set(emoticons), key=emoticons.count, reverse=True)[:3] if emoticons else [None, None, None]
    emoticonality_avg = annotations['emoticonality'].mean() * 100 if 'emoticonality' in annotations and not annotations['emoticonality'].isnull().all() else 0

    # Calcul de l'utilisation des types de lieux (placeType)
    place_type_usage = {
        'here': 0,
        'close': 0,
        'district': 0,
        'city': 0,
        'tour': 0
    }
    if 'placeType' in annotations:
        place_type_counts = annotations['placeType'].value_counts(normalize=True) * 100
        for place in place_type_usage.keys():
            if place in place_type_counts:
                place_type_usage[place] = place_type_counts[place]

    # Calcul de la fréquence d'expérience (timing)
    experience_frequency = {
        'UNIQUE': 0,
        'REGULAR': 0,
        'OCCASIONAL': 0
    }
    if 'timing' in annotations:
        timing_counts = annotations['timing'].value_counts(normalize=True) * 100
        for timing in experience_frequency.keys():
            if timing in timing_counts:
                experience_frequency[timing] = timing_counts[timing]

    # Calcul des zones couvertes
    covered_zones = calculate_covered_zones(annotations)

    # Retour des métriques calculées avec les préfixes
    return {
        f'{prefix}items_count': items_count,
        f'{prefix}avg_volume_lexical': avg_volume_lexical,
        f'{prefix}avg_icons_per_annotation': avg_icons_per_annotation,
        f'{prefix}avg_types_icons': avg_types_icons,
        f'{prefix}icon_type_ratio': icon_type_ratio,
        f'{prefix}avg_tags_per_annotation': annotations['total_tags_count'].mean() if 'total_tags_count' in annotations and not annotations['total_tags_count'].isnull().all() else 0,
        f'{prefix}tag_type_ratio': tag_type_ratio,
        f'{prefix}emoticon_top3': emoticon_top3,
        f'{prefix}emoticonality_avg': emoticonality_avg,
        f'{prefix}place_type_usage': place_type_usage,
        f'{prefix}experience_frequency': experience_frequency,
        f'{prefix}covered_zones': covered_zones,
    }



def calculate_pca(user_id):
    annotations = fetch_user_annotations(user_id)
    return calculate_profile_metrics(annotations, prefix='pca_')

def calculate_pcoa(user_id):
    ids = fetch_user_viewed_annotations(user_id)
    annotations =  fetch_annotations_by_id(ids)
    return calculate_profile_metrics(annotations, prefix='pcoa_')

import json

def calculate_tour_metrics(tours, prefix):
    total_points = 0
    total_annotations = 0
    text_lengths = []
    zones_covered = set()  # Set pour éviter les doublons de zones
    tour_types = {}
    annotation_frequency = {}

    for index, tour in tours.iterrows():
        coords = tour['body']  # Liste des coordonnées [lon1, lat1, lon2, lat2, ...]
        # Si coords est une chaîne de caractères, la convertir en liste
        if isinstance(coords, str):
            try:
                coords = json.loads(coords)  # Supposons que c'est du JSON
            except json.JSONDecodeError:
                # Si la chaîne n'est pas du JSON, la transformer manuellement si nécessaire
                coords = list(map(float, coords.strip('[]').split(',')))

        num_points = len(coords) // 2  # Chaque point est une paire [lon, lat]
        total_points += num_points
        
        # Ajouter les zones couvertes par les coordonnées
        for i in range(0, len(coords), 2):
            zones_covered.add((coords[i], coords[i + 1]))

        # Compter les types de parcours (share)
        tour_type = tour['share']
        if tour_type in tour_types:
            tour_types[tour_type] += 1
        else:
            tour_types[tour_type] = 1

        # Calcul de la longueur de texte des messages associés
        if 'messages' in tour and tour['messages']:
            text_lengths.append(len(tour['messages']))

        # Fréquence des annotations
        if 'annotations' in tour and tour['annotations']:
            try:
                # Si les annotations sont une chaîne de caractères JSON, on les désérialise
                if isinstance(tour['annotations'], str):
                    annotations = json.loads(tour['annotations'])
                else:
                    annotations = tour['annotations']
                
                # Compter les annotations
                total_annotations += len(annotations)
                for annotation in annotations:
                    if annotation in annotation_frequency:
                        annotation_frequency[annotation] += 1
                    else:
                        annotation_frequency[annotation] = 1
            except json.JSONDecodeError:
                print(f"Annotations malformées dans la ligne {index}: {tour['annotations']}")
    
    avg_points = total_points / len(tours) if len(tours) > 0 else 0
    point_density = total_points / len(zones_covered) if zones_covered else 0
    avg_volume_lexical = sum(text_lengths) / len(text_lengths) if text_lengths else 0
    tour_type_proportion = {k: v / len(tours) for k, v in tour_types.items()} if len(tours) > 0 else {}
    items_count = len(tours)

    print('annotation_frequency: ', annotation_frequency)

    
    return {
        f'{prefix}items_count': items_count,
        f'{prefix}avg_points': avg_points,
        f'{prefix}point_density': point_density,
        f'{prefix}frequent_geo_zones': list(zones_covered),  # Convertir en liste
        f'{prefix}tour_type_proportion': tour_type_proportion,
        f'{prefix}avg_volume_lexical': avg_volume_lexical,
        f'{prefix}annotation_frequency': annotation_frequency,
        f'{prefix}covered_zones': list(zones_covered),
    }

def calculate_pcp(user_id):
    paths = fetch_user_tours(user_id)
    if paths.empty:
        return {
            'pcp_items_count': 0,
            'pcp_avg_points': 0,
            'pcp_point_density': 0,
            'pcp_frequent_geo_zones': [],  # Convertir en liste
            'pcp_tour_type_proportion': {},
            'pcp_avg_volume_lexical': 0,
            'pcp_annotation_frequency': {},
            'pcp_covered_zones': [],
        }

    # Utiliser la fonction commune pour calculer les métriques avec préfixe 'pcp_'
    return calculate_tour_metrics(paths, prefix='pcp_')

def calculate_pcop(user_id):
    # Récupérer uniquement les parcours consultés
    paths = fetch_user_tours(user_id)
    if paths.empty:
        return {
            'pcop_items_count': 0,
            'pcop_avg_points': 0,
            'pcop_point_density': 0,
            'pcop_frequent_geo_zones': [],  # Convertir en liste
            'pcop_tour_type_proportion': {},
            'pcop_avg_volume_lexical': 0,
            'pcop_annotation_frequency': {},
            'pcop_covered_zones': [],
        }

    # Utiliser la fonction commune pour calculer les métriques avec préfixe 'pcop_'
    return calculate_tour_metrics(paths, prefix='pcop_')


def save_user_profile(user_id, pca, pcoa, pcp, pcop):
    profile = UserProfile.query.get(user_id)
    if not profile:
        profile = UserProfile(user_id=user_id)
    
    # Dictionnaires des données à combiner
    data_sources = {
        'pca': pca,
        'pcoa': pcoa,
        'pcp': pcp,
        'pcop': pcop
    }
    
    # Parcourir chaque source de données et mettre à jour le profil
    for prefix, data in data_sources.items():
        for key, value in data.items():
            if value is not None:  # Vérifiez que la valeur n'est pas nulle
                # print(f"Setting {key} to {value}") 
                setattr(profile, f"{key}", value)
    
    # Sauvegarder le profil dans la base de données
    db.session.add(profile)
    db.session.commit()

    # print('Complete profile:', profile.to_dict())

    return profile




def calculate_users_profiles():
    user_ids = fetch_user_ids()
    # Pour tester
    for user in user_ids:  
        # Calculer PCA
        pca = calculate_pca(user)
        # print('PCA for user {}: {}'.format(user, pca))
        # return
        
        # Calculer PCoA
        pcoa = calculate_pcoa(user)
        # print('PCoA for user {}: {}'.format(user, pcoa))
        
        # Calculer PCP
        pcp = calculate_pcp(user)
        # print('PCP for user {}: {}'.format(user, pcp))
        
        # Calculer PCoP
        pcop = calculate_pcop(user)
        # print('PCoP for user {}: {}'.format(user, pcop))
        
        # Enregistrer ou mettre à jour le profil utilisateur
        save_user_profile(user, pca, pcoa, pcp, pcop)

