from .db_operations import connect_to_local_database
from sqlalchemy import MetaData, Table, Column, Integer, String

def generate_and_save_recommendations():
    try:
        db_session = connect_to_local_database()

        metadata = MetaData()

        recommendations_table = Table('recommendations', metadata,
                                      Column('post_id', Integer, primary_key=True),
                                      Column('recommendation', String(length=255)),
                                      extend_existing=True)

        metadata.create_all(db_session.bind, checkfirst=True)

        query = "SELECT post_id, volume_lexical, diversity_lexical FROM indicators"
        result = db_session.execute(query)

        recommendations = []
        for row in result:
            post_id = row['post_id']
            volume_lexical = row['volume_lexical']
            diversity_lexical = row['diversity_lexical']

            if volume_lexical > 100:
                recommendation = f"Post {post_id}: High volume lexical content."
            elif diversity_lexical < 0.2:
                recommendation = f"Post {post_id}: Low lexical diversity."
            else:
                recommendation = f"Post {post_id}: Keep up the good work!"

            recommendations.append({'post_id': post_id, 'recommendation': recommendation})

        for recommendation in recommendations:
            db_session.execute(recommendations_table.insert().prefix_with("IGNORE"), recommendation)

        db_session.commit()
        print("Recommendations generated and saved to database successfully.")

    except Exception as e:
        print(f"Error generating or saving recommendations: {e}")
    finally:
        db_session.close()
