import re
from dateutil import parser
import pandas as pd
import requests
from sqlalchemy import Text, create_engine, MetaData, Table, Column, Integer, String, Float, JSON, DateTime, inspect, text
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.engine import reflection
from flask import current_app
from datetime import datetime, timedelta
from geopy.distance import geodesic

import json

# Authentication function
def get_auth_token(email, password):
    login_url = current_app.config['MOBILES_API_URL'] +"login_check"
    login_data = {"mail": email, "password": password}
    try:
        response = requests.post(login_url, json=login_data)
        response.raise_for_status()
        return response.json().get("token")
    except requests.RequestException as err:
        print(f"Erreur lors de la connexion : {err}")
        return None

# Function to fetch users from remote API
def fetch_data_from_api(api_url, auth_token, limit=50):
    headers = {"Authorization": f"Bearer {auth_token}"}
    data = []
    fetched_ids = set()
    page = 1

    while True:
        try:
            params = {"page": page, "limit": limit}
            response = requests.get(api_url, headers=headers, params=params)
            response.raise_for_status()
            response_data = response.json().get("hydra:member", [])

            if not response_data:
                break

            new_data_count = 0
            for item in response_data:
                item_id = item.get("id")
                if item_id in fetched_ids:
                    break
                fetched_ids.add(item_id)
                data.append(item)
                new_data_count += 1

            if new_data_count == 0 or len(response_data) < limit:
                break

            page += 1
        except requests.RequestException as err:
            print(f"Erreur: {err}")
            return []
    
    print(f"Total items fetched: {len(data)}")
    return data

def get_distant_users():
    auth_token = get_auth_token('madjid.sadallah@liris.cnrs.fr', '!MSada2024')
    if auth_token:
        api_url = current_app.config['MOBILES_API_URL'] +"users"
        return fetch_data_from_api(api_url, auth_token)
    return []

def get_distant_annotations():
    auth_token = get_auth_token('madjid.sadallah@liris.cnrs.fr', '!MSada2024')
    if auth_token:
        api_url = current_app.config['MOBILES_API_URL'] +"annotations"
        annotations = fetch_data_from_api(api_url, auth_token)

        # Extraire l'ID de l'utilisateur des annotations
        for annotation in annotations:
            user_url = annotation.get('user')  # Assurez-vous que 'user' est la clé appropriée
            if user_url:
                # Utiliser une expression régulière pour extraire l'ID
                match = re.search(r'/api/users/(\d+)', user_url)
                if match:
                    user_id = match.group(1)
                    annotation['user'] = user_id  # Ajouter l'ID de l'utilisateur au dictionnaire
            
            tour_url = annotation.get('tour')  # Assurez-vous que 'user' est la clé appropriée
            if tour_url:
                # Utiliser une expression régulière pour extraire l'ID
                match = re.search(r'/api/tours/(\d+)', tour_url)
                if match:
                    tour_id = match.group(1)
                    annotation['tour'] = tour_id  # Ajouter l'ID de l'utilisateur au dictionnaire
            
            messages_urls = annotation.get('messages', [])
            if messages_urls:
                annotation['messages'] = [re.search(r'/api/messages/(\d+)', url).group(1) for url in messages_urls if re.search(r'/api/messages/(\d+)', url)]
        return annotations
    return []

def get_distant_tours():
    auth_token = get_auth_token('madjid.sadallah@liris.cnrs.fr', '!MSada2024')
    if auth_token:        
        api_url = current_app.config['MOBILES_API_URL'] +"tours"
        tours = fetch_data_from_api(api_url, auth_token)
        for tour in tours:
            user_url = tour.get('user')  # Assurez-vous que 'user' est la clé appropriée
            if user_url:
                # Utiliser une expression régulière pour extraire l'ID
                match = re.search(r'/api/users/(\d+)', user_url)
                if match:
                    user_id = match.group(1)
                    tour['user'] = user_id  # Ajouter l'ID de l'utilisateur au dictionnaire         
            annotation_urls = tour.get('annotations', [])
            if annotation_urls:
                tour['annotations'] = [re.search(r'/api/annotations/(\d+)', url).group(1) for url in annotation_urls if re.search(r'/api/annotations/(\d+)',url)]

            linked_annotations_urls = tour.get('linked_annotations', [])
            if linked_annotations_urls:
                tour['linked_annotations'] = [re.search(r'/api/annotations/(\d+)', url).group(1) for url in linked_annotations_urls if re.search(r'/api/annotations/(\d+)',url)]

            messages_urls = tour.get('messages', [])
            if messages_urls:
                tour['messages'] = [re.search(r'/api/messages/(\d+)', url).group(1) for url in messages_urls if re.search(r'/api/messages/(\d+)',url)]
        return tours
    return []

def get_distant_messages():
    auth_token = get_auth_token('madjid.sadallah@liris.cnrs.fr', '!MSada2024')
    if auth_token:
        api_url =  current_app.config['MOBILES_API_URL'] +"messages"
        messages = fetch_data_from_api(api_url, auth_token)
        for msg in messages:
            user_url = msg.get('user')  # Assurez-vous que 'user' est la clé appropriée
            if user_url:
            # Utiliser une expression régulière pour extraire l'ID
                match = re.search(r'/api/users/(\d+)', user_url)
                if match:
                    user_id = match.group(1)
                    msg['user'] = user_id  # Ajouter l'ID de l'utilisateur au dictionnaire
            annotation_url = msg.get('annotation')  # Assurez-vous que 'user' est la clé appropriée
            if annotation_url:
            # Utiliser une expression régulière pour extraire l'ID
                match = re.search(r'/api/annotations/(\d+)', annotation_url)
                if match:
                    annotation_id = match.group(1)
                    msg['annotation'] = annotation_id  # Ajouter l'ID de l'utilisateur au dictionnaire
        return messages
    return []

# Database connection function
def connect_to_local_database():
    db_url = current_app.config['SQLALCHEMY_DATABASE_URI']
    try:
        engine = create_engine(db_url, echo=False)
        Session = sessionmaker(bind=engine)
        return Session(), engine
    except Exception as e:
        print(f"Error connecting to database: {e}")
        return None, None

# Utility to create or update a table
def create_or_update_table(engine, table_name, data, string_length=255, ignore_columns=None):
    """
    Crée ou met à jour une table dans la base de données en tolérant les valeurs NULL et en ignorant certaines colonnes.

    :param engine: L'objet Engine SQLAlchemy.
    :param table_name: Le nom de la table.
    :param data: Les données pour initialiser la table.
    :param string_length: Longueur maximale pour les colonnes de type String.
    :param ignore_columns: Liste des colonnes à ignorer lors de la création/mise à jour.
    """
    if ignore_columns is None:
        ignore_columns = []

    try:
        # Détermine les types de colonnes en fonction des données fournies
        column_types = {
            key: (Text if key == 'devices' or key == 'comment' or key == 'body' else (String(length=string_length) if isinstance(value, str) else 
                 Float if isinstance(value, (int, float)) else 
                 JSON))
            for item in data for key, value in item.items()
            if key not in ignore_columns  # Ignore les colonnes spécifiées
        }
        column_types['id'] = Integer  # Ajoute l'ID en tant que colonne Integer

        # Définition des colonnes pour la table avec tolérance pour les valeurs NULL
        columns = [Column(name, column_type, nullable=True) for name, column_type in column_types.items()]
        metadata = MetaData()
        table = Table(table_name, metadata, *columns, extend_existing=True)

        # Vérifie si la table existe déjà dans la base de données
        inspector = inspect(engine)
        table_exists = inspector.has_table(table_name)

        # Si la table n'existe pas, crée-la
        if not table_exists:
            metadata.create_all(engine)  # Crée la table
        else:
            # Obtenez les colonnes existantes et comparez-les avec les nouvelles
            existing_columns = set(col['name'] for col in inspector.get_columns(table_name))
            new_columns = set(column_types.keys()) - existing_columns
            with engine.connect() as conn:
                for new_column in new_columns:
                    # Créer une instance du type de colonne et compiler pour le dialecte SQL spécifique
                    column_type_instance = column_types[new_column]
                    if new_column not in ignore_columns:  # Ignore les colonnes spécifiées
                        alter_stmt = f"ALTER TABLE {table_name} ADD COLUMN {new_column} {column_type_instance}"
                        conn.execute(text(alter_stmt))

        # Utiliser une session transactionnelle pour insérer les données
        Session = sessionmaker(bind=engine)
        session = Session()

        # Suppression des anciennes données
        try:
            session.execute(table.delete())  # Supprime toutes les lignes de la table
            session.commit()  # Valide la suppression
            print(f"Anciennes données supprimées de la table '{table_name}'.")
        except SQLAlchemyError as e:
            session.rollback()
            print(f"Erreur lors de la suppression des anciennes données dans la table {table_name}: {e}")
        
        # Préparez les données avec des valeurs NULL pour les colonnes manquantes
        for item in data:
            for column in table.columns.keys():
                if column not in item:
                    item[column] = None  # Affectez None si la colonne est manquante

        try:
            session.execute(table.insert(), data)  # Insère les nouvelles données
            session.commit()  # Valide la transaction
            print(f"Table '{table_name}' créée ou mise à jour avec succès et données insérées.")

        except SQLAlchemyError as e:
            session.rollback()  # Annule la transaction en cas d'erreur
            print(f"Une erreur s'est produite lors de l'insertion des données dans la table {table_name} : {e}")
        finally:
            session.close()

    except SQLAlchemyError as e:
        print(f"Une erreur s'est produite lors de la création ou de la mise à jour de la table {table_name} : {e}")


        
def old_create_or_update_table(engine, table_name, data):
    try:
        column_types = {key: String(length=255) if isinstance(value, str) else Float if isinstance(value, (int, float)) else JSON
                        for item in data for key, value in item.items()}
        column_types['id'] = Integer

        columns = [Column(name, column_types[name]) for name in column_types.keys()]
        table = Table(table_name, MetaData(), *columns, extend_existing=True)

        inspector = reflection.Inspector.from_engine(engine)
        table_exists = table_name in inspector.get_table_names()

        if not table_exists:
            table.create(engine, checkfirst=True)
        else:
            existing_columns = set(col['name'] for col in inspector.get_columns(table_name))
            new_columns = set(column_types.keys()) - existing_columns
            with engine.connect() as conn:
                for new_column in new_columns:
                    print(new_column)
                    if new_column != '@type':
                        alter_stmt = f"ALTER TABLE {table_name} ADD COLUMN {new_column} {column_types[new_column].compile(dialect=engine.dialect)}"
                        conn.execute(text(alter_stmt))

        with engine.connect() as conn:
            conn.execute(table.delete())
            conn.execute(table.insert().prefix_with("IGNORE"), data)

        print(f"Table '{table_name}' créée ou mise à jour avec succès.")
    except SQLAlchemyError as e:
        print(f"Une erreur s'est produite lors de la création ou de la mise à jour de la table {table_name} : {e}")

def save_users_to_local_database(users):
    session, engine = connect_to_local_database()
    if session and engine:
        create_or_update_table(engine, 'users', users, ignore_columns=['@type','@id','testgroup'])
        session.close()

def save_annotations_to_local_database(annotations):
    session, engine = connect_to_local_database()
    if session and engine:
        create_or_update_table(engine, 'annotations', annotations, ignore_columns=['@type','@id','testgroup'])
        session.close()

def save_tours_to_local_database(annotations):
    session, engine = connect_to_local_database()
    if session and engine:
        create_or_update_table(engine, 'tours', annotations, ignore_columns=['@type','@id'])
        session.close()

def save_messages_to_local_database(messages):
    session, engine = connect_to_local_database()
    if session and engine:
        create_or_update_table(engine, 'messages', messages, ignore_columns=['@type','@id'])
        session.close()

def format_date(date_string):
    try:
        parsed_date = parser.parse(date_string)
        return parsed_date.strftime('%Y-%m-%d')
    except (ValueError, TypeError) as e:
        print(f"Erreur lors du formatage de la date: {e}")
        return None

# Insert functions
def insert_data(table_name, data):
    session, engine = connect_to_local_database()
    if not session or not engine:
        print('There is some internal error')
        return
    
    table = Table(table_name, MetaData(), autoload_with=engine)
    session.execute(table.insert().values(**data))
    session.commit()
    session.close()

def insert_indicator(user_id, category, strategy, indicator_type, value):
    # print("User: ",user_id," - category :", category, " - strategy: ", strategy, " - type: ", indicator_type, " - value: ", value)
    # return

    # Convertir les valeurs en JSON si nécessaire
    if isinstance(value, list):
        value = json.dumps(value)
    data = {
        'user_id': user_id,
        'category': category,
        'strategy': strategy,
        'type': indicator_type,
        'value': value,
        'date': datetime.utcnow()
    }
    insert_data('indicators', data)

def insert_recommendation(user_id, category, strategy, title, recommendation, suggestion, suggestion_type):
    # Écarter les anciennes recommandations de la même stratégie pour cet utilisateur
    discard_old_recommendations(user_id, strategy)

    # Préparer les données de la nouvelle recommandation
    data = {
        'user_id': user_id,
        'category': category,
        'strategy': strategy,
        'title': title,
        'recommendation': recommendation,
        'suggestion': suggestion,
        'suggestion_type': suggestion_type,
        'created_at': datetime.utcnow(),
        'status': 'Created'  # Nouvel ajout : définir le statut à 'Created'
    }
    
    # Insérer la nouvelle recommandation
    insert_data('recommendations', data)


def discard_old_recommendations(user_id, strategy):
    """
    Marque toutes les recommandations existantes d'un utilisateur avec la même stratégie comme 'discarded'.

    :param user_id: L'ID de l'utilisateur pour lequel les recommandations doivent être écartées.
    :param strategy: La stratégie des recommandations à écarter.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return False

    try:
        # Récupérer la table 'recommendations'
        metadata = MetaData()
        recommendations_table = Table('recommendations', metadata, autoload_with=engine)

        # Récupérer les recommandations à écarter
        select_query = (
            recommendations_table.select()
            .where((recommendations_table.c.user_id == user_id) & 
                   (recommendations_table.c.strategy == strategy) & 
                   (recommendations_table.c.status != 'discarded'))  # Éviter de sélectionner celles déjà 'discarded'
        )
        
        # Exécuter la requête de sélection
        result = session.execute(select_query)
        recommendations_to_discard = result.fetchall()

        # Écarter chaque recommandation trouvée
        for recommendation in recommendations_to_discard:
            update_recommendation_status(recommendation.id, 'discarded')

        session.close()

        print(f"Les recommandations avec la stratégie '{strategy}' pour l'utilisateur {user_id} ont été marquées comme 'discarded'.")
        return True
    except SQLAlchemyError as e:
        print(f"Erreur lors de l'écartement des recommandations: {e}")
        session.rollback()
        session.close()
        return False



def fetch_users_with_pending_recommendations():
    """
    Récupère tous les utilisateurs ayant des recommandations à analyser, 
    c'est-à-dire des recommandations avec le statut 'Created' ou 'Discarded'.
    
    :return: DataFrame contenant les utilisateurs avec des recommandations à analyser
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        # Charger la table des recommandations
        metadata = MetaData()
        recommendations_table = Table('recommendations', metadata, autoload_with=engine)

        # Créer la requête pour récupérer les utilisateurs avec des recommandations 'Created' ou 'Discarded'
        query = session.query(recommendations_table.c.user_id).filter(
            recommendations_table.c.status.in_(["Created", "Discarded"])
        ).distinct()  # Utiliser distinct() pour éviter les doublons d'utilisateurs

        # Exécuter la requête et récupérer les données dans un DataFrame
        result = session.execute(query)
        data = pd.DataFrame(result.fetchall(), columns=["user_id"])

        session.close()
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des utilisateurs avec des recommandations à analyser: {e}")
        session.close()
        return []

def insert_impact(user_id, category, strategy, analysis_type, result):
    data = {
        'user_id': user_id,
        'category': category,
        'strategy': strategy,
        'analysis_type': analysis_type,
        'result': result,
        'date': datetime.utcnow()
    }
    insert_data('impacts', data)

# Fetch data functions
# def fetch_data(table_name):
#     session, engine = connect_to_local_database()
#     if not session or not engine:
#         return []

#     table = Table(table_name, MetaData(), autoload_with=engine)
#     query = session.query(table)
#     data = pd.read_sql(query.statement, engine)
#     session.close()
#     return data

def fetch_data(table_name, exclude_fields=None, include_fields=None, field_filters_to_include=None, field_filters_to_exclude=None):
    """
    Récupère des données à partir d'une table spécifiée en argument.
    Args:
        table_name (str): Nom de la table.
        exclude_fields (list, optional): Liste des champs à exclure. Par défaut, None.
        include_fields (list, optional): Liste des champs à inclure. Par défaut, None.
        field_filters_to_include (dict, optional): Filtres d'inclusion pour les champs spécifiques. Par défaut, None.
            Exemple : {'champ_X': ['VAL1', 'VAL2'], 'champ_Y': ['AUTRE_VAL']}
        field_filters_to_exclude (dict, optional): Filtres d'exclusion pour les champs spécifiques. Par défaut, None.
            Exemple : {'champ_A': ['EXCL_VAL1', 'EXCL_VAL2']}
    Returns:
        pandas.DataFrame: Les données récupérées.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return pd.DataFrame()

    table = Table(table_name, MetaData(), autoload_with=engine)
    query = session.query(table)

    if exclude_fields:
        for field in exclude_fields:
            query = query.filter(getattr(table.c, field) != None)

    if include_fields:
        for field in include_fields:
            query = query.filter(getattr(table.c, field) != None)

    if field_filters_to_include:
        for field, values in field_filters_to_include.items():
            query = query.filter(getattr(table.c, field).in_(values))

    if field_filters_to_exclude:
        for field, values in field_filters_to_exclude.items():
            query = query.filter(~getattr(table.c, field).in_(values))

    data = pd.read_sql(query.statement, engine)
    session.close()
    return data

def fetch_annotations_by_user(user_id, since_date=None):
    
    """
    Récupère les annotations faites par un utilisateur, optionnellement filtrées depuis une certaine date.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les annotations sont considérées (facultatif).
    :return: DataFrame contenant les annotations.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return pd.DataFrame()

    annotations_table = Table('annotations', MetaData(), autoload_with=engine)
    
    
    # Construire la requête
    query = session.query(annotations_table).filter_by(user=user_id)
    
    
    if since_date:
        query = query.filter(annotations_table.c.date >= since_date)
   
    # Exécuter la requête et charger les résultats dans un DataFrame
    data = pd.read_sql(query.statement, engine)
    
    session.close()
    
    return data

def fetch_annotations_by_id(ids):
    session, engine = connect_to_local_database()
    if not session or not engine:
        return pd.DataFrame()

    annotations_table = Table('annotations', MetaData(), autoload_with=engine)

    # Vérifier si ids est un seul ID et le convertir en liste
    if isinstance(ids, int):
        ids = [ids]
    
    
    # Construire la requête
    query = session.query(annotations_table).filter(annotations_table.c.id.in_(ids))
    
    # Exécuter la requête et charger les résultats dans un DataFrame
    data = pd.read_sql(query.statement, engine)
    
    session.close()
    
    return data




def fetch_new_annotations(since_date=None):
    """
    Récupère les annotations récentes à partir d'une date donnée ou des 7 derniers jours si aucune date n'est fournie.
    
    :param since_date: Date depuis laquelle les annotations sont considérées comme nouvelles.
    :return: DataFrame contenant les annotations récentes.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return pd.DataFrame()

    annotations_table = Table('annotations', MetaData(), autoload_with=engine)
    
    # Si since_date est None, prendre les 7 derniers jours
    if since_date is None:
        since_date = datetime.now() - timedelta(days=7)
    
    # Construire la requête pour récupérer les annotations récentes
    query = session.query(annotations_table).filter(annotations_table.c.date >= since_date)
    
    # Exécuter la requête et charger les résultats dans un DataFrame
    data = pd.read_sql(query.statement, engine)
    
    session.close()
    engine.dispose()
    
    return data




def get_user_registration_date(user_id):
    session, engine = connect_to_local_database()
    if not session or not engine:
        return None

    try:
        user_table = Table('users', MetaData(), autoload_with=engine)
        query = session.query(user_table.c.date).filter_by(id=user_id).first()
        session.close()


        if query and query[0]:
            return format_date(query[0])
        else:
            print(f"Aucune date d'enreistrement trouvée pour l'utilisateur {user_id}")
            return None
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération de la date d'inscription de l'utilisateur {user_id}: {e}")
        session.close()
        return None

def fetch_users():
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        user_table = Table('users', MetaData(), autoload_with=engine)
        query = session.query(user_table)
        data = pd.read_sql(query.statement, engine)
        session.close()
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des utilisateurs: {e}")
        session.close()
        return []

def fetch_annotations(exclude_fields=None, include_fields=None, field_filters_to_include=None, field_filters_to_exclude=None):
    """
    Récupère des annotations en utilisant la fonction fetch_data avec des filtres.
    Args:
        exclude_fields (list, optional): Liste des champs à exclure. Par défaut, None.
        include_fields (list, optional): Liste des champs à inclure. Par défaut, None.
        field_filters_to_include (dict, optional): Filtres d'inclusion pour les champs spécifiques. Par défaut, None.
            Exemple : {'champ_X': ['VAL1', 'VAL2'], 'champ_Y': ['AUTRE_VAL']}
        field_filters_to_exclude (dict, optional): Filtres d'exclusion pour les champs spécifiques. Par défaut, None.
            Exemple : {'champ_A': ['EXCL_VAL1', 'EXCL_VAL2']}
    Returns:
        pandas.DataFrame: Les données récupérées.
    """
    try:
        data = fetch_data('annotations',
                          exclude_fields=exclude_fields,
                          include_fields=include_fields,
                          field_filters_to_include=field_filters_to_include,
                          field_filters_to_exclude=field_filters_to_exclude)
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des annotations : {e}")
        return []


def fetch_tours(exclude_fields=None, include_fields=None, 
                field_filters_to_include=None, field_filters_to_exclude=None):
    """
    Récupère des tours en utilisant la fonction fetch_data avec des filtres.
    
    Args:
        exclude_fields (list, optional): Liste des champs à exclure. Par défaut, None.
        include_fields (list, optional): Liste des champs à inclure. Par défaut, None.
        field_filters_to_include (dict, optional): Filtres d'inclusion pour les champs spécifiques. Par défaut, None.
        field_filters_to_exclude (dict, optional): Filtres d'exclusion pour les champs spécifiques. Par défaut, None.
        
    Returns:
        pandas.DataFrame: Les données récupérées.
    """
    try:
        data = fetch_data('tours',
                          exclude_fields=exclude_fields,
                          include_fields=include_fields,
                          field_filters_to_include=field_filters_to_include,
                          field_filters_to_exclude=field_filters_to_exclude)
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des tours : {e}")
        return []

def fetch_comments():
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        comment_table = Table('comments', MetaData(), autoload_with=engine)
        query = session.query(comment_table)
        data = pd.read_sql(query.statement, engine)
        session.close()
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des comments: {e}")
        session.close()
        return []
    
def fetch_notifications():
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        notifications_table = Table('notifications', MetaData(), autoload_with=engine)
        query = session.query(notifications_table)
        data = pd.read_sql(query.statement, engine)
        session.close()
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des notifications: {e}")
        session.close()
        return []

def fetch_recommendations(user_id=None):
    """
    Récupère les recommandations de la base de données. Si user_id est spécifié,
    la fonction filtre les recommandations pour cet utilisateur.
    
    :param user_id: ID de l'utilisateur (optionnel)
    :return: DataFrame contenant les recommandations
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        notifications_table = Table('recommendations', MetaData(), autoload_with=engine)
        query = session.query(notifications_table)

        # Si un user_id est fourni, on filtre par cet utilisateur
        if user_id is not None:
            query = query.filter(notifications_table.c.user_id == user_id)

        # Exécuter la requête et récupérer les données dans un DataFrame
        data = pd.read_sql(query.statement, engine)
        
        session.close()
        return data
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des recommandations: {e}")
        session.close()
        return []


def update_recommendation_status(recommendation_id, new_status):
    """
    Met à jour le statut d'une recommandation spécifique en fonction de l'ID de la recommandation.
    
    :param recommendation_id: ID de la recommandation à mettre à jour.
    :param new_status: Le nouveau statut à attribuer à la recommandation.
    :return: True si la mise à jour a réussi, False sinon.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return False

    try:
        # Récupérer la table 'recommendations'
        metadata = MetaData()
        recommendations_table = Table('recommendations', metadata, autoload_with=engine)

        # Mettre à jour le statut et la date de mise à jour du statut
        update_query = (
            recommendations_table.update()
            .where(recommendations_table.c.id == recommendation_id)
            .values(status=new_status, status_updated_at=datetime.utcnow())
        )

        # Exécuter la requête de mise à jour
        session.execute(update_query)
        session.commit()
        session.close()

        print(f"Le statut de la recommandation {recommendation_id} a été mis à jour en '{new_status}'.")
        return True
    except SQLAlchemyError as e:
        print(f"Erreur lors de la mise à jour du statut de la recommandation: {e}")
        session.rollback()
        session.close()
        return False
  


def fetch_user_annotations(user_id, since_date=None):
    """
    Récupère les annotations faites par un utilisateur depuis une date donnée ou toutes les annotations si aucune date n'est fournie.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les annotations sont considérées (doit être un objet datetime ou None).
    :return: Liste des annotations avec leurs dates de création et de modification.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        annotations_table = Table('annotations', MetaData(), autoload_with=engine)

        # Construire la requête en fonction de la présence ou non de since_date
        if since_date is None:
            # Prendre toutes les annotations si aucune date n'est fournie
            query = session.query(annotations_table).filter(annotations_table.c.user == user_id)
        else:
            # Vérifiez que since_date est un objet datetime valide
            if isinstance(since_date, datetime):
                query = (
                    session.query(annotations_table)
                    .filter(annotations_table.c.user == user_id)
                    .filter(
                        (annotations_table.c.date >= since_date) | 
                        (annotations_table.c.dateModif >= since_date)
                    )
                )
            else:
                # Si since_date n'est pas un datetime, lancer une exception
                raise ValueError("since_date doit être un objet datetime valide. Voici son contenu: ", since_date)

        # Exécuter la requête et récupérer les résultats sous forme de DataFrame
        result = pd.read_sql(query.statement, engine)

    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des annotations: {e}")
        result = []
    except ValueError as ve:
        print(f"Erreur de validation: {ve}")
        result = []
    finally:
        session.close()
        engine.dispose()

    return result


def fetch_user_tours(user_id, since_date=None):
    
    """
    Récupère les tours faites par un utilisateur, optionnellement filtrées depuis une certaine date.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les annotations sont considérées (facultatif).
    :return: DataFrame contenant les annotations.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return pd.DataFrame()

    tour_table = Table('tours', MetaData(), autoload_with=engine)
    
    
    # Construire la requête
    query = session.query(tour_table).filter_by(user=user_id)
    
    
    if since_date:
        query = query.filter(tour_table.c.date >= since_date)
   
    # Exécuter la requête et charger les résultats dans un DataFrame
    data = pd.read_sql(query.statement, engine)
    
    session.close()
    
    return data

def fetch_user_comments(user_id, since_date):
    """
    Récupère les commentaires faits par un utilisateur depuis une date donnée.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les commentaires sont considérés.
    :return: Liste des commentaires avec leurs dates de création.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        comments_table = Table('messages', MetaData(), autoload_with=engine)
        query = (
            session.query(comments_table.c.id, comments_table.c.annotation, comments_table.c.date)
            .filter(comments_table.c.user == '/api/users/'+user_id)
            .filter(comments_table.c.date >= since_date)
        )
        result = pd.read_sql(query.statement, engine)
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des commentaires: {e}")
        result = []
    finally:
        session.close()
        engine.dispose()

    return result

def fetch_annotation_comments(annotation_id):
    """
    Récupère les commentaires faits sur une annotation.
    
    :param annotation_id: ID de l'annotation.
    :return: Liste des commentaires (IDs)
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        comments_table = Table('messages', MetaData(), autoload_with=engine)
        query = (
            session.query(comments_table.c.id)
            .filter(comments_table.c.annotation == annotation_id)
        )
        result = pd.read_sql(query.statement, engine)
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des commentaires: {e}")
        result = []
    finally:
        session.close()
        engine.dispose()

    return result

def fetch_annotations_around_location(location, radius_km=5):
    """
    Récupère les annotations autour d'une localisation donnée dans la base de données.
    
    :param location: Dictionnaire contenant les coordonnées avec les clés 'lat' et 'lon'.
    :param radius_km: Rayon autour de la localisation pour la recherche des annotations en kilomètres.
    :return: Liste des annotations trouvées.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        annotations_table = Table('annotations', MetaData(), autoload_with=engine)
        query = (
            session.query(annotations_table.c.id, annotations_table.c.coords)
            .filter(annotations_table.c.coords.isnot(None))
        )
        result = pd.read_sql(query.statement, engine)
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des annotations: {e}")
        result = pd.DataFrame(columns=['id', 'coords'])
    finally:
        session.close()
        engine.dispose()

    nearby_annotations = []
    for _, row in result.iterrows():
        id = row['id']
        coords_dict = json.loads(row['coords'])
        lat = coords_dict.get('lat')
        lon = coords_dict.get('lon')
        if lat is not None and lon is not None:
            distance = geodesic((location['lat'], location['lon']), (lat, lon)).km
            if distance <= radius_km:
                nearby_annotations.append((id, lat, lon))

    return nearby_annotations

def fetch_annotations_with_coordinates():
    """
    Récupère les annotations avec leurs coordonnées depuis la base de données.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        annotations_table = Table('annotations', MetaData(), autoload_with=engine)
        query = (
            session.query(annotations_table.c.id, annotations_table.c.coords)
            .filter(annotations_table.c.coords.isnot(None))
        )
        result = pd.read_sql(query.statement, engine)
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des annotations avec coordonnées: {e}")
        result = pd.DataFrame(columns=['id', 'coords'])
    finally:
        session.close()
        engine.dispose()

    # Extraire latitude et longitude de la colonne coords
    annotations_with_coords = []
    for _, row in result.iterrows():
        id = row['id']
        coords_dict = json.loads(row['coords'])  # Parse JSON string to dictionary
        lat = coords_dict.get('lat')
        lon = coords_dict.get('lon')
        if lat is not None and lon is not None:
            annotations_with_coords.append((id, lat, lon))

    return annotations_with_coords




def fetch_place_types_from_annotations():
    """
    Récupère les différents types de lieux à partir des annotations.

    :return: Liste des types de lieux.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        annotations_table = Table('annotations', MetaData(), autoload_with=engine)
        query = (
            session.query(annotations_table.c.placeType)
            .distinct()
        )
        result = query.all()
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des types de lieux : {e}")
        result = []
    finally:
        session.close()
        engine.dispose()

    return [type_[0] for type_ in result]

def fetch_recent_annotations(since_date):
    """
    Récupère les annotations ajoutées récemment depuis une date donnée.

    :param since_date: Date à partir de laquelle les annotations sont considérées comme récentes.
    :return: Liste des annotations ajoutées récemment avec leurs coordonnées.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        annotations_table = Table('annotations', MetaData(), autoload_with=engine)
        query = (
            session.query(annotations_table.c.id, annotations_table.c.coords, annotations_table.c.date)
            .filter(annotations_table.c.date >= since_date)
        )
        result = query.all()
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des annotations récentes : {e}")
        result = []
    finally:
        session.close()
        engine.dispose()

    recent_annotations_with_coords = []
    for annotation in result:
        id, coords, date = annotation
        try:
            coords_dict = json.loads(coords)
            lat = coords_dict.get('lat')
            lon = coords_dict.get('lon')
            if lat is not None and lon is not None:
                recent_annotations_with_coords.append((id, lat, lon, date))
        except json.JSONDecodeError as e:
            print(f"Erreur lors du décodage des coordonnées : {e}")

    return recent_annotations_with_coords

def fetch_indicators(user_id=None, indicator_type=None, category=None, since_date=None, past_date=None):
    """
    Récupère les indicateurs d'utilisateur en fonction des filtres optionnels fournis.
    
    :param user_id: (facultatif) ID de l'utilisateur pour filtrer les indicateurs.
    :param indicator_type: (facultatif) Type d'indicateur (ex. 'pca_items_count', 'pcoa_items_count').
    :param category: (facultatif) Catégorie de l'indicateur (ex. 'CAT2-Engagement').
    :param since_date: (facultatif) Date depuis laquelle les annotations sont considérées.
    :param past_date: (facultatif) Date limite pour rechercher les indicateurs passés.
    :return: Liste des indicateurs ou une valeur spécifique si `past_date` est fourni.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return [] if not past_date else 0

    try:
        indicators_table = Table('indicators', MetaData(), autoload_with=engine) 
        query = session.query(indicators_table)
        
        # Ajout des filtres en fonction des paramètres fournis
        if user_id:
            query = query.filter(indicators_table.c.user_id == user_id)
        
        if indicator_type:
            query = query.filter(indicators_table.c.type == indicator_type)

        if category:
            query = query.filter(indicators_table.c.category == category)

        if since_date:
            query = query.filter(indicators_table.c.date >= since_date)

        if past_date:
            query = query.filter(indicators_table.c.date <= past_date).order_by(indicators_table.c.date.desc()).limit(1)

        # Exécution de la requête
        result = pd.read_sql(query.statement, engine)

        # Si on cherche un indicateur spécifique dans le passé, retourner une valeur ou 0
        if past_date:
            return result['value'].values[0] if not result.empty else 0
       
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des indicateurs : {e}")
        return [] if not past_date else 0
    finally:
        session.close()
        engine.dispose()

    return result


def fetch_category_recommendations(category, since_date=None):
    """
    Récupère les indicateurs d'utilisateur depuis une date donnée (ou depuis le début si `since_date` n'est pas spécifié).
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les annotations sont considérées (facultatif).
    :return: Liste des indicateurs avec leurs dates de création et de modification.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []
    
    try:
        recos_table = Table('recommendations', MetaData(), autoload_with=engine)
        query = (
            session.query(recos_table.c.id, recos_table.c.user_id, recos_table.c.category,
                          recos_table.c.strategy, recos_table.c.recommendation, recos_table.c.created_at)
            .filter(recos_table.c.category == category)
        )
        if since_date:
            query = query.filter(recos_table.c.created_at >= since_date)

        result = pd.read_sql(query.statement, engine)
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des recos : {e}")
        result = []
    finally:
        session.close()
        engine.dispose()

    return result

def fetch_category_indicators(category, since_date=None):
    """
    Récupère les indicateurs d'utilisateur depuis une date donnée (ou depuis le début si `since_date` n'est pas spécifié).
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les annotations sont considérées (facultatif).
    :return: Liste des indicateurs avec leurs dates de création et de modification.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return []

    try:
        indicators_table = Table('indicators', MetaData(), autoload_with=engine)
        query = (
            session.query(indicators_table.c.id, indicators_table.c.user_id, indicators_table.c.category,
                          indicators_table.c.strategy, indicators_table.c.type, indicators_table.c.value,
                          indicators_table.c.date)
            .filter(indicators_table.c.category == category)
        )
        if since_date:
            query = query.filter(indicators_table.c.date >= since_date)

        result = pd.read_sql(query.statement, engine)
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération des indicateurs : {e}")
        result = []
    finally:
        session.close()
        engine.dispose()

    return result

def get_indicator_value(indicators_df, indicator_type, offset=0):
    """
    Récupère la valeur d'un indicateur spécifique à partir du DataFrame des indicateurs.
    Si la valeur est une chaîne de caractères représentant un nombre, la convertit en nombre.
    
    :param indicators_df: DataFrame contenant les indicateurs.
    :param indicator_type: Type d'indicateur dont la valeur est demandée.
    :param offset: Décalage pour récupérer une valeur plus ancienne (0 pour la plus récente, 1 pour celle d'avant, etc.).
    :return: Valeur de l'indicateur ou None si l'indicateur n'existe pas ou si l'offset est trop grand.
    """
    # Filtrer le DataFrame pour le type d'indicateur demandé et trier par date
    indicator_rows = indicators_df[indicators_df['type'] == indicator_type].sort_values(by='date', ascending=False)

    if indicator_rows.empty:
        return None

    # Vérifier si l'offset est valide
    if offset >= len(indicator_rows):
        return None

    # Extraire la valeur de la ligne correspondant à l'offset
    value = indicator_rows.iloc[offset]['value']

    # Convertir la valeur en nombre si c'est une chaîne représentant un nombre
    if isinstance(value, str):
        try:
            value = float(value)
        except ValueError:
            # Retourner None si la chaîne ne peut pas être convertie en nombre
            return None

    return value




def fetch_user_profile(user_id):
    """
    Récupère le profil utilisateur sous forme de DataFrame.
    
    :param user_id: L'identifiant de l'utilisateur.
    :return: Un DataFrame contenant les informations du profil utilisateur.
    """
    session, engine = connect_to_local_database()
    if not session or not engine:
        return pd.DataFrame()  # Retourner un DataFrame vide en cas de problème avec la connexion

    try:
        # Charger la table 'user_profiles'
        user_profile_table = Table('user_profiles', MetaData(), autoload_with=engine)
        
        # Créer une requête filtrée par user_id
        query = session.query(user_profile_table).filter_by(user_id=user_id)
        
        # Exécuter la requête et convertir le résultat en DataFrame
        data = pd.read_sql(query.statement, engine)
        
        return data  # Retourner les données sous forme de DataFrame
    except SQLAlchemyError as e:
        print(f"Erreur lors de la récupération du profil utilisateur : {e}")
        return pd.DataFrame()  # Retourner un DataFrame vide en cas d'erreur