from elasticsearch import Elasticsearch
from flask import current_app
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from elasticsearch.helpers import scan
from pandas import json_normalize 
from datetime import datetime, timedelta
from geopy.distance import geodesic
from sklearn.cluster import DBSCAN
from geopy.distance import geodesic
from datetime import datetime
from datetime import datetime, timedelta

# Instanciation du client
es_client = None


def get_elasticsearch_client():
    global es_client
    if es_client is None:
        try:
            elastic_config = current_app.config['ELASTIC_URI']
            es_client = Elasticsearch(
                [{'host': elastic_config['host'], 'port': elastic_config['port'], 'scheme': elastic_config['scheme']}],
                http_auth=(elastic_config['username'], elastic_config['password'])
            )
        except ConnectionError as e:
            print(f"Error connecting to Elasticsearch: {e}")
            es_client = None
    return es_client

# Fetch actions for a user
def fetch_actions(user_id, start_date, days):
    """
    Fetch user actions from Elasticsearch within a specified date range.

    :param user_id: ID of the user.
    :param start_date: Start date for the range, can be in "YYYY-MM-DD" format or as a timestamp.
    :param days: Number of days from the start_date to fetch actions.
    :return: List of user actions.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []

    # Detect if start_date is a timestamp or date string
    if isinstance(start_date, (int, float)):  # If timestamp
        start_date_dt = datetime.fromtimestamp(start_date)
    elif isinstance(start_date, str):  # If date string
        start_date_dt = datetime.strptime(start_date, "%Y-%m-%d")
    else:
        raise ValueError("Invalid start_date format. Use 'YYYY-MM-DD' or a timestamp.")

    end_date_dt = start_date_dt + timedelta(days=days)
    
    # Format dates to ISO 8601
    start_date_str = start_date_dt.isoformat() + 'Z'
    end_date_str = end_date_dt.isoformat() + 'Z'

    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"range": {"begin": {"gte": start_date_str, "lte": end_date_str}}}
                ],
                "filter": {
                    "terms": {
                        "type": [
                            'm:openPost', 'm:openMessage',
                            'm:addPost', 'm:editPost',
                            'm:startTour', 'm:addMessage', 'm:addEmoji',
                            'm:movePost', 'm:editTour', 'm:addFavorite',
                            'm:moveTour', 'm:addressSearch', 'm:Filter',
                            'm:deletePost', 'm:deleteMessage'
                        ]
                    }
                }
            }
        }
    }

    response = es.search(index="mobiles", body=query)
    actions = response['hits']['hits']
    
    
    return actions

# Fetch initial shares for a user
def fetch_initial_shares(user_id, start_date, days):
    es = get_elasticsearch_client()
    if es is None:
        return []
    # Detect if start_date is a timestamp or date string
    if isinstance(start_date, (int, float)):  # If timestamp
        start_date_dt = datetime.fromtimestamp(start_date)
    elif isinstance(start_date, str):  # If date string
        start_date_dt = datetime.strptime(start_date, "%Y-%m-%d")
    else:
        raise ValueError("Invalid start_date format. Use 'YYYY-MM-DD' or a timestamp.")

    end_date_dt = start_date_dt + timedelta(days=days)
    
    # Format dates to ISO 8601
    start_date_str = start_date_dt.isoformat() + 'Z'
    end_date_str = end_date_dt.isoformat() + 'Z'

    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"range": {"begin": {"gte": start_date_str, "lte": end_date_str}}}
                ],
                "filter": {
                    "terms": {
                        "type": [
                            'm:addPost', 'm:addMessage', 'm:addTour'
                        ]
                    }
                }
            }
        }
    }
    response = es.search(index="mobiles", body=query)
    shares = response['hits']['hits']
    
    return shares

def fetch_discovered_locations(user_id, start_date=None, days=None):
    es = get_elasticsearch_client()
    if es is None:
        return []

    # Vérification et conversion du format de start_date si elle est fournie
    if start_date:
        if isinstance(start_date, (int, float)):  # Si start_date est un timestamp
            start_date_dt = datetime.fromtimestamp(start_date)
        elif isinstance(start_date, str):  # Si start_date est une chaîne de caractères
            try:
                start_date_dt = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
            except ValueError:
                try:
                    start_date_dt = datetime.strptime(start_date, "%Y-%m-%d")
                except ValueError:
                    raise ValueError("Invalid start_date format. Use 'YYYY-MM-DD', 'YYYY-MM-DD HH:MM:SS', or a timestamp.")
        elif isinstance(start_date, datetime):  # Si start_date est déjà un objet datetime
            start_date_dt = start_date
        else:
            raise ValueError("Invalid start_date type. It should be a string, timestamp, or datetime object.")
    else:
        start_date_dt = None

    # Calcul de la date de fin si days est fourni
    if days:
        end_date_dt = start_date_dt + timedelta(days=days) if start_date_dt else datetime.utcnow()
    else:
        end_date_dt = None

    # Formatage des dates en ISO 8601
    start_date_str = start_date_dt.isoformat() + 'Z' if start_date_dt else None
    end_date_str = end_date_dt.isoformat() + 'Z' if end_date_dt else None

    # Construction de la requête Elasticsearch
    must_conditions = [{"term": {"userId": user_id}}]

    if start_date_str:
        range_query = {"begin": {"gte": start_date_str}}
        if end_date_str:
            range_query["begin"]["lte"] = end_date_str
        must_conditions.append({"range": range_query})

    query = {
        "query": {
            "bool": {
                "must": must_conditions,
                "should": [
                    {"term": {"type": "m:startTour"}},
                    {"term": {"type": "m:addPost"}},
                    {"term": {"type": "m:openPost"}}
                ],
                "minimum_should_match": 1
            }
        }
    }

    # Exécution de la requête Elasticsearch
    response = es.search(index="mobiles", body=query)
    actions = response['hits']['hits']

    # Extraction des coordonnées selon le type d'événement
    coordinates = []
    for action in actions:
        source = action['_source']
        if source['type'] == "m:addPost" and 'coordinates' in source:
            coordinates.append(source['coordinates'])
        elif source['type'] == "m:openPost" and 'post_details' in source and 'coordinates' in source['post_details']:
            coordinates.append(source['post_details']['coordinates'])
        elif source['type'] == "m:startTour" and 'position' in source:
            coordinates.append(source['position'])

    # Vérification si aucune coordonnée n'est trouvée
    if not coordinates:
        return []

    # Convertir les coordonnées en tableau numpy
    coords_array = np.array(coordinates)

    # Conversion en liste de listes
    coords_list = [[coord['lon'], coord['lat']] for coord in coords_array]

    # Appliquer DBSCAN pour regrouper les points proches
    db = DBSCAN(eps=0.01, min_samples=1).fit(coords_list)
    labels = db.labels_

    # Regrouper les coordonnées par cluster
    discovered_locations = []
    for label in set(labels):
        cluster_points = np.array([coords_list[i] for i in range(len(labels)) if labels[i] == label])
        if cluster_points.size > 0:
            discovered_locations.append({
                "cluster_id": label,
                "coordinates": np.mean(cluster_points, axis=0).tolist(),  # Calculer le centre du cluster
                "count": len(cluster_points)
            })

    print(f"Clusters découverts: {discovered_locations}")
    return discovered_locations

# Fetch created annotations for a user
def fetch_content_creation(user_id, start_date=None, days=None):
    es = get_elasticsearch_client()
    if es is None:
        return []

    # Vérification et conversion du format de start_date si elle est fournie
    if start_date:
        if isinstance(start_date, (int, float)):  # Si start_date est un timestamp
            start_date_dt = datetime.fromtimestamp(start_date)
        elif isinstance(start_date, str):  # Si start_date est une chaîne de caractères
            try:
                # Tenter de convertir en format datetime à partir de "YYYY-MM-DD HH:MM:SS"
                start_date_dt = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
            except ValueError:
                try:
                    # Si le premier format échoue, essayer "YYYY-MM-DD"
                    start_date_dt = datetime.strptime(start_date, "%Y-%m-%d")
                except ValueError:
                    raise ValueError("Invalid start_date format. Use 'YYYY-MM-DD', 'YYYY-MM-DD HH:MM:SS', or a timestamp.")
        elif isinstance(start_date, datetime):  # Si start_date est déjà un objet datetime
            start_date_dt = start_date
        else:
            raise ValueError("Invalid start_date type. It should be a string, timestamp, or datetime object.")
    else:
        # Si start_date n'est pas fournie, on récupère toutes les données jusqu'à aujourd'hui
        start_date_dt = None

    # Calcul de la date de fin si days est fourni
    if days:
        end_date_dt = start_date_dt + timedelta(days=days) if start_date_dt else datetime.utcnow()
    else:
        # Si days n'est pas fourni, end_date_dt n'est pas défini
        end_date_dt = None

    # Formatage des dates en ISO 8601
    start_date_str = start_date_dt.isoformat() + 'Z' if start_date_dt else None
    end_date_str = end_date_dt.isoformat() + 'Z' if end_date_dt else None

    # Construction de la requête Elasticsearch
    range_query = {}
    if start_date_str and end_date_str:
        range_query = {"begin": {"gte": start_date_str, "lte": end_date_str}}
    elif start_date_str:
        range_query = {"begin": {"gte": start_date_str}}
    elif end_date_str:
        range_query = {"begin": {"lte": end_date_str}}

    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"range": range_query} if range_query else {}
                ],
                "filter": {
                    "terms": {
                        "type": [
                            'm:addPost', 'm:addMessage', 'm:editPost',
                            'm:addEmoji', 'm:movePost'
                        ]
                    }
                }
            }
        }
    }

    # Exécution de la requête Elasticsearch
    response = es.search(index="mobiles", body=query)
    annotations = response['hits']['hits']

    return annotations

def fetch_route_creation(user_id, start_date=None, days=None):
    es = get_elasticsearch_client()
    if es is None:
        return []

    # Préparer les paramètres de la plage de dates
    end_date_dt = None
    if start_date and days:
        # Utiliser les deux
        start_date_dt = datetime.strptime(start_date, "%Y-%m-%d") if isinstance(start_date, str) else start_date
        end_date_dt = start_date_dt + timedelta(days=days)
    elif days:
        # Utiliser uniquement days jours à partir d'aujourd'hui
        end_date_dt = datetime.utcnow()
        start_date_dt = end_date_dt - timedelta(days=days)
    elif start_date:
        # Utiliser uniquement start_date jusqu'à maintenant
        start_date_dt = datetime.strptime(start_date, "%Y-%m-%d") if isinstance(start_date, str) else start_date

    # Construire la requête Elasticsearch
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}}
                ],
                "filter": {
                    "terms": {
                        "type": ['m:startTour', 'm:editTour', 'm:moveTour']
                    }
                }
            }
        }
    }

    # Ajouter la plage de dates si disponible
    if start_date_dt:
        date_range = {"gte": start_date_dt.isoformat() + 'Z'}
        if end_date_dt:
            date_range["lte"] = end_date_dt.isoformat() + 'Z'
        query["query"]["bool"]["must"].append({"range": {"begin": date_range}})

    # Rechercher dans Elasticsearch
    response = es.search(index="mobiles", body=query)
    routes = response['hits']['hits']
    
    return routes


def fetch_user_ids():
    #TODO: supprime ça
    print('Fetch users : nous testons avec juste un utilisateur')
    user_ids = ['131']
    return user_ids
    """
    Récupère tous les user_id uniques depuis l'index 'mobiles' dans Elasticsearch.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    query = {
        "size": 0,
        "aggs": {
            "unique_users": {
                "terms": {
                    "field": "userId",
                    "size": 10000
                }
            }
        }
    }
    response = es.search(index='mobiles', body=query)
    
    user_ids = [bucket['key'] for bucket in response['aggregations']['unique_users']['buckets']]
    return user_ids

def fetch_logs_for_user(user_id):
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"range": {"timestamp": {"gte": "now-30d/d"}}}
                ]
            }
        }
    }
    try:
        response = es.search(index="mobiles", body=query)
        return response['hits']['hits']
    except ConnectionError as e:
        print(f"Error fetching logs: {e}")
        return []

def fetch_annotations():
    # Connect to the database to fetch annotations
    from db_operations import fetch_annotations
    return fetch_annotations()

def calculate_distance(coord1, coord2):
    if not coord1 or not coord2:
        # Handle the case of empty coordinates
        return None
    lat1, lon1 = coord1['lat'], coord1['lon']
    lat2, lon2 = coord2['lat'], coord2['lon']
    return geodesic((lat1, lon1), (lat2, lon2)).km

def fetch_user_logs(user_id=None, start_date=None, end_date=None):
    """
    Récupère les logs d'activité d'un utilisateur pour une période donnée depuis Elasticsearch.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    query = {
        "query": {
            "bool": {
                "must": []
            }
        }
    }
    if user_id:
        query["query"]["bool"]["must"].append({"term": {"userId": user_id}})
    if start_date:
        query["query"]["bool"]["must"].append({"range": {"timestamp": {"gte": start_date}}})
    if end_date:
        query["query"]["bool"]["must"].append({"range": {"timestamp": {"lte": end_date}}})
    
    response = es.search(index="mobiles", body=query, size=10000)
    # es.close()
    return response['hits']['hits']

def fetch_user_logs_by_type(user_id=None, start_date=None, end_date=None, log_type=None):
    """
    Récupère les logs d'activité d'un utilisateur pour une période donnée depuis Elasticsearch, filtrés par type.
    
    :param user_id: ID de l'utilisateur
    :param start_date: Date de début de la période
    :param end_date: Date de fin de la période
    :param log_type: Type de log à filtrer
    :return: Liste des logs d'activité filtrés
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    query = {
        "query": {
            "bool": {
                "must": []
            }
        }
    }
    
    if user_id:
        query["query"]["bool"]["must"].append({"term": {"userId": user_id}})
    if start_date:
        query["query"]["bool"]["must"].append({"range": {"timestamp": {"gte": start_date}}})
    if end_date:
        query["query"]["bool"]["must"].append({"range": {"timestamp": {"lte": end_date}}})
    if log_type:
        query["query"]["bool"]["must"].append({"term": {"type": log_type}})
    
    response = es.search(index="mobiles", body=query, size=10000)
    return response['hits']['hits']

# def fetch_user_logs(user_id, start_date, end_date):
#     """
#     Récupère les logs d'activité d'un utilisateur pour une période donnée depuis Elasticsearch.
#     """
#     es = get_elasticsearch_client()
#     if es is None:
#         return []
#     query = {
#         "query": {
#             "bool": {
#                 "must": [
#                     {"term": {"userId": user_id}},
#                     {"range": {"timestamp": {"gte": start_date, "lte": end_date}}}
#                 ]
#             }
#         }
#     }
#     response = es.search(index="mobiles", body=query, size=10000)
#     # es.close()
#     return response['hits']['hits']

def fetch_user_logs_with_time(user_id, start_date):
    """
    Récupère les logs d'activité d'un utilisateur avec estimation du temps passé sur chaque annotation.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    # Requête pour obtenir les logs de l'utilisateur avec la date de début
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"range": {"begin": {"gte": start_date}}}
                ]
            }
        }
    }

    response = es.search(index="mobiles", body=query, size=1000)  # Ajuster la taille si nécessaire
    # es.close()
    
    return response['hits']['hits']

def fetch_user_filters(user_id, since_date=None):
    """
    Récupère les logs de filtrage d'un utilisateur.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"term": {"type": "m:Filter"}}
                ],
                # Ajoutez le filtre ici
                "filter": []
            }
        }
    }

    # Filtrer par date si une date de début est spécifiée
    if since_date:
        date_filter = {
            "range": {
                "begin": {
                    "gte": since_date
                }
            }
        }
        query["query"]["bool"]["filter"].append(date_filter)

    # Effectuer la recherche dans Elasticsearch
    try:
        results = es.search(index="mobiles", body=query, size=1000)  # Ajustez la taille si nécessaire
        # Extraire les documents renvoyés par Elasticsearch
        hits = results['hits']['hits']
        filter_data = []

        for hit in hits:
            filter_data.append(hit['_source'])

        # Convertir les données en DataFrame
        df_filters = pd.DataFrame(filter_data)
        return df_filters
    except Exception as e:
        print(f"Erreur lors de la récupération des filtres utilisateur : {e}")
        return pd.DataFrame()



def fetch_user_actions(user_id, start_date=None, action_type=None):
    """
    Récupère les logs d'activité d'un utilisateur à partir de Elasticsearch.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}}
                ]
            }
        },
        "sort": [{"begin": {"order": "asc"}}]
    }

    if start_date:
        query["query"]["bool"]["must"].append({"range": {"begin": {"gte": start_date}}})

    if action_type:
        query["query"]["bool"]["must"].append({"term": {"type": action_type}})

    response = es.search(index="mobiles", body=query, size=1000)  # Ajuster la taille si nécessaire
    # es.close()
    
    return response['hits']['hits']


def fetch_interaction_count_for_content(user_id, content_id):
    """
    Récupère le nombre d'interactions d'un utilisateur spécifique avec un contenu donné.

    :param user_id: ID de l'utilisateur
    :param content_id: ID du contenu (annotation ou autre)
    :return: Nombre d'interactions
    """
    es = get_elasticsearch_client()
    if es is None:
        return 0

    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"term": {"postId": content_id}},
                    {"term": {"type": "m:openPost"}}  # Type d'interaction
                ]
            }
        }
    }
    
    response = es.search(index="mobiles", body=query, size=10000)
    return len(response['hits']['hits'])  # Nombre de résultats retournés

def fetch_interaction_count_for_content(content_id, user_id=None):
    """
    Récupère le nombre d'interactions avec un contenu donné.
    Si user_id est fourni, filtre les interactions pour cet utilisateur spécifique.
    Si user_id n'est pas fourni, récupère les interactions pour le contenu sans filtrer par utilisateur.

    :param user_id: ID de l'utilisateur (facultatif)
    :param content_id: ID du contenu (annotation ou autre)
    :return: Nombre d'interactions
    """
    es = get_elasticsearch_client()
    if es is None:
        return 0

    # Construire la requête de base pour filtrer par content_id et type d'interaction
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"postId": content_id}},
                    {"term": {"type": "m:openPost"}}  # Type d'interaction
                ]
            }
        }
    }

    # Ajouter un filtre pour user_id uniquement s'il est fourni
    if user_id is not None:
        query['query']['bool']['must'].append({"term": {"userId": user_id}})

    # Print the query for debugging
    

    try:
        response = es.search(index="mobiles", body=query, size=10000)
        return len(response['hits']['hits'])
    except Exception as e:
        print(f"Error performing search: {e}")
        return 0

    
def fetch_actions_count(user_id, action_type):
    """
    Récupère le nombre d'actions spécifiques d'un utilisateur.

    :param user_id: ID de l'utilisateur
    :param action_type: Type d'action (ex: m:addPost, m:editPost, m:deletePost)
    :return: Nombre d'actions
    """
    es = get_elasticsearch_client()
    if es is None:
        return 0

    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"term": {"type": action_type}}  # Type d'interaction
                ]
            }
        }
    }
    
    response = es.search(index="mobiles", body=query, size=10000)
    return len(response['hits']['hits'])  # Nombre de résultats retournés


def reconstruct_sessions(user_id, start_date):
    """
    Reconstruit les sessions de 10 minutes à partir des logs d'activité d'un utilisateur.
    """
    actions = fetch_user_actions(user_id, start_date)
    
    sessions = []
    current_session = []
    last_action_time = None

    for action in actions:
        action_time = datetime.fromisoformat(action['_source']['begin'].replace("Z", "+00:00"))
        
        if last_action_time and (action_time - last_action_time) > timedelta(minutes=10):
            if current_session:
                sessions.append(current_session)
                current_session = []
        
        current_session.append(action)
        last_action_time = action_time
    
    if current_session:
        sessions.append(current_session)
    
    return sessions

def fetch_content_popularity():
    """
    Récupère les indices de popularité des lieux, annotations et parcours à partir de Elasticsearch.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    queries = {
        "annotations": {
            "index": "mobiles",
            "body": {
                "aggs": {
                    "annotations": {
                        "terms": {"field": "annotation_id", "size": 1000},
                        "aggs": {
                            "open_post_count": {"filter": {"term": {"type": "m:openPost"}}},
                            "see_tour_count": {"filter": {"term": {"type": "m:seeTour"}}}
                        }
                    }
                }
            }
        },
        "tours": {
            "index": "mobiles",
            "body": {
                "aggs": {
                    "tours": {
                        "terms": {"field": "tour_id", "size": 1000},
                        "aggs": {
                            "open_post_count": {"filter": {"term": {"type": "m:openPost"}}},
                            "see_tour_count": {"filter": {"term": {"type": "m:seeTour"}}}
                        }
                    }
                }
            }
        }
    }

    content_popularity = {}
    for content_type, query in queries.items():
        response = es.search(index=query["index"], body=query["body"], size=0)
        buckets = response["aggregations"][content_type]["buckets"]
        content_popularity[content_type] = {
            bucket["key"]: {
                "open_post_count": bucket["open_post_count"]["doc_count"],
                "see_tour_count": bucket["see_tour_count"]["doc_count"]
            }
            for bucket in buckets
        }
    
    # es.close()
    return content_popularity

def fetch_interactions_by_content(index, content_ids):
    """
    Récupère les interactions globales pour une liste de contenus (annotations, lieux, parcours).
    """
    es = get_elasticsearch_client()
    if es is None:
        return []

    query = {
        "query": {
            "bool": {
                "should": [
                    {"terms": {"content_id": content_ids}},
                    {"terms": {"type": ["m:openPost", "m:seeTour"]}}
                ]
            }
        },
        "aggs": {
            "by_content": {
                "terms": {
                    "field": "content_id",
                    "size": len(content_ids)
                },
                "aggs": {
                    "interaction_count": {
                        "value_count": {
                            "field": "type"
                        }
                    }
                }
            }
        }
    }

    response = es.search(index=index, body=query)
    # es.close()

    content_popularity = {}
    for bucket in response['aggregations']['by_content']['buckets']:
        content_id = bucket['key']
        interaction_count = bucket['interaction_count']['value']
        content_popularity[content_id] = interaction_count
    
    return content_popularity

def fetch_new_interactions(index, since_date):
    """
    Récupère le nombre d'interactions globales avec les nouveautés depuis une date donnée.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"range": {"begin": {"gte": since_date}}}
                ],
                "should": [
                    {"terms": {"type": ["m:openPost", "m:seeTour", "m:Filter"]}}
                ]
            }
        },
        "aggs": {
            "by_content": {
                "terms": {
                    "field": "content_id",
                    "size": 10000  # Ajustez la taille selon vos besoins
                },
                "aggs": {
                    "interaction_count": {
                        "value_count": {
                            "field": "type"
                        }
                    }
                }
            }
        }
    }
    
    response = es.search(index=index, body=query)
    # es.close()
    
    new_interactions = {}
    for bucket in response['aggregations']['by_content']['buckets']:
        content_id = bucket['key']
        interaction_count = bucket['interaction_count']['value']
        new_interactions[content_id] = interaction_count
    
    return new_interactions

from elasticsearch import Elasticsearch, exceptions

def fetch_annotation_popularity():
    """
    Récupère la popularité des annotations en comptant le nombre de fois où elles ont été ouvertes (m:openPost)
    et calcule le pourcentage par rapport à toutes les vues.

    :return: Liste de dictionnaires avec l'ID de l'annotation, le nombre de vues, et le pourcentage de vues.
    """
    es = get_elasticsearch_client()
    if es is None:
        print("Elasticsearch client could not be created.")
        return []

    try:
        query = {
            "size": 0,  # Nous n'avons pas besoin des hits réels, juste des agrégations
            "query": {
                "term": {"type": "m:openPost"}
            },
            "aggs": {
                "annotations": {
                    "terms": {
                        "field": "postId",
                        "size": 10000  # Limite le nombre de résultats, ajustez si nécessaire
                    }
                },
                "total_views": {
                    "sum": {
                        "field": "doc_count"
                    }
                }
            }
        }

        response = es.search(index="mobiles", body=query)
        buckets = response['aggregations']['annotations']['buckets']

        # Calcul du nombre total de vues
        total_views = sum([bucket['doc_count'] for bucket in buckets])

        # Extraire les IDs des annotations, leurs nombres de vues, et calculer le pourcentage
        popularity_data = []
        for bucket in buckets:
            annotation_id = bucket['key']
            view_count = bucket['doc_count']
            view_percentage = (view_count / total_views) * 100 if total_views > 0 else 0
            popularity_data.append({
                "annotation_id": annotation_id,
                "view_count": view_count,
                "view_percentage": view_percentage
            })

        return popularity_data

    except exceptions.RequestError as e:
        print(f"Request error: {e}")
        return []
    except exceptions.ConnectionError as e:
        print(f"Connection error: {e}")
        return []
    except Exception as e:
        print(f"Unexpected error: {e}")
        return []

def fetch_annotation_views(post_id, user_id):
    """
    Récupère le nombre de fois qu'une annotation a été ouverte (m:openPost),
    en excluant les vues de l'auteur.

    :param post_id: ID de l'annotation
    :param user_id: ID de l'auteur de l'annotation
    :return: Nombre de vues de l'annotation, 0 par défaut si aucune vue.
    """
    es = get_elasticsearch_client()
    if es is None:
        print("Elasticsearch client could not be created.")
        return 0

    try:
        query = {
            "query": {
                "bool": {
                    "must": [
                        {"term": {"type": "m:openPost"}},
                        {"term": {"postId": post_id}},  # Filtrer par l'ID de l'annotation
                    ],
                    "must_not": [
                        {"term": {"userId": user_id}}  # Exclure les vues de l'auteur
                    ]
                }
            }
        }

        response = es.search(index="mobiles", body=query)
        
        # Compter le nombre de vues pour l'annotation
        view_count = response['hits']['total']['value'] if 'hits' in response and 'total' in response['hits'] else 0
        
        return view_count

    except exceptions.RequestError as e:
        print(f"Request error: {e}")
        return 0
    except exceptions.ConnectionError as e:
        print(f"Connection error: {e}")
        return 0
    except Exception as e:
        print(f"Unexpected error: {e}")
        return 0


def fetch_interactions_by_content(content_type, content_id):
    """
    Récupère les interactions spécifiques basées sur le type de contenu et l'identifiant de contenu depuis Elasticsearch.
    
    Args:
    - content_type (str): Le type de contenu ('annotation' ou 'tour').
    - content_id (str): L'identifiant du contenu.
    
    Returns:
    - list: Liste des interactions pour le contenu spécifié.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    # Définir le type d'interaction basé sur le type de contenu
    if content_type == 'annotation':
        interaction_types = ['m:openPost', 'm:addEmoji']  # Ajoutez d'autres types d'interactions spécifiques aux annotations
    elif content_type == 'tour':
        interaction_types = ['m:seeTour']  # Ajoutez d'autres types d'interactions spécifiques aux tours
    else:
        raise ValueError("content_type doit être 'annotation' ou 'tour'")
    
    # Requête Elasticsearch pour récupérer les interactions
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"content_id": content_id}},
                    {"terms": {"type": interaction_types}}
                ]
            }
        }
    }
    
    response = es.search(index="mobiles", body=query)
    interactions = response['hits']['hits']
    
    # es.close()
    
    return interactions

def fetch_user_viewed_annotations(user_id, start_date=None, end_date=None):
    """
    Récupère la liste des annotations que l'utilisateur a déjà vues dans une plage de dates optionnelle.

    :param user_id: ID de l'utilisateur.
    :param start_date: Date de début pour le filtrage (optionnelle).
    :param end_date: Date de fin pour le filtrage (optionnelle).
    :return: Liste des IDs des annotations vues par l'utilisateur.
    """
    es = get_elasticsearch_client()
    if es is None:
        print("Elasticsearch client could not be created.")
        return []

    try:
        # Construction de la requête Elasticsearch
        query = {
            "size": 10000,  # Limite le nombre de résultats, ajustez si nécessaire
            "query": {
                "bool": {
                    "must": [
                        {"term": {"userId": user_id}},
                        {"term": {"type": "m:openPost"}}
                    ]
                }
            },
            "aggs": {
                "annotations": {
                    "terms": {
                        "field": "postId",
                        "size": 10000  # Limite le nombre de résultats, ajustez si nécessaire
                    }
                }
            }
        }

        # Si des dates sont fournies, ajouter une condition de filtrage par date
        if start_date or end_date:
            date_filter = {
                "range": {
                    "begin": {}
                }
            }
            if start_date:
                date_filter["range"]["begin"]["gte"] = start_date
            if end_date:
                date_filter["range"]["begin"]["lte"] = end_date

            query["query"]["bool"]["filter"] = [date_filter]

        # Exécution de la requête Elasticsearch
        response = es.search(index="mobiles", body=query)
        buckets = response['aggregations']['annotations']['buckets']
        
        # Extraction des IDs d'annotations consultées
        viewed_annotation_ids = [bucket['key'] for bucket in buckets]
        
        return viewed_annotation_ids

    except exceptions.RequestError as e:
        print(f"Request error: {e}")
        return []
    except exceptions.ConnectionError as e:
        print(f"Connection error: {e}")
        return []
    except Exception as e:
        print(f"Unexpected error: {e}")
        return []

def fetch_user_viewed_tours(user_id, start_date=None, end_date=None):
    """
    Récupère la liste des tours que l'utilisateur a déjà vues dans une plage de dates optionnelle.

    :param user_id: ID de l'utilisateur.
    :param start_date: Date de début pour le filtrage (optionnelle).
    :param end_date: Date de fin pour le filtrage (optionnelle).
    :return: Liste des IDs des tours vues par l'utilisateur.
    """
    es = get_elasticsearch_client()
    if es is None:
        print("Elasticsearch client could not be created.")
        return []

    try:
        # Construction de la requête Elasticsearch
        query = {
            "size": 10000,  # Limite le nombre de résultats, ajustez si nécessaire
            "query": {
                "bool": {
                    "must": [
                        {"term": {"userId": user_id}},
                        {"term": {"type": "m:seeTour"}}
                    ]
                }
            },
            "aggs": {
                "tours": {
                    "terms": {
                        "field": "tourId",
                        "size": 10000  # Limite le nombre de résultats, ajustez si nécessaire
                    }
                }
            }
        }

        # Si des dates sont fournies, ajouter une condition de filtrage par date
        if start_date or end_date:
            date_filter = {
                "range": {
                    "begin": {}
                }
            }
            if start_date:
                date_filter["range"]["begin"]["gte"] = start_date
            if end_date:
                date_filter["range"]["begin"]["lte"] = end_date

            query["query"]["bool"]["filter"] = [date_filter]

        # Exécution de la requête Elasticsearch
        response = es.search(index="mobiles", body=query)
        buckets = response['aggregations']['tours']['buckets']
        
        # Extraction des IDs de tours consultés
        viewed_tour_ids = [bucket['key'] for bucket in buckets]
        
        return viewed_tour_ids

    except exceptions.RequestError as e:
        print(f"Request error: {e}")
        return []
    except exceptions.ConnectionError as e:
        print(f"Connection error: {e}")
        return []
    except Exception as e:
        print(f"Unexpected error: {e}")
        return []

def fetch_annotations_around_location(location, radius='5km'):
    """
    Récupère les annotations autour d'une localisation donnée dans Elasticsearch.

    :param location: Dictionnaire contenant les coordonnées avec les clés 'lat' et 'lon'.
    :param radius: Rayon autour de la localisation pour la recherche des annotations.
    :return: Liste des annotations trouvées.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    query = {
        "query": {
            "bool": {
                "filter": {
                    "geo_distance": {
                        "distance": radius,
                        "coordinates": {
                            "lat": location['lat'],
                            "lon": location['lon']
                        }
                    }
                }
            }
        }
    }
    response = es.search(index='annotations', body=query)
    # es.close()
    return response['hits']['hits']

def fetch_visited_locations(user_id):
    """
    Infère les lieux visités par un utilisateur en se basant sur les logs de session dans Elasticsearch.

    :param user_id: ID de l'utilisateur pour lequel on souhaite récupérer les lieux visités.
    :return: Liste des coordonnées des lieux visités.
    """
    
    es = get_elasticsearch_client()
    
    if es is None:
        return []
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"term": {"type": "m:openPost"}}  # Assumes 'm:openPost' is used for visit-like activities
                ]
            }
        }
    }
    response = es.search(index='mobiles', body=query)
    

    visited_locations = []
    for hit in response['hits']['hits']:
        source = hit['_source']
        if 'post_details' in source:
            source = source['post_details']
            if 'coordinates' in source:
                visited_locations.append(source['coordinates'])
    
    return visited_locations


def fetch_user_visits(user_id, since_date):
    """
    Récupère les visites des lieux par un utilisateur depuis une date donnée.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les visites sont considérées.
    :return: Liste des visites effectuées par l'utilisateur avec leurs coordonnées.
    """
    es = get_elasticsearch_client()
    if es is None:
        return []
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"range": {"begin": {"gte": since_date}}}
                ],
                "filter": {
                    "exists": {
                        "field": "coordinates"
                    }
                }
            }
        }
    }
    
    response = es.search(index="mobiles", body=query, size=10000)
    # es.close()
    
    visits = response['hits']['hits']
    return [{'coordinates': hit['_source']['coordinates']} for hit in visits]

# es_operations.py

from elasticsearch import Elasticsearch

def fetch_user_traces(user_id, since_date=None): 
    """
    Récupère les traces d'un utilisateur depuis une date donnée et retourne un DataFrame.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les traces sont considérées. Si None, toutes les traces seront récupérées.
    :return: DataFrame contenant les traces de l'utilisateur avec leurs coordonnées.
    """
    es = get_elasticsearch_client()
    if es is None:
        return pd.DataFrame()  # Retourne un DataFrame vide si le client Elasticsearch n'est pas disponible
    
    # Initialisation de la requête de base
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}}
                ]
            }
        }
    }

    # Ajout de la condition sur since_date si elle est fournie
    if since_date is not None:
        query['query']['bool']['must'].append({"range": {"timestamp": {"gte": since_date}}})

    response = es.search(index="mobiles", body=query, size=10000)
    
    # Vérification que la réponse contient des hits
    if 'hits' not in response or 'hits' not in response['hits']:
        return pd.DataFrame()  # Retourne un DataFrame vide si aucun hit trouvé

    traces = response['hits']['hits']
    
    # Extraction des hits contenant les coordinates
    filtered_traces = [hit['_source'] for hit in traces if 'coordinates' in hit['_source']]
    
    # Retourne un DataFrame à partir de la liste des hits filtrés
    return pd.DataFrame(filtered_traces)


def convert_to_elasticsearch_format(date_str):
    """
    Convertit une date au format 'YYYY-MM-DD' en un format ISO 8601 pour Elasticsearch.
    
    :param date_str: Date sous forme de chaîne ('YYYY-MM-DD')
    :return: Date au format ISO 8601
    """
    try:
        # Convertir la date en objet datetime
        date_obj = datetime.strptime(date_str, '%Y-%m-%d')
        # Convertir l'objet datetime en chaîne de caractères ISO 8601
        return date_obj.isoformat()
    except ValueError:
        raise ValueError("Format de date invalide. Utilisez 'YYYY-MM-DD'.")




def calculate_end_date(start_date_str, days):
    """
    Calcule la date de fin en ajoutant un nombre de jours à une date de début.
    
    :param start_date_str: Date de début sous forme de chaîne ('YYYY-MM-DD')
    :param days: Nombre de jours à ajouter
    :return: Date de fin au format ISO 8601
    """
    try:
        # Convertir la date de début en objet datetime
        start_date = datetime.strptime(start_date_str, '%Y-%m-%d')
        # Ajouter le nombre de jours pour obtenir la date de fin
        end_date = start_date + timedelta(days=days)
        # Convertir la date de fin en chaîne de caractères ISO 8601
        return end_date.isoformat()
    except ValueError:
        raise ValueError("Format de date invalide. Utilisez 'YYYY-MM-DD'.")


from datetime import datetime, timedelta
import traceback

def fetch_user_locations(user_id, start_date=None, days=None):
    """
    Récupère les emplacements découverts (recherchés ou visités) par l'utilisateur dans un délai donné.

    :param user_id: ID de l'utilisateur
    :param start_date: Date de début (format YYYY-MM-DD ou timestamp). Par défaut, considère toutes les données disponibles.
    :param days: Nombre de jours à partir de la date de début. Par défaut, considère toutes les données disponibles.
    :return: Liste des emplacements uniques découverts par l'utilisateur
    """
    es = get_elasticsearch_client()
    if es is None:
        return []

    # Construire la requête de base
    must_conditions = [{"term": {"userId": user_id}}]

    # Ajouter les conditions de date si start_date est fourni
    if start_date:
        try:
            # Convertir start_date en format Elasticsearch si nécessaire
            if isinstance(start_date, str):
                start_date = datetime.strptime(start_date, '%Y-%m-%d')
            start_date_str = start_date.strftime('%Y-%m-%d')

            if days:
                end_date = (start_date + timedelta(days=days)).strftime('%Y-%m-%d')
            else:
                end_date = datetime.now().strftime('%Y-%m-%d')

            must_conditions.append({"range": {"begin": {"gte": start_date_str, "lte": end_date}}})
        except ValueError as e:
            print("Format de date invalide pour start_date. Utilisez le format YYYY-MM-DD.")
            return []

    # Query Elasticsearch pour les actions pertinentes
    query = {
        "query": {
            "bool": {
                "must": must_conditions,
                "filter": {
                    "terms": {
                        "type": ['m:addressSearch', 'm:addPost', 'm:startTour']
                    }
                }
            }
        }
    }

    try:
        response = es.search(index="mobiles", body=query)
        actions = response['hits']['hits']
    except Exception as e:
        print("Une erreur est survenue :")
        print(str(e))
        print("Détails de l'erreur :")
        traceback.print_exc()
        return []

    # Extraire et dédupliquer les emplacements
    locations = []
    unique_locations = set()

    for action in actions:
        location = extract_location_from_action(action)

        if location and not is_location_near(location, unique_locations):
            unique_locations.add(location)
            locations.append(location)

    return locations


def extract_location_from_action(action):
    """
    Extrait les coordonnées géographiques d'une action Elasticsearch.

    :param action: Données d'action provenant d'Elasticsearch
    :return: Tuple de coordonnées (latitude, longitude)
    """
    try:
        lat = action['_source']['location']['lat']
        lon = action['_source']['location']['lon']
        return (lat, lon)
    except KeyError:
        return None

def is_location_near(location, unique_locations, threshold=0.1):
    """
    Vérifie si un emplacement est à proximité d'un des emplacements existants.

    :param location: Emplacement sous forme de tuple (lat, lon)
    :param unique_locations: Ensemble des emplacements déjà identifiés
    :param threshold: Distance en kilomètres pour considérer les emplacements comme proches
    :return: Booléen indiquant si l'emplacement est proche d'un emplacement existant
    """
    for existing_location in unique_locations:
        if geodesic(location, existing_location).km < threshold:
            return True
    return False

def fetch_opened_posts(user_id):
    """
    Récupère les posts ouverts par un utilisateur depuis Elasticsearch.

    :param user_id: ID de l'utilisateur
    :return: DataFrame contenant les posts ouverts par l'utilisateur
    """
    es = get_elasticsearch_client()
    if es is None:
        return pd.DataFrame()
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"terms": {"type": ['m:openPost']}}
                ]
            }
        }
    }
    
    response = es.search(index="mobiles", body=query)
    data = []
    for hit in response['hits']['hits']:
        source = hit['_source']
        if 'coordinates' in source:
            data.append(source['coordinates'])
    
    # Convert result to DataFrame
    df = pd.DataFrame(data, columns=['coordinates'])
    return df

def fetch_address_searches(user_id):
    """
    Récupère les recherches d'adresse effectuées par un utilisateur depuis Elasticsearch.

    :param user_id: ID de l'utilisateur
    :return: DataFrame contenant les recherches d'adresse de l'utilisateur
    """
    es = get_elasticsearch_client()
    if es is None:
        return pd.DataFrame()
    
    query = {
        "query": {
            "bool": {
                "must": [
                    {"term": {"userId": user_id}},
                    {"terms": {"type": ['m:addressSearch']}}
                ]
            }
        }
    }
    
    response = es.search(index="mobiles", body=query)
    # data = [hit['_source']['coordinates'] for hit in response['hits']['hits']]
    data = []
    for hit in response['hits']['hits']:
        source = hit['_source']
        if 'coordinates' in source:
            data.append(source['coordinates'])
    
    # Convert result to DataFrame
    df = pd.DataFrame(data, columns=['coordinates'])
    return df