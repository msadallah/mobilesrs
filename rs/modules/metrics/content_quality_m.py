import ast
import pandas as pd
import re
import seaborn as sns
import matplotlib.pyplot as plt
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler, MultiLabelBinarizer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestRegressor
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer
from pandas import json_normalize
from scipy import stats
from sqlalchemy import create_engine, Column, Integer, String, DateTime, Float, JSON, MetaData, Table, select, inspect, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.types import Float
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime
import numpy as np
from shapely.geometry import Point, MultiPoint, Polygon
from sklearn.cluster import DBSCAN
import geopandas as gpd
import math
import warnings
import json
import logging
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.exc import SQLAlchemyError
import warnings

from modules.db_operations import fetch_annotations, fetch_user_annotations, insert_indicator


nltk.download('stopwords')
nltk.download('punkt')

stop_words = set(stopwords.words('french'))

def clean_text(text):
    word_tokens = word_tokenize(text)
    
    filtered_text = [word for word in word_tokens if word.casefold() not in stop_words]
    
    return " ".join(filtered_text)

def calculate_MT_for_user(user_df):
    n = len(user_df)  
    sum_VM = user_df['volume_lexical'].sum()  
    sum_DM = user_df['diversity_lexical'].sum()  

    MT = (sum_VM + sum_DM) / (2 * n)
    return MT

def calculate_user_TauxSensible(user_df, weight):
    try:
        taux_annotations_sensibles = user_df['TauxAnnotationsSensibles'].astype(float).fillna(0)
        taux_icones_sensibles = user_df['TauxIconesSensibles'].astype(float).fillna(0)

        user_df['TauxSensible'] = weight * taux_annotations_sensibles + (1 - weight) * taux_icones_sensibles

        return user_df
    except (ValueError, TypeError):
        return user_df.fillna(0)

def calculate_ExpressionGraphique(row):
    ExpressionGraphique = row['ExpressionGraphique'].sum() / row['ExpressionGraphique'].count() if row['ExpressionGraphique'].count() > 0 else 0
    return ExpressionGraphique

# Fonction Haversine pour calculer la distance entre deux points
def haversine_distance(lat1, lon1, lat2, lon2):
    try:
        R = 6371  # Rayon moyen de la Terre en kilomètres
        lat1, lon1, lat2, lon2 = map(np.radians, [lat1, lon1, lat2, lon2])

        dlat = lat2 - lat1
        dlon = lon2 - lon1
        a = np.sin(dlat / 2) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2) ** 2
        c = 2 * np.arcsin(np.sqrt(a))
        distance = R * c

        return distance
    except (ValueError, ZeroDivisionError):
        return np.nan


def calculate_convex_hull_area(row):
    user_points = MultiPoint(list(zip(row['position.lon_radians'], row['position.lat_radians'])))
    
    convex_hull_area = user_points.convex_hull.area
    
    return convex_hull_area


def create_geospatial_profile(user_data):
    
    total_geolocated_annotations = user_data['position.lon'].count() 

    user_data_crs = {'init': 'epsg:4326'}  # Utilisez l'EPSG correspondant à votre système de coordonnées
    

    user_data['geometry'] = [Point(lon, lat) for lon, lat in zip(user_data['position.lon'], user_data['position.lat'])]
    user_data = gpd.GeoDataFrame(user_data, geometry='geometry', crs=user_data_crs)

    distances = [haversine_distance(lat1, lon1, lat2, lon2) for (lat1, lon1), (lat2, lon2) in zip(zip(user_data['position.lat'], user_data['position.lon']), zip(user_data.shift(-1)['position.lat'], user_data.shift(-1)['position.lon']))]

    user_data['distances'] = distances

    avg_distance_between_annotations = user_data['distances'].mean()

    avg_distance_between_annotations = avg_distance_between_annotations if not np.isnan(avg_distance_between_annotations) else 0


    distances_series = pd.Series(distances)
    distances_series = distances_series.dropna()


    # Clustering des annotations en utilisant DBSCAN
    coords = user_data[['position.lon', 'position.lat']].values
    dbscan = DBSCAN(eps=0.5, min_samples=2)
    user_data['cluster'] = dbscan.fit_predict(coords)

    # Nombre d'annotations dans chaque cluster
    cluster_counts = user_data['cluster'].value_counts()

    # Distance totale parcourue par l'utilisateur
    total_distance_traveled = sum(distances_series)

    # Diversité géographique (nombre de clusters différents)
    geographic_diversity = cluster_counts.shape[0]

    # Create the geospatial profile dictionary
    geospatial_profile = {
        'total_geolocated_annotations': total_geolocated_annotations,
        'avg_distance_between_annotations': avg_distance_between_annotations,
        'cluster_counts': cluster_counts.to_dict(),
        'total_distance_traveled': total_distance_traveled,
        'geographic_diversity': geographic_diversity
    }

    return geospatial_profile


def old_calculate_quality_indicators(user):
    annotations = fetch_user_annotations(user)      
    df = pd.DataFrame(annotations )

    print(df)


    # Suppression des colonnes non désirées
    columns_to_exclude = ['@id', 'type', 'id', 'dateDisplay', 'username', 'end', '@version', 'tour_id', '@timestamp']
    columns_to_exclude = [col for col in columns_to_exclude if col in annotations.columns]
    df = annotations.drop(columns=columns_to_exclude, axis=1)

    # Création d'une colonne 'messages' à partir de 'comment'
    if 'comment' in df.columns:
        df['messages'] = df['comment']

    # Réinitialisation de l'index
    df.reset_index(drop=True, inplace=True)

    # Création de colonnes supplémentaires
    if 'messages' in df.columns:
        df['message_length'] = df['messages'].apply(len)
    if 'image_id' in df.columns:
        df['graphical'] = df['image_id'].apply(lambda x: 0 if pd.isnull(x) else 1)
        df['image_id'] = df.apply(lambda row: row['image_id'] if pd.notnull(row['image_id']) or row['graphical'] == 0 else 'unknown_image', axis=1)
    else:
        df['graphical'] = 0

    # Définir la fonction clean_text si ce n'est pas déjà fait
    def clean_text(text):
        # Exemple de fonction de nettoyage. Remplacez par la vôtre.
        return text.lower().strip()

    if 'messages' in df.columns:
        df['clean_message'] = df['messages'].apply(clean_text)
        df['clean_message_length'] = df['clean_message'].apply(len)


    df_icons = pd.DataFrame()
    if 'tags' in df.columns:
        df['tags'] = df['tags'].apply(lambda x: x if isinstance(x, list) else [])
        df_tags = df['tags'].explode().str.get_dummies().rename(lambda x: f'tag-{x}', axis=1)
        df = pd.concat([df, df_tags], axis=1)


    if 'icons' in df.columns:
        # Split 'icons' into a list of icons
        df['icons'] = df['icons'].apply(lambda x: x.split(',') if isinstance(x, str) else [])
        # Create DataFrame where each icon gets its own column
        df_icons = df['icons'].apply(lambda x: pd.Series([1] * len(x), index=x)).fillna(0)
        # Ensure that df_icons is a DataFrame
        df_icons = pd.DataFrame(df_icons.tolist()).fillna(0).astype(int)
        df_icons.columns = ['icon-' + str(col) for col in df_icons.columns]
        df_icons = df_icons.astype(int)

    # Concatenate the original DataFrame with the new columns
    df = pd.concat([df, df_tags, df_icons], axis=1)
    
    
    
    # Transformation des colonnes catégorielles
    df_place = pd.get_dummies(df['placeType'], prefix='place') if 'placeType' in df.columns else pd.DataFrame()
    df_timing = pd.get_dummies(df['timing'], prefix='timing') if 'timing' in df.columns else pd.DataFrame()
    df_emoticon = pd.get_dummies(df['emoticon'], prefix='emoticon') if 'emoticon' in df.columns else pd.DataFrame()

    # Concaténation des DataFrames
    df = pd.concat([df, df_place, df_timing, df_emoticon, df_tags, df_icons], axis=1)

    # Affichage complet des colonnes
    pd.set_option('display.max_columns', 1000)

    # Création d'une copie pour les traces
    df_traces = df.copy()
    df_traces['user'] = df_traces['user'].astype(int)

    ## Masse Textuelle
    data_list = []

    current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    mt_value = calculate_MT_for_user(df_traces)
    data_list.append({'user': user, 'type': 'MasseTextuelle', 'value': mt_value, 'date': current_date})

    df_indicator = pd.DataFrame(data_list, columns=['user', 'type', 'value', 'date'])
    
    
    df_traces['TauxAnnotationsSensibles'] = df_traces['memory_icons_count'] / df_traces.groupby('user')['memory_icons_count'].transform('sum')
    df_traces['TauxIconesSensibles'] = df_traces['memory_icons_count'] / df_traces.groupby('user')['memory_icons_count'].transform('sum')

    weight = 0.5

    
    current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    user_df_with_taux_sensible = calculate_user_TauxSensible(df_traces, weight)
    taux_sensible_mean = user_df_with_taux_sensible['TauxSensible'].mean()
        

    df_indicator = pd.concat([df_indicator, pd.DataFrame({
            'user': [user],
            'type': ['TauxSensible'],
            'value': [taux_sensible_mean],
            'date': [current_date]
        })], ignore_index=True)


    ## Diversification iconographique
    df_traces = df.copy()
    df_traces['user'] = df_traces['user'].astype(int)

    alpha = 0.5
    beta = 0.5 

    
    current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    NombreMoyenIcones = df_traces['total_icons_count'].mean()
    NombreMoyenTypesIcones = df_traces['diversity_types_icons_used'].mean()

    icon_types = ['social', 'mission', 'memory', 'activity', 'senses', 'environment']
    icon_types_usage = df_traces[[type + '_icons_count' for type in icon_types]].sum()

        # Handle division by zero and replace NaNs with zeros
    total_icons_used = df_traces['total_icons_count'].sum()
    if total_icons_used != 0:            
            icon_types_proportion = icon_types_usage / total_icons_used
            # Créer un dictionnaire de mapping pour les noms de colonnes
            column_mapping = {col: re.sub(r'^stats\.icons_stats\.|\_icons_count$', '', col) for col in icon_types_proportion.index}

            # Renommer les colonnes dans icon_types_proportion sans modifier le type
            icon_types_proportion = icon_types_proportion.rename(index=column_mapping)
            # print(icon_types_proportion)
    else:
            icon_types_proportion = pd.Series([0] * len(icon_types), index=icon_types)
            

    icon_types_proportion = icon_types_proportion.fillna(0)  # Replace NaNs with zeros

    for icon_type, proportion in icon_types_proportion.items():
            df_indicator = pd.concat([df_indicator, pd.DataFrame({ 'user': [user], 'type': [f'Taux{icon_type.capitalize()}'], 'value': [float(proportion)], 'date': [current_date]})], ignore_index=True)

    diversification_iconographique_value = alpha * NombreMoyenIcones + beta * NombreMoyenTypesIcones

        # Handle NaN values and replace them with zeros
    diversification_iconographique_value = 0 if np.isnan(diversification_iconographique_value) else diversification_iconographique_value
        
    df_indicator = pd.concat([df_indicator, pd.DataFrame({ 'user': [user], 'type': ['DiversificationIconographique'], 'value': [diversification_iconographique_value], 'date': [current_date]})], ignore_index=True)

        # Include zero values for icon usage types
    if total_icons_used == 0:
            for icon_type in icon_types:
                df_indicator = pd.concat([df_indicator, pd.DataFrame({ 'user': [user], 'type': [f'Taux{icon_type.capitalize()}'], 'value': [0], 'date': [current_date]})], ignore_index=True)


    
    

    ## Expression graphique
    df_traces = df.copy()
    df_traces['user'] = df_traces['user'].astype(int)

    df_traces['ExpressionGraphique'] = df_traces['graphical'].astype(int)

    current_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    expression_graphique_value = calculate_ExpressionGraphique(df_traces)
    df_indicator = pd.concat([df_indicator, pd.DataFrame({ 'user': [user], 'type': ['ExpressionGraphique'], 'value': [expression_graphique_value], 'date': [current_date]})], ignore_index=True)

    

    for _, indicator_row in df_indicator.iterrows():
            # Convertir la ligne en dictionnaire (non nécessaire ici mais utilisé pour la démonstration)
            indicator_dict = indicator_row.to_dict()

            # Utiliser la notation de clé pour accéder aux valeurs des colonnes
            if(not math.isnan(indicator_row['value'])):
                insert_indicator(user, 
                            "Qualité du contenu", 
                            indicator_row['type'],  # Accéder à la colonne 'type' avec la clé
                            indicator_row['type'],  # Accéder à la colonne 'type' avec la clé
                            indicator_row['value'])  # Accéder à la colonne 'value' avec la clé
    
    print('That\'s all folks')



# NEW VERSION
def compute_annotation_score(annotation):
    """
    Calcule le score de qualité d'une annotation en fonction de plusieurs critères pondérés.

    Paramètres:
    - annotation (DataFrame): Une annotation individuelle sous forme de DataFrame.

    Retourne:
    - float: Le score de qualité de l'annotation.
    """
    score = 0
    total_weight = 0

    # Critères de qualité avec leurs poids respectifs
    criteria_weights = {
        'message_length': 0.3,  # Longueur du message
        'graphical': 0.2,       # Utilisation d'images (1 ou 0)
        'num_tags': 0.2,        # Nombre de tags
        'num_icons': 0.2,       # Nombre d'icônes
        'diversity_of_icons': 0.1  # Diversité des icônes (pourcentage ou ratio)
    }

    # Calculer le score pour chaque critère
    for criterion, weight in criteria_weights.items():
        if criterion == 'message_length':
            score += min(len(annotation['messages']) / 100, 1) * weight  # Normalisation de la longueur
        elif criterion == 'graphical':
            score += annotation['graphical'] * weight
        elif criterion == 'num_tags':
            score += min(len(annotation['tags']), 5) / 5 * weight  # Normalisation sur 5 tags max
        elif criterion == 'num_icons':
            score += min(annotation['total_icons_count'], 10) / 10 * weight  # Normalisation sur 10 icônes max
        elif criterion == 'diversity_of_icons':
            score += annotation['diversity_types_icons_used'] * weight  # Supposé normalisé à 1

        total_weight += weight

    # Retourner le score normalisé
    return score / total_weight if total_weight > 0 else 0


def calculate_sqpa(user_id):
    annotations = fetch_user_annotations(user_id)
    total_score = sum(compute_annotation_score(row) for index, row in annotations.iterrows())
    sqpa_value = total_score / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Qualité du contenu", "SQPA", sqpa_value)

def calculate_spa(user_id):
    annotations = fetch_user_annotations(user_id)
    low_quality_annotations = [row for index, row in annotations.iterrows() if compute_annotation_score(row) < 3]  
    spa_value = len(low_quality_annotations) / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Qualité du contenu",  "SPA", spa_value)

def calculate_volume_lexical(user_id):
    annotations = fetch_user_annotations(user_id)
    total_volume = sum(len(row.get('messages', '').split()) for index, row in annotations.iterrows())
    vl_value = total_volume / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Expression Textuelle", "VL", vl_value)

def calculate_diversite_lexicale(user_id):
    annotations = fetch_user_annotations(user_id)
    unique_words = set()
    for index, row in annotations.iterrows():
        unique_words.update(row.get('messages', '').split())
    dl_value = len(unique_words)
    insert_indicator(user_id, "CAT3-Quality", "Expression Textuelle",  "DL", dl_value)

def calculate_idi(user_id):
    annotations = fetch_user_annotations(user_id)
    icons = []
    for index, row in annotations.iterrows():
        icons.extend(row.get('icons', []))
    unique_icons = len(set(icons))
    idi_value = unique_icons / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Enrichissement symbolique",  "IDI", idi_value)


def calculate_ifi(user_id):
    annotations = fetch_user_annotations(user_id)
    total_icons = sum(len(row.get('icons', [])) for index, row in annotations.iterrows())
    ifi_value = total_icons / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Enrichissement symbolique","IFI", ifi_value)

def calculate_irt(user_id):
    annotations = fetch_user_annotations(user_id)
    total_tags = sum(len(row.get('tags', [])) for index, row in annotations.iterrows())
    irt_value = total_tags / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Enrichissement symbolique", "IRT", irt_value)

def calculate_ue(user_id):
    annotations = fetch_user_annotations(user_id)
    emoji_count = sum('emoji' in row.get('messages', '') for index, row in annotations.iterrows())
    ue_value = emoji_count / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Enrichissement symbolique",  "UE", ue_value)

def calculate_ti(user_id):
    annotations = fetch_user_annotations(user_id)
    image_count = sum(row.get('image_id') is not None for index, row in annotations.iterrows())
    ti_value = image_count / len(annotations) if not annotations.empty else 0
    insert_indicator(user_id, "CAT3-Quality", "Expression Graphique", "TI", ti_value)


def calculate_quality_indicators(user_id):
    calculate_sqpa(user_id)
    calculate_spa(user_id)
    calculate_volume_lexical(user_id)
    calculate_diversite_lexicale(user_id)
    calculate_idi(user_id)
    calculate_ifi(user_id)
    calculate_irt(user_id)
    calculate_ue(user_id)
    calculate_ti(user_id)
