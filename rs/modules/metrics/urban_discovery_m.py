import numpy as np
from sklearn.cluster import DBSCAN
from geopy.distance import geodesic
from shapely.geometry import Polygon, MultiPoint
from math import radians, sin, cos, sqrt, atan2
import json
from modules.db_operations import (
    fetch_annotations_around_location, fetch_annotations_by_user, fetch_annotations_with_coordinates, 
    insert_indicator
)
from modules.es_operations import fetch_user_traces, fetch_visited_locations

DEFAULT_SINCE_DATE = "2000-01-01"

# Utilitaires géographiques
def calculate_area_surface(coordinates):
    """
    Calcule la surface d'une zone urbaine définie par une série de coordonnées (latitude, longitude).
    
    :param coordinates: Liste des coordonnées (latitude, longitude) définissant le polygone.
    :return: Surface de la zone urbaine en mètres carrés.
    """
    if len(coordinates) < 3:
        # Un polygone doit avoir au moins 3 points
        print("Pas assez de points pour définir un polygone.")
        return 0
    
    # Création d'un polygone à partir des coordonnées
    polygon = Polygon(coordinates)
    
    # La surface retournée par shapely est en unités de degré carré
    # Convertir en mètres carrés en utilisant une approximation
    # Note : La conversion exacte dépend de la localisation et de la projection
    # Ici, on utilise la conversion simplifiée en degrés pour un calcul approximatif.
    area_m2 = polygon.area * (40008000 / 360) ** 2  # Approximation en utilisant la largeur de la terre
    
    return area_m2

def calculate_distance(coord1, coord2):
    """
    Calcule la distance entre deux coordonnées géographiques en kilomètres.
    
    :param coord1: Premier point (latitude, longitude).
    :param coord2: Deuxième point (latitude, longitude).
    :return: Distance en kilomètres.
    """
    return geodesic((coord1['lat'], coord1['lon']), (coord2['lat'], coord2['lon'])).kilometers


def calculate_harv_distance(coord1, coord2):
    """
    Calcule la distance de Haversine entre deux points (latitude, longitude) en kilomètres.
    
    :param coord1: Tuple (lat, lon) du premier point.
    :param coord2: Tuple (lat, lon) du second point.
    :return: Distance en kilomètres.
    """
    lat1, lon1 = radians(coord1[0]), radians(coord1[1])
    lat2, lon2 = radians(coord2[0]), radians(coord2[1])
    
    dlat = lat2 - lat1
    dlon = lon2 - lon1
    
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    R = 6371  # Rayon de la Terre en kilomètres
    return R * c


# Fonction utilitaire pour obtenir le nombre d'annotations autour de chaque lieu
def get_annotations_count_around_locations(locations):
    """
    Compte le nombre total d'annotations autour des lieux donnés.
    
    :param locations: Liste des coordonnées des lieux.
    :return: Nombre total d'annotations.
    """
    total_annotations = 0
    for location in locations:
        annotations = fetch_annotations_around_location(location)
        total_annotations += len(annotations)
    return total_annotations


# Catégorie 4 : Découverte Urbaine
# Stratégie 1 : Découverte de POI Basée sur les Tendances de Visite
def calculate_dap(user_id):
    """
    Calcule la densité d'annotations autour des POI liés aux lieux visités par un utilisateur.
    
    :param user_id: ID de l'utilisateur.
    """
    # Récupérer les lieux visités par l'utilisateur
    visited_locations = fetch_visited_locations(user_id)
    
    if not visited_locations:
        print(f"Aucun lieu visité trouvé pour l'utilisateur {user_id}. DAP sera défini à 0.")
        dap = 0
        insert_indicator(
            user_id,
            "Découverte Urbaine",
            "Recommandation à partir des tendances de visite",
            "DAP",
            dap
        )
        return

    print(f"Lieux visités par l'utilisateur {user_id} : {visited_locations}")
    
    # Convertir les lieux visités en POI (Points of Interest)
    visited_pois = get_pois_nearby(visited_locations)
    
    if not visited_pois:
        print(f"Aucun POI trouvé autour des lieux visités par l'utilisateur {user_id}. DAP sera défini à 0.")
        dap = 0
        insert_indicator(
            user_id,
            "Découverte Urbaine",
            "Recommandation à partir des tendances de visite",
            "DAP",
            dap
        )
        return
    
    print(f"POIs proches des lieux visités par l'utilisateur {user_id} : {visited_pois}")
    
    # Calculer la densité d'annotations
    total_annotations = sum(poi['annotations_count'] for poi in visited_pois)
    total_pois = len(visited_pois)
    
    # Calcul de la DAP : si aucun POI, on retourne une densité de 0
    dap = total_annotations / total_pois if total_pois > 0 else 0

    print(f"DAP pour l'utilisateur {user_id} : {dap}")
    
    # Insérer l'indicateur DAP dans la base de données
    insert_indicator(
        user_id,
        "Découverte Urbaine",
        "Recommandation à partir des tendances de visite",
        "DAP",
        dap
    )

def get_average_dap_for_similar_zones(visited_locations, zone_radius=1.0):
    """
    Calcule la densité moyenne d'annotations dans des zones similaires aux lieux visités.
    
    :param visited_locations: Liste des coordonnées des lieux visités.
    :param zone_radius: Rayon de recherche autour de chaque lieu pour définir des zones similaires (en km).
    :return: Densité moyenne d'annotations dans ces zones similaires.
    """
    all_annotations = fetch_annotations_with_coordinates()
    
    if not all_annotations:
        return 0
    
    total_dap = 0
    count_zones = 0
    
    for location in visited_locations:
        lat, lon = location['lat'], location['lon']
        
        # Filtrer les annotations proches de chaque lieu visité
        nearby_annotations = fetch_annotations_around_location({'lat': lat, 'lon': lon}, zone_radius)
        
        if nearby_annotations:
            # Calcul de la DAP pour cette zone
            surface_area = np.pi * (zone_radius ** 2)  # Surface d'un cercle avec le rayon donné
            dap = len(nearby_annotations) / surface_area
            total_dap += dap
            count_zones += 1
    
    # Retourner la DAP moyenne pour toutes les zones similaires
    average_dap = total_dap / count_zones if count_zones > 0 else 0
    return average_dap


def calculate_prl(user_id):
    """
    Calcule la popularité relative des POI liés aux lieux visités par un utilisateur.
    """
    visited_locations = fetch_visited_locations(user_id)
    if not visited_locations:
        print(f"Aucun lieu visité trouvé pour l'utilisateur {user_id}.")
        return
    
    # Convertir les lieux visités en POI (Points of Interest)
    visited_pois = get_pois_nearby(visited_locations)
    if not visited_pois:
        print("Aucun POI trouvé autour des lieux visités.")
        return
    
    # Calcul de la DAP pour les lieux visités
    total_annotations = sum(poi['annotations_count'] for poi in visited_pois)
    total_pois = len(visited_pois)
    dap = total_annotations / total_pois if total_pois > 0 else 0

    # Calcul du DAP moyen pour les zones similaires
    dap_mean = get_average_dap_for_similar_zones(visited_locations)

    # Calcul de la PRL
    prl = dap / dap_mean if dap_mean > 0 else 0

    # Insérer l'indicateur PRL dans la base de données
    insert_indicator(
        user_id,
        "Découverte Urbaine",
        "Recommandation à partir des tendances de visite",
        "PRL",
        prl
    )


# Stratégie 2 : Exploration Équilibrée des Zones Urbaines
def calculate_dmapp(user_id):
    """
    Calcule la DMAPP (Distance Moyenne des Points de Proximité) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur
    """
    # Récupérer les traces de l'utilisateur
    user_df = fetch_user_traces(user_id)
    
    if user_df.empty:
        print(f"DMAPP - Aucune trace trouvée pour l'utilisateur {user_id}. DMAPP sera défini à NULL.")
        dmapp_value = None  # Utiliser None pour insérer NULL dans la base de données
        insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "DMAPP", "DMAPP", dmapp_value)
        return
    
    # Vérifier si le DataFrame contient les colonnes pour les coordonnées
    if 'coordinates' not in user_df.columns:
        print(f"DMAPP - Les coordonnées ne sont pas disponibles pour l'utilisateur {user_id}. DMAPP sera défini à NULL.")
        dmapp_value = None  # Utiliser None pour insérer NULL dans la base de données
        insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "DMAPP", "DMAPP", dmapp_value)
        return
    
    print(f"Traces de l'utilisateur {user_id} : {user_df[['coordinates']].head()}")
    
    # Liste pour stocker les distances DMAPP
    dmapp_distances = []
    
    # Itérer sur chaque ligne pour calculer les distances par rapport à tous les autres points
    for index, row in user_df.iterrows():
        print(f"Calcul DMAPP pour l'utilisateur {user_id}, ligne {index}: {row['coordinates']}")
        
        # Supposons que les coordonnées sont un dictionnaire avec des clés 'lat' et 'lon'
        lat_radians = np.radians(row['coordinates']['lat'])
        lon_radians = np.radians(row['coordinates']['lon'])
        
        # Calculer les distances entre ce point et tous les autres points
        distances = user_df.apply(
            lambda x: calculate_harv_distance(
                (lat_radians, lon_radians), 
                (np.radians(x['coordinates']['lat']), np.radians(x['coordinates']['lon']))
            ), 
            axis=1
        )
        
        # Exclure les distances nulles (comparaison avec soi-même) et calculer la plus petite distance
        min_distance = distances[distances > 0].min() if not distances.empty else None
        dmapp_distances.append(min_distance)
    
    # Conversion des distances en un tableau NumPy
    dmapp_distances_np = np.array(dmapp_distances)
    
    # Calcul de la moyenne des distances, en ignorant les valeurs NaN
    dmapp_value = np.nanmean(dmapp_distances_np) if dmapp_distances_np.size > 0 else None
    
    print(f"DMAPP pour l'utilisateur {user_id} : {dmapp_value}")
    
    # Insertion de l'indicateur DMAPP dans la base de données
    insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "DMAPP", "DMAPP", dmapp_value)


#  surface du polygone convexe englobant
def calculate_spc(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule la surface du polygone convexe englobant (SPC) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les traces doivent être récupérées.
    """
    # Récupérer les traces de l'utilisateur
    user_df = fetch_user_traces(user_id, since_date)
    
    if user_df.empty:
        print(f"SPC - Aucune trace trouvée pour l'utilisateur {user_id}. SPC sera défini à NULL.")
        spc_value = None  # Utiliser None pour insérer NULL dans la base de données
        insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "SPC", "SPC", spc_value)
        return
    
    # Vérification de la présence de la colonne 'coordinates'
    if 'coordinates' not in user_df.columns:
        print(f"SPC - Les coordonnées ne sont pas disponibles pour l'utilisateur {user_id}. SPC sera défini à NULL.")
        spc_value = None  # Utiliser None pour insérer NULL dans la base de données
        insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "SPC", "SPC", spc_value)
        return
    
    # Extraction des coordonnées (lon, lat) à partir du champ 'coordinates'
    points = list(zip(user_df['coordinates'].apply(lambda x: x['lon']), 
                      user_df['coordinates'].apply(lambda x: x['lat'])))
    
    # Vérification du nombre de points (au moins 3 points nécessaires pour un polygone convexe)
    if len(points) < 3:
        print(f"SPC - Pas assez de points pour calculer le polygone convexe pour l'utilisateur {user_id}. SPC sera défini à NULL.")
        spc_value = None  # Utiliser None pour insérer NULL dans la base de données
        insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "SPC", "SPC", spc_value)
        return
    
    # Calcul du polygone convexe et de la surface
    convex_hull = MultiPoint(points).convex_hull
    spc_value = convex_hull.area if convex_hull else None  # Utiliser None si le polygone n'est pas valide
    
    print(f"SPC pour l'utilisateur {user_id} : {spc_value}")
    
    # Insertion de l'indicateur SPC dans la base de données
    insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "SPC", "SPC", spc_value)


# Stratégie 3 : Diversification des Types d’Annotations Urbaines
def calculate_dau(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule la diversité des annotations urbaines pour un utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les annotations sont considérées.
    :return: Diversité des annotations urbaines.
    """
    # Récupération des annotations de l'utilisateur depuis la date spécifiée
    annotations_df = fetch_annotations_by_user(user_id, since_date)
    
    if annotations_df.empty:
        print(f"DAU - Aucune annotation trouvée pour l'utilisateur {user_id}. DAU sera défini à 0.")
        return 0
    
    # Extraction des coordonnées des annotations
    coordinates = []
    for _, row in annotations_df.iterrows():
        coords = row['coords']  # Adaptez selon le nom de colonne réel
        coords_dict = json.loads(coords)
        lat = coords_dict.get('lat')
        lon = coords_dict.get('lon')
        if lat is not None and lon is not None:
            coordinates.append((lat, lon))
    
    # Récupération des zones urbaines dynamiques
    urban_areas = get_dynamic_urban_areas(coordinates)
    
    # Comptage des types d'annotations par zone
    annotation_types_per_area = {area: set() for area in urban_areas}
    
    for _, row in annotations_df.iterrows():
        coords = row['coords']
        coords_dict = json.loads(coords)
        lat = coords_dict.get('lat')
        lon = coords_dict.get('lon')
        if lat is not None and lon is not None:
            for area_id, area_coords in urban_areas.items():
                if any(calculate_harv_distance((lat, lon), coord) < 0.01 for coord in area_coords):
                    annotation_types_per_area[area_id].add(row['placeType'])
    
    # Calcul de la diversité
    total_types = sum(len(types) for types in annotation_types_per_area.values())
    total_areas = len(annotation_types_per_area)
    
    dau = total_types / total_areas if total_areas > 0 else 0

    # Insérer l'indicateur dans la base de données
    insert_indicator(
        user_id,
        "Découverte Urbaine",
        "Diversité des Annotations Urbaines",
        "DAU",
        dau
    )



# Stratégie 4 : Réduction de la Concentration des Annotations
def calculate_dc(user_id):
    annotations_df = fetch_annotations_by_user(user_id)
    if annotations_df.empty:
        print(f"Aucune annotation trouvée pour l'utilisateur {user_id}.")
        return
    coordinates = [(json.loads(row['coords'])['lat'], json.loads(row['coords'])['lon']) for _, row in annotations_df.iterrows()]
    cluster_data = get_clusters(coordinates)
    concentration_scores = []
    for cluster_id, coords in cluster_data.items():
        num_annotations = len(coords)
        area_surface = calculate_area_surface(coords)
        if area_surface > 0:
            concentration_scores.append(num_annotations / area_surface)
    dc_value = np.mean(concentration_scores) if concentration_scores else 0
    insert_indicator(user_id, "Découverte Urbaine", "Degré de Clusterisation", "DC", dc_value)

def calculate_dsa(user_id):
    annotations_df = fetch_annotations_by_user(user_id)
    if annotations_df.empty:
        print(f"Aucune annotation trouvée pour l'utilisateur {user_id}.")
        return
    coordinates = [(json.loads(row['coords'])['lat'], json.loads(row['coords'])['lon']) for _, row in annotations_df.iterrows()]
    clusters = get_clusters(coordinates)
    total_annotations = len(coordinates)
    cluster_annotations = sum(len(cluster) for cluster in clusters.values())
    dsa_value = cluster_annotations / total_annotations if total_annotations > 0 else 0
    insert_indicator(user_id, "Découverte Urbaine", "DSA", "DSA", dsa_value)

# Fonction auxiliaire pour obtenir les POI à proximité des lieux visités
def get_pois_nearby(visited_locations, eps=0.2, min_samples=3):
    """
    Génère des Points d’Intérêt (POI) potentiels à partir des annotations et sélectionne 
    ceux qui sont à proximité des lieux visités par l'utilisateur.

    :param visited_locations: Liste des coordonnées des lieux visités par l'utilisateur (en degrés).
    :param eps: Distance maximale entre deux points pour les considérer dans le même cluster (en kilomètres).
    :param min_samples: Nombre minimum d'annotations pour former un POI.
    :return: Liste de POI à proximité des lieux visités par l'utilisateur.
    """
    # Récupérer les annotations avec coordonnées
    annotations_with_coords = fetch_annotations_with_coordinates()
    if not annotations_with_coords:
        print("Aucune annotation avec coordonnées trouvée.")
        return []

    # Extraire les coordonnées et les IDs des annotations
    coords = np.array([(lat, lon) for _, lat, lon in annotations_with_coords])
    annotation_ids = [annotation_id for annotation_id, _, _ in annotations_with_coords]

    # Convertir les coordonnées en radians
    coords_radians = np.radians(coords)

    # Appliquer DBSCAN pour regrouper les annotations en POI potentiels
    clustering = DBSCAN(eps=eps / 6371.0, min_samples=min_samples, metric='haversine').fit(coords_radians)
    
    # Associer chaque annotation à son cluster
    clusters = {}
    for idx, label in enumerate(clustering.labels_):
        if label == -1:
            continue  # Skip noise points
        if label not in clusters:
            clusters[label] = []
        clusters[label].append((annotation_ids[idx], annotations_with_coords[idx]))  # Inclure l'ID d'annotation

    # Construire des POI à partir des clusters
    pois = []
    for cluster_id, points in clusters.items():
        if len(points) >= min_samples:
            # Calculer le centre géographique du cluster comme POI
            lat_mean = np.mean([lat for _, (annotation_id, lat, lon) in points])
            lon_mean = np.mean([lon for _, (annotation_id, lat, lon) in points])
            annotations_count = len(points)
            annotation_ids = [annotation_id for annotation_id, _ in points]  # Collecter les IDs des annotations
            pois.append({
                'id': f'poi_{cluster_id}',
                'name': f'POI {cluster_id}',
                'description': f'POI avec {annotations_count} annotations.',
                'lat': lat_mean,
                'lon': lon_mean,
                'annotations_count': annotations_count,
                'annotation_ids': annotation_ids  # Ajouter les IDs d'annotations ici
            })

    # Filtrer les POI qui sont proches des lieux visités par l'utilisateur
    nearby_pois = []
    for poi in pois:
        for visited_location in visited_locations:
            visited_lat = visited_location['lat']
            visited_lon = visited_location['lon']
            distance = haversine_distance((poi['lat'], poi['lon']), (visited_lat, visited_lon))
            if distance <= eps:  # Distance en kilomètres
                nearby_pois.append(poi)
                break  # On ne rajoute qu'une fois par POI
    
    # Après récupération des annotations
    print(f"Nombre d'annotations récupérées : {len(annotations_with_coords)}")

    # Après clustering DBSCAN
    print(f"Labels après clustering : {set(clustering.labels_)}")

    # Nombre de POI potentiels créés
    print(f"Nombre de POI créés : {len(pois)}")

    # Filtrage des POI à proximité
    print(f"POI avant filtrage par proximité : {len(pois)}")

    return nearby_pois


def haversine_distance(coord1, coord2):
    """
    Calcule la distance de Haversine entre deux points en kilomètres.

    :param coord1: Tuple (lat, lon) du premier point.
    :param coord2: Tuple (lat, lon) du second point.
    :return: Distance en kilomètres.
    """
    lat1, lon1 = radians(coord1[0]), radians(coord1[1])
    lat2, lon2 = radians(coord2[0]), radians(coord2[1])
    dlat = lat2 - lat1
    dlon = lon2 - lon1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    # Rayon de la Terre en kilomètres
    R = 6371.0
    return R * c

# Fonction auxiliaire pour obtenir les zones urbaines dynamiques
def get_dynamic_urban_areas(coordinates):
    urban_areas = {}
    clustering = DBSCAN(eps=0.2, min_samples=5).fit(coordinates)
    for label in set(clustering.labels_):
        if label == -1:
            continue
        cluster_coords = [coord for i, coord in enumerate(coordinates) if clustering.labels_[i] == label]
        if len(cluster_coords) >= 5:
            urban_areas[label] = cluster_coords
    return urban_areas

# Fonction auxiliaire pour obtenir les clusters d'annotations
def get_clusters(coordinates, eps=0.2, min_samples=5):
    clustering = DBSCAN(eps=eps / 6371.0, min_samples=min_samples, metric='haversine').fit(np.radians(coordinates))
    clusters = {}
    for idx, label in enumerate(clustering.labels_):
        if label == -1:
            continue
        if label not in clusters:
            clusters[label] = []
        clusters[label].append(coordinates[idx])
    return clusters


def get_unexplored_zones(user_id, annotations, min_distance=0.5):
    """
    Identifie les zones inexplorées autour des annotations existantes.
    
    :param user_id: ID de l'utilisateur.
    :param annotations: Liste des coordonnées (lat, lon) des annotations de l'utilisateur.
    :param min_distance: Distance minimale en kilomètres pour considérer une zone comme inexplorée.
    :return: Liste de coordonnées représentant des zones inexplorées.
    """
    if not annotations:
        print(f"Aucune annotation trouvée pour l'utilisateur {user_id}.")
        return []
    
    # Extraire les coordonnées des annotations
    print(type(annotations))
    print(annotations)
    # coordinates = [(json.loads(ann['coords'])['lat'], json.loads(ann['coords'])['lon']) for ann in annotations]
    coordinates = [(lat, lon) for lat, lon in annotations]
    
    # Clustering des annotations pour définir les zones explorées
    clustering = DBSCAN(eps=min_distance, min_samples=5, metric=calculate_harv_distance).fit(coordinates)
    
    # Obtenir les clusters de zones explorées
    explored_zones = [coordinates[i] for i in range(len(coordinates)) if clustering.labels_[i] != -1]
    
    # Déterminer les zones inexplorées (en dehors des clusters)
    unexplored_zones = []
    
    for coord in coordinates:
        is_far = all(calculate_harv_distance(coord, explored) > min_distance for explored in explored_zones)
        if is_far:
            unexplored_zones.append(coord)
    
    return unexplored_zones



def calculate_pze(user_id):
    """
    Calcule la proximité aux zones non exploitées (PZE) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur.
    :return: Proximité aux zones non exploitées (PZE).
    """
    # Récupérer les traces (annotations) de l'utilisateur
    annotations_df = fetch_annotations_by_user(user_id)
    
    if annotations_df.empty:
        print(f"Aucune annotation trouvée pour l'utilisateur {user_id}.")
        return None
    
    # Obtenir les coordonnées des annotations de l'utilisateur
    user_annotations = [(json.loads(row['coords'])['lat'], json.loads(row['coords'])['lon']) 
                        for _, row in annotations_df.iterrows()]
    
    # Déterminer les zones non explorées
    unexplored_zones = get_unexplored_zones(user_id, user_annotations)
    
    if not unexplored_zones:
        print("Aucune zone non explorée trouvée.")
        return None

    # Calculer la distance minimale entre chaque annotation de l'utilisateur et les zones non explorées
    distances_to_unexplored = []
    for annotation in user_annotations:
        min_distance = min(calculate_harv_distance(annotation, zone) for zone in unexplored_zones)
        distances_to_unexplored.append(min_distance)
    
    # Calculer la distance moyenne aux zones non explorées
    pze_value = np.mean(distances_to_unexplored) if distances_to_unexplored else np.nan
    
    # Insérer l'indicateur dans la base de données
    insert_indicator(user_id, "Exploration Équilibrée des Zones Urbaines", "Proximité aux Zones Non Exploitées", "PZE", pze_value)
    
    return pze_value
