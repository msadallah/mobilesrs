from modules.db_operations import fetch_user_annotations, fetch_user_comments, insert_indicator
from modules.es_operations import fetch_actions_count, fetch_user_actions

# Valeur par défaut pour "depuis toujours"
DEFAULT_SINCE_DATE = "0000-01-01"

# --------------------------------------------
# Catégorie 5 : Interaction et réflexion
# --------------------------------------------
def calculate_tac(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Taux d'Amélioration de la Contribution (TAC) pour un utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les actions sont considérées.
    :return: TAC.
    """
    # Récupérer les annotations et les actions de l'utilisateur depuis la date spécifiée
    annotations = fetch_user_annotations(user_id, since_date)
    actions = fetch_user_actions(user_id, since_date)
    
    # Compter les modifications et suppressions
    M = fetch_actions_count(user_id, "m:editPost")
    S = fetch_actions_count(user_id, "m:deletePost")
    
    # Définir les types d'interactions à considérer
    interaction_types = {'m:openPost', 'm:seePostsMode', 'm:showNotifications', 'm:readNotification', 
                          'm:selectNotification', 'm:addPost', 'm:editPost', 'm:startTour', 
                          'm:addMessage', 'm:addEmoji', 'm:movePost', 'm:editTour', 'm:addFavorite', 
                          'm:moveTour', 'm:addressSearch', 'm:Filter'}
    
    # Compter les interactions totales
    # L = sum(1 for act in actions if act.get('action_type') in interaction_types)
    total_actions = {action: fetch_actions_count(user_id, action) for action in interaction_types}

    # Calcul du total de toutes les actions
     
    L = sum(total_actions.values())
    
    # Calculer le TAC
    tac = (M + S) / L if L else 0
    
    # Insérer l'indicateur dans la base de données
    insert_indicator(
        user_id,
        "Interaction et réflexion",
        "Auto-Évaluation et Amélioration des Contributions",
        "TAC",
        tac
    )
    
    return tac

def calculate_tec(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Taux d'Engagement Communautaire (TEC) pour un utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les actions sont considérées.
    :return: TEC.
    """
    # Récupérer les commentaires de l'utilisateur depuis la date spécifiée
    comments = fetch_user_comments(user_id, since_date)
    
    # Compter les commentaires et les réponses
    C = fetch_actions_count(user_id, 'm:addMessage')  # Nombre de commentaires
    R = fetch_actions_count(user_id, 'm:addEmoji')  # Nombre de réponses aux commentaires
    
    # Nombre total de contributions reçues
    L = len(comments)
    
    # Calculer le TEC
    tec = (C + R) / L if L else 0
    
    # Insérer l'indicateur dans la base de données
    insert_indicator(
        user_id,
        "Interaction et réflexion",
        "Interaction avec les Contributions Communautaires",
        "TEC",
        value=tec
    )
    


def calculate_trc(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Taux de Réponse aux Commentaires (TRC) pour un utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les actions sont considérées.
    :return: TRC.
    """
    # Récupérer les commentaires reçus de l'utilisateur depuis la date spécifiée
    comments = fetch_user_comments(user_id, since_date)
    
    # Compter les réponses aux commentaires
    R = fetch_actions_count(user_id, 'm:addEmoji')  # Réponses aux commentaires
    
    # Nombre total de commentaires reçus
    C = len(comments)
    
    # Calculer le TRC
    trc = R / C if C else 0
    
    # Insérer l'indicateur dans la base de données
    insert_indicator(
        user_id,
        "Interaction et réflexion",
        "Réactivité aux Commentaires sur les Contributions",
        "TRC",
        trc
    )
    
    return trc

def calculate_tcc(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Taux de Contribution aux Commentaires (TCC) pour un utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les actions sont considérées.
    :return: TCC.
    """
    annotations = fetch_user_annotations(user_id, since_date)
    comments = fetch_user_comments(user_id, since_date)
    
    # Compter les commentaires faits
    C = fetch_actions_count(user_id, 'm:addMessage')  # Commentaires faits
    
    # Contributions commentables
    T = len(annotations) + len(comments)
    
    # Calculer le TCC
    tcc = C / T if T else 0
    
    # Insérer l'indicateur dans la base de données
    insert_indicator(user_id, "Interaction et réflexion", "Taux de Contribution aux Commentaires", "TCC", tcc)
    
    return tcc