from collections import defaultdict
from datetime import datetime, timedelta
import json
import math
from sqlalchemy.exc import SQLAlchemyError

import pandas as pd
from modules.db_operations import fetch_annotations, fetch_annotations_by_id, fetch_annotations_by_user, fetch_comments, fetch_notifications, fetch_indicators, fetch_recommendations, fetch_tours, fetch_user_tours, fetch_user_annotations, fetch_user_profile, fetch_users, get_indicator_value, insert_indicator
from modules.es_operations import calculate_distance, fetch_actions_count, fetch_address_searches, fetch_annotation_popularity, fetch_interaction_count_for_content, fetch_interactions_by_content, fetch_logs_for_user, fetch_new_interactions, fetch_opened_posts, fetch_user_actions, fetch_user_filters, fetch_user_locations, fetch_user_logs, fetch_user_logs_by_type, fetch_user_logs_with_time, fetch_user_viewed_annotations, fetch_user_viewed_tours, reconstruct_sessions
from geopy.distance import geodesic

DEFAULT_SINCE_DATE = "0000-01-01"
# --------------------------------------------
# Catégorie 2 : Promotion d'un Engagement Durable et Ré-engagement
# --------------------------------------------
# Stratégie 1 : Maintien de l'Engagement Basé sur le Profil d’Annotation
# --------------------------------------------

def calculate_annotation_creation_weight(user_id):
    """
    Calcule le poids de création d'annotations (𝒘_𝑷𝑪A) et l'insère dans la base de données.
    """
    profile = fetch_user_profile(user_id)
    if profile.empty:
        return

    created_annotations = profile['pca_items_count'].values[0]
    print("profile: ", profile)
    print("profile['pca_items_count'].values[0]: ", profile['pca_items_count'].values[0], "et profile['pcoa_items_count'].values[0]: ", profile['pcoa_items_count'].values[0])
    total_interactions = created_annotations + profile['pcoa_items_count'].values[0]

    if total_interactions == 0:
        value = 0
    else:
        value = created_annotations / total_interactions

    insert_indicator(user_id, "CAT2-Engagement", "Weight of Annotation Creation Profile", "WPCA", value)


def calculate_annotation_consultation_weight(user_id):
    """
    Calcule le poids de consultation d'annotations (𝒘_𝑷𝑪𝑶𝑨) et l'insère dans la base de données.
    """
    profile = fetch_user_profile(user_id)
    if profile.empty:
        return

    consulted_annotations = profile['pcoa_items_count'].values[0]
    total_interactions = profile['pca_items_count'].values[0] + consulted_annotations

    if total_interactions == 0:
        value = 0
    else:
        value = consulted_annotations / total_interactions

    insert_indicator(user_id, "CAT2-Engagement", "Weight of Annotation COnsultation Profile", "WPCOA", value)


# --------------------------------------------
# Stratégie 2 : Maintien de l'Engagement Basé sur la Proximité des Lieux Annotés
# --------------------------------------------

def calculate_pla(user_id, user_locations):
    """
    Calcul du PLA (Proximité des lieux annotés) pour un utilisateur donné.
    """
    print('Computing PLA for user: ', user_id)
    # Définir les filtres pour exclure les annotations de l'utilisateur actuel
    field_filters_to_exclude = {
        'user': [user_id]
    }

    # Récupérer les annotations en excluant celles de l'utilisateur actuel
    annotations = fetch_annotations(field_filters_to_exclude=field_filters_to_exclude)
    
    if annotations.empty:
        pla = 0
    else:
        distances = []

        for _, annotation in annotations.iterrows():
            coords = annotation['coords']
            coords_dict = json.loads(coords)
            annotation_location = {
                "lat": coords_dict['lat'],
                "lon": coords_dict['lon']
            }
            dist = calculate_distance(user_locations, annotation_location)
            if dist is not None:
                distances.append(dist)

        # Calculer la distance moyenne
        pla = sum(distances) / len(distances) if distances else 0
    
    insert_indicator(user_id, "CAT2-Engagement", "Proximié des Lieux Annotés", "PLA", pla)

# --------------------------------------------
# Stratégie 3 : suggestion de parcours basée profil de parcours
# --------------------------------------------


def calculate_path_creation_weight(user_id):
    """
    Calcule le poids de création de parcours (𝒘_𝑷𝑪𝑷) et l'insère dans la base de données.
    """
    profile = fetch_user_profile(user_id)
    
    if profile.empty:
        return
    
    # Extraire les valeurs avec une vérification explicite pour gérer les None
    created_paths = profile['pcp_items_count'].values[0]
    pcop_items_count = profile['pcop_items_count'].values[0]

    # Vérifier que les valeurs ne sont pas None, sinon les remplacer par 0
    created_paths = created_paths if created_paths is not None else 0
    pcop_items_count = pcop_items_count if pcop_items_count is not None else 0
    
    total_interactions = created_paths + pcop_items_count

    

    if total_interactions == 0:
        value = 0
    else:
        value = created_paths / total_interactions

    insert_indicator(user_id, "CAT2-Engagement", "Weight of Tour Creation Profile", "WPCP", value)


def calculate_path_consultation_weight(user_id):
    """
    Calcule le poids d'intérêts de parcours (𝒘_𝑷𝑪𝒐𝑷) et l'insère dans la base de données.
    """
    profile = fetch_user_profile(user_id)
    
    if profile.empty:
        return
    
    # Extraire les valeurs, avec vérification pour éviter les None
    consulted_paths = profile['pcop_items_count'].values[0]
    created_paths = profile['pcp_items_count'].values[0]

    # Vérifier que les valeurs ne sont pas None, sinon les remplacer par 0
    consulted_paths = consulted_paths if consulted_paths is not None else 0
    created_paths = created_paths if created_paths is not None else 0

    total_interactions = created_paths + consulted_paths

    if total_interactions == 0:
        value = 0
    else:
        value = consulted_paths / total_interactions

    # Insérer l'indicateur calculé
    insert_indicator(user_id, "CAT2-Engagement", "Weight of Tour COnsultation Profile", "WPCOP", value)



# --------------------------------------------
# Stratégie 4 : Réengagement à travers les Interactions Utilisateur
# --------------------------------------------
def calculate_id(user_id, period_days=30):
    """
    Calcul de l'Indicateur de Désengagement (ID) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur
    :param period_days: Nombre de jours pour la période d'évaluation
    :return: Indicateur de Désengagement (ID)
    """
    end_date = datetime.now()
    start_date = end_date - timedelta(days=period_days)
    
    # Types de logs avec poids associés
    log_weights = {
        'm:addPost': 5,
        'm:editPost': 4,
        'm:openPost': 3,
        'm:openMessage': 2,
        'm:startTour': 6,
        'm:addMessage': 4,
        'm:addEmoji': 1,
        'm:movePost': 2,
        'm:editTour': 5,
        'm:addFavorite': 3,
        'm:moveTour': 4,
        'm:addressSearch': 3,
        'm:Filter': 2,
        'm:deletePost': 1,
        'm:deleteMessage': 1
    }
    
    # Récupérer les logs d'activité pour chaque type
    total_activity = 0
    for log_type, weight in log_weights.items():
        logs = fetch_user_logs_by_type(user_id, start_date, end_date, log_type)
        total_activity += len(logs) * weight
    
    # Calculer l'ID comme la moyenne des activités pondérées
    num_logs = len(log_weights)  # Nombre de types de logs considérés
    ID = total_activity / period_days if period_days > 0 else 0
    
    # Insertion de l'indicateur ID dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Indice Désengagement", "ID", ID)
    
    return ID

# --------------------------------------------
# Stratégie 5 : Réengagement Basé sur l’Historique des Activités
# --------------------------------------------
def calculate_sea(user_id, indicators_df):
    """
    Calcule le Score d'Engagement d'Annotations (SEA) basé sur l'évolution de la création et consultation d'annotations.
    """
    profile = fetch_user_profile(user_id)
    if profile.empty:
        return

    # Pondérations
    alpha = 0.5
    beta = 0.5

    # Récupérer les données actuelles
    current_w_pca = profile['pca_items_count'].values[0] if 'pca_items_count' in profile.columns else 0
    current_w_pcoa = profile['pcoa_items_count'].values[0] if 'pcoa_items_count' in profile.columns else 0

    # Récupérer les données historiques pour PCA et PCOA
    past_w_pca = get_indicator_value(indicators_df, 'pca_items_count', offset=1)  # Offset pour 30 jours en arrière
    past_w_pcoa = get_indicator_value(indicators_df, 'pcoa_items_count', offset=1)  # Offset pour 30 jours en arrière

    # Vérifier l'existence des données historiques, sinon utiliser 0
    past_w_pca = past_w_pca if past_w_pca is not None else 0
    past_w_pcoa = past_w_pcoa if past_w_pcoa is not None else 0

    # Calculer l'évolution
    delta_w_pca = current_w_pca - past_w_pca
    delta_w_pcoa = current_w_pcoa - past_w_pcoa

    # Calculer le SEA
    sea_score = (alpha * (delta_w_pca / past_w_pca if past_w_pca != 0 else 0) +
                 beta * (delta_w_pcoa / past_w_pcoa if past_w_pcoa != 0 else 0))

    # Insérer le score dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Score d'Engagement d'Annotations", "SEA", sea_score)



def calculate_sep(user_id, indicators_df):
    """
    Calcule le Score d'Engagement des Parcours (SEP) basé sur l'évolution de la création et consultation de parcours.
    """
    profile = fetch_user_profile(user_id)
    if profile.empty:
        return

    # Pondérations
    gamma = 0.5
    delta = 0.5

    # Récupérer les données actuelles
    current_w_pcp = profile['pcp_items_count'].values[0] if 'pcp_items_count' in profile.columns else 0
    current_w_pcop = profile['pcop_items_count'].values[0] if 'pcop_items_count' in profile.columns else 0

    # S'assurer que les valeurs actuelles sont des nombres (0 si None)
    current_w_pcp = float(current_w_pcp) if current_w_pcp is not None else 0.0
    current_w_pcop = float(current_w_pcop) if current_w_pcop is not None else 0.0

    # Récupérer les données historiques pour PCP et PCOP
    past_w_pcp = get_indicator_value(indicators_df, 'pcp_items_count', offset=1)  # Offset pour 30 jours en arrière
    past_w_pcop = get_indicator_value(indicators_df, 'pcop_items_count', offset=1)  # Offset pour 30 jours en arrière

    # Vérifier l'existence des données historiques, sinon utiliser 0
    past_w_pcp = past_w_pcp if past_w_pcp is not None else 0.0
    past_w_pcop = past_w_pcop if past_w_pcop is not None else 0.0

    # Calculer l'évolution
    delta_w_pcp = current_w_pcp - past_w_pcp
    delta_w_pcop = current_w_pcop - past_w_pcop

    # Calculer le SEP
    sep_score = (gamma * (delta_w_pcp / past_w_pcp if past_w_pcp != 0 else 0) +
                 delta * (delta_w_pcop / past_w_pcop if past_w_pcop != 0 else 0))

    # Insérer le score dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Score d'Engagement des Parcours", "SEP", sep_score)


# --------------------------------------------
#  Stratégie 6 : Réengagement à Travers le Contenu de la Communauté
# --------------------------------------------
def calculate_sca(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Score de Consultation des Annotations (SCA) pour un utilisateur.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les consultations sont considérées.
    :return: SCA normalisé entre 0 et 1.
    """
    # Récupérer les annotations vues par l'utilisateur depuis la date spécifiée
    viewed_annotations = fetch_user_viewed_annotations(user_id, since_date)
    
    # Compter le nombre d'annotations vues
    A = len(viewed_annotations)

    # Récupérer le total d'annotations disponibles
    total_annotations = len(fetch_annotations())

    # Calculer le SCA comme une proportion
    sca = (A / total_annotations) if total_annotations > 0 else 0

    # Insérer l'indicateur SCA dans la base de données
    insert_indicator(
        user_id,
        "CAT2-Engagement",
        "Score de Consultation des Annotations",
        "SCA",  # Indicateur Score de Consultation des Annotations
        value=sca
    )
    
    return sca

def calculate_scp(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Score de Consultation des Parcours (SCP) pour un utilisateur.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les parcours sont considérés.
    :return: SCP normalisé entre 0 et 1.
    """
    # Récupérer les parcours consultés par l'utilisateur depuis la date spécifiée
    viewed_paths = fetch_user_viewed_tours(user_id, since_date) 
    # Compter le nombre de parcours consultés
    P = len(viewed_paths)

    # Récupérer le total de parcours disponibles
    total_paths = fetch_tours() 

    # Convertir total_paths en entier si nécessaire
    try:
        total_paths = len(total_paths)
    except ValueError:
        total_paths = 0  # Si la conversion échoue, on met total_paths à 0

    # Calculer le SCP comme une proportion
    scp = (P / total_paths) if total_paths > 0 else 0

    # Insérer l'indicateur SCP dans la base de données
    insert_indicator(
        user_id,
        "CAT2-Engagement",
        "Score de Consultation des Parcours",
        "SCP",  # Indicateur Score de Consultation des Parcours
        value=scp
    )
    
    return scp


def calculate_sis(user_id, since_date=DEFAULT_SINCE_DATE):
    """
    Calcule le Score d'Interaction Sociale (SIS) pour un utilisateur.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les interactions sont considérées.
    :return: SIS normalisé entre 0 et 1.
    """
    # Récupérer le nombre de commentaires et de réactions depuis la date spécifiée
    C = fetch_actions_count(user_id, 'm:addMessage')  # Nombre de commentaires
    R = fetch_actions_count(user_id, 'm:addEmoji')    # Nombre de réactions
    
    # Total des commentaires et réactions disponibles dans la période
    total_comments = fetch_comments()  # Total des commentaires dans la période
    # total_reactions = fetch_total_reactions_count()  # Total des réactions dans la période
    
    total_interactions = total_comments # + total_reactions  # Somme des interactions possibles

    # Calculer le SIS comme une proportion
    sis = ((C + R) / len(total_interactions)) if len(total_interactions) > 0 else 0

    # Insérer l'indicateur SIS dans la base de données
    insert_indicator(
        user_id,
        "CAT2-Engagement",
        "Score d'Interaction Sociale",
        "SIS",
        value=sis
    )
    
    return sis

############################################################################################
############################################################################################
#  OLD VERSION
def calculate_tel(user_id):
    """
    Calcul du TEL (Taux d'exploration de lieux annotés) pour un utilisateur donné.
    """
    print('Computing TEL for user: ', user_id)
    logs = fetch_logs_for_user(user_id)
    if not logs:
        tel = 0
    else:
        num_explorations = sum(1 for log in logs if 'open_annotation' in log['_source']['type'] or 'open_route' in log['_source']['type'])
        num_notifications = sum(1 for log in logs if 'notification' in log['_source']['type'])

        tel = num_explorations / num_notifications if num_notifications > 0 else 0

    insert_indicator(user_id, "CAT2-Engagement", "Exploration Contextuelle des Lieux Annotés", "TEL", tel)


# --------------------------------------------
# Stratégie 2 : Annotation Contextuelle Opportune
# --------------------------------------------
THRESHOLD_OA = 3
THRESHOLD_TCA = 0.2

def identify_non_annotated_places(locations, annotations, threshold_distance=0.05):
    """
    Identifie les lieux intéressants non encore annotés.
    
    :param locations: Liste des emplacements visités ou recherchés par l'utilisateur (avec coordonnées 'lat' et 'lon').
    :param annotations: Liste des annotations existantes (avec coordonnées 'lat' et 'lon').
    :param threshold_distance: Distance maximale (en kilomètres) pour considérer qu'un lieu est déjà annoté. Par défaut, 50 mètres.
    :return: Liste des emplacements non annotés.
    """
    non_annotated_places = []
    
    for loc in locations:
        lat = loc.get('lat')
        lon = loc.get('lon')
        
        is_annotated = False
        for ann in annotations:
            ann_lat = ann.get('lat')
            ann_lon = ann.get('lon')
            distance = calculate_distance({"lat": lat, "lon": lon}, {"lat": ann_lat, "lon": ann_lon})
            
            if distance <= threshold_distance:
                is_annotated = True
                break
        
        if not is_annotated:
            non_annotated_places.append(loc)
    
    return non_annotated_places


def calculate_oa(user_id, start_date=None, days=None):
    """
    Calcul de l'Opportunité d'Annotation (OA) pour un utilisateur donné.
    """
    print('Computing OA for user: ', user_id)
    locations = fetch_user_locations(user_id, start_date, days)
    annotations = fetch_user_annotations(user_id, start_date)
    
    non_annotated_places = identify_non_annotated_places(locations, annotations)
    oa = len(non_annotated_places)
    
    insert_indicator(user_id, "CAT2-Engagement", "Annotation Contextuelle Opportune", "OA", oa)
    insert_indicator(user_id, "CAT2-Engagement", "Annotation Contextuelle Opportune", "non_annotated_places", non_annotated_places)
    
    return non_annotated_places, oa

def calculate_tca(user_id, start_date=None, days=None):
    """
    Calcul du Taux de Création d'Annotations (TCA) pour un utilisateur donné.
    """
    print('Computing TCA for user: ', user_id)
    annotations = fetch_user_annotations(user_id, start_date)
    locations = fetch_user_locations(user_id, start_date, days)
    
    if not locations or not annotations:
        tca = 0
    else:
        num_annotations = len([ann for ann in annotations if ann['created']])
        num_locations = len(locations)
        
        tca = num_annotations / num_locations if num_locations > 0 else 0
    
    insert_indicator(user_id, "CAT2-Engagement", "Annotation Contextuelle Opportune", "TCA", tca)
    
    return tca


# --------------------------------------------------
# Stratégie 3 : Découverte Contextuelle de Parcours
# --------------------------------------------------
def correct_coordinates(lat, lon):
    """
    Corrige les coordonnées pour s'assurer qu'elles sont dans les plages valides.
    
    :param lat: Latitude à corriger
    :param lon: Longitude à corriger
    :return: Tuple contenant la latitude et la longitude corrigées
    """
    lat = max(min(lat, 90), -90)
    lon = max(min(lon, 180), -180)
    return lat, lon

def calculate_min_distance_to_tour(user_location, tour_coordinates):
    """
    Calcule la distance minimale entre la localisation de l'utilisateur et un parcours.

    :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
    :param tour_coordinates: Liste de tuples représentant les points du parcours [(lat, lon), (lat, lon), ...].
    :return: Distance minimale en kilomètres.
    """
    min_distance = float('inf')



    # Assurez-vous que user_location est bien un dictionnaire
    if not isinstance(user_location, dict) or 'lat' not in user_location or 'lon' not in user_location:
        return None

    for coord in tour_coordinates:
        point = correct_coordinates(coord[1], coord[0])  # (latitude, longitude)
        distance = geodesic((user_location['lat'], user_location['lon']), point).kilometers
        
        if distance < min_distance:
            min_distance = distance

    return min_distance


def calculate_tep(user_id):
    """
    Calcul du Taux d'exploration de Parcours (TEP) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur
    :return: TEP
    """
    print('Computing TEP for user: ', user_id)
    logs = fetch_logs_for_user(user_id)
    
    if not logs:
        tep = 0
    else:
        # Comptage des explorations de parcours (supposant que chaque log pertinent est une exploration)
        num_explorations = sum(1 for log in logs if 'tour_exploration' in log['_source']['type'])
        
        # Comptage des notifications envoyées
        num_notifications = sum(1 for log in logs if 'notification' in log['_source']['type'] and 'tour_recommendation' in log['_source']['subtype'])
        
        tep = num_explorations / num_notifications if num_notifications > 0 else 0
    
    # Insertion dans la base de données 
    insert_indicator(user_id, "CAT2-Engagement", "Découverte Contextuelle de Parcours", "TEP", tep)

def calculate_pp(user_id, user_location):
    """
    Calcul de la Proximité des Parcours (PP) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur
    :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
    :return: Proximité des Parcours (PP)
    """
    print('Computing PP for user: ', user_id)
    # Récupération de tous les parcours depuis la base de données (retourne un DataFrame ou une liste vide)
    tours = fetch_tours()
    
    pp = 0
    if isinstance(tours, pd.DataFrame) and not tours.empty:
        distances = []
        
        for _, tour in tours.iterrows():
            # Extraction des coordonnées du champ 'body'
            coordinates = json.loads(tour['body'])
            # Conversion en une liste de tuples [(lat, lon), (lat, lon), ...]
            tour_coordinates = [(coordinates[i], coordinates[i+1]) for i in range(0, len(coordinates), 2)]
            # Calcul de la distance minimale entre l'utilisateur et ce parcours
            dist = calculate_min_distance_to_tour(user_location, tour_coordinates)
            if dist:
                distances.append(dist)
        
        # Calcul de la distance moyenne si des distances ont été trouvées
        pp = sum(distances) / len(distances) if distances else 0
    else:
        print("Soit l'utilisateur n'a pas de parcours ou bien il y a erreur")
    
    # Insertion dans la base de données 
    insert_indicator(user_id, "CAT2-Engagement", "Découverte Contextuelle de Parcours", "PP", pp)


# --------------------------------------------
# Stratégie 4 : Création Contextuelle de Parcours
# --------------------------------------------
import json
from geopy.distance import geodesic

def correct_coordinates(lat, lon):
    """
    Corrige les coordonnées pour s'assurer qu'elles sont dans les plages valides.
    
    :param lat: Latitude à corriger
    :param lon: Longitude à corriger
    :return: Tuple contenant la latitude et la longitude corrigées
    """
    lat = max(min(lat, 90), -90)
    lon = max(min(lon, 180), -180)
    return lat, lon

def calculate_min_distance_to_tour(user_location, tour_coordinates):
    """
    Calcule la distance minimale entre la localisation de l'utilisateur et un parcours.

    :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
    :param tour_coordinates: Liste de tuples représentant les points du parcours [(lat, lon), (lat, lon), ...].
    :return: Distance minimale en kilomètres.
    """
    min_distance = float('inf')
    if not isinstance(user_location, dict) or 'lat' not in user_location or 'lon' not in user_location:
        return None

    for coord in tour_coordinates:
        point = correct_coordinates(coord[1], coord[0])
        distance = geodesic((user_location['lat'], user_location['lon']), point).kilometers
        if distance < min_distance:
            min_distance = distance

    return min_distance

def is_within_proximity(tour_coordinates, user_location, proximity_threshold_km=5):
    """
    Vérifie si l'une des coordonnées du tour est à proximité de la localisation de l'utilisateur.

    :param tour_coordinates: Liste de tuples représentant les points du parcours [(lat, lon), (lat, lon), ...].
    :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
    :param proximity_threshold_km: Distance maximale en kilomètres pour considérer le tour comme proche.
    :return: True si le tour est à proximité de l'utilisateur, False sinon.
    """
    min_distance = calculate_min_distance_to_tour(user_location, tour_coordinates)
    return min_distance is not None and min_distance <= proximity_threshold_km

def is_opportunity_for_user(tour,  user_interests):
    """
    Détermine si un tour représente une opportunité pour l'utilisateur.
    
    :param tour: Dictionnaire représentant un tour, avec une clé 'body' contenant les coordonnées du parcours.
    :param user_id: ID de l'utilisateur.
    :return: True si le tour représente une opportunité pour l'utilisateur, False sinon.
    """
    try:        
        body = json.loads(tour['body'])
    except json.JSONDecodeError as e:
        print(f"Erreur lors de la conversion du corps du tour en liste de coordonnées: {e}")
        return False

    tour_coordinates = [(body[i+1], body[i]) for i in range(0, len(body), 2)]
    

    for interest in user_interests:
        if is_within_proximity(tour_coordinates, interest['location']):
            return True

    return False

def get_user_interests(user_id):
    """
    Récupère les centres d'intérêt d'un utilisateur basés sur les annotations, les posts ouverts et les recherches d'adresse.

    :param user_id: ID de l'utilisateur
    :return: Liste des centres d'intérêt sous forme de dictionnaires avec les coordonnées
    """
    interests = []

    annotations_df = fetch_annotations_by_user(user_id)
    posts_df = fetch_opened_posts(user_id)
    searches_df = fetch_address_searches(user_id)

    for index, row in annotations_df.iterrows():
        coords = json.loads(row['coords'])
        if coords:
            interests.append({"location": {"lat": coords['lat'], "lon": coords['lon']}})

    for index, row in posts_df.iterrows():
        coords = json.loads(row['coords'])
        if len(coords) % 2 == 0:
            for i in range(0, len(coords), 2):
                interests.append({"location": {"lat": coords[i+1], "lon": coords[i]}})

    for index, row in searches_df.iterrows():
        coords = json.loads(row['coords'])
        if len(coords) % 2 == 0:
            for i in range(0, len(coords), 2):
                interests.append({"location": {"lat": coords[i+1], "lon": coords[i]}})

    return interests

def calculate_ocp(tours, user_id):
    """
    Calcul de l'Opportunité de Création de Parcours (OCP) pour un utilisateur donné.
    """
    opportunities = 0
    user_interests = get_user_interests(user_id)
    
    # Vérifie si le DataFrame tours n'est pas vide
    if not tours.empty:
        # Parcourir les lignes du DataFrame tours avec iterrows()
        for _, tour in tours.iterrows():
            if is_opportunity_for_user(tour, user_interests):
                opportunities += 1

    # Insérer l'indicateur calculé dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Création Contextuelle de Parcours", "OCP", opportunities)


def calculate_tcp(user_id):
    """
    Calcul du Taux de Création de Parcours (TCP) pour un utilisateur donné.
    """
    print('Computing TCP for user: ', user_id)
    recommendations = fetch_recommendations()
    N_recommendations = len(recommendations)

    created_tours = fetch_tours()
    N_parcours = len(created_tours)

    TCP = N_parcours / N_recommendations if N_recommendations > 0 else 0

    insert_indicator(user_id, "CAT2-Engagement", "Création Contextuelle de Parcours", "TCP", TCP)




# --------------------------------------------
# Stratégie 5 : Réactivation de l'Utilisateur Désengagé
# --------------------------------------------


def fetch_user_recommendations(user_id):
    """
    Récupère les recommandations envoyées à un utilisateur.
    
    :param user_id: ID de l'utilisateur
    :return: DataFrame contenant les recommandations
    """
    # Simulation de récupération des recommandations
    # Remplacer par une vraie récupération de données
    data = {
        'date_sent': [datetime.now() - timedelta(days=i) for i in range(10)],
        'recommendation_id': range(1, 11)
    }
    df = pd.DataFrame(data)
    return df

def fetch_user_reactivations(user_id):
    """
    Récupère les données sur les réactivations d'un utilisateur après recommandation.
    
    :param user_id: ID de l'utilisateur
    :return: DataFrame contenant les réactivations
    """
    # Simulation de récupération des réactivations
    # Remplacer par une vraie récupération de données
    data = {
        'date_reactivated': [datetime.now() - timedelta(days=i) for i in range(5)],
        'recommendation_id': [1, 2, 3, 5, 7]
    }
    df = pd.DataFrame(data)
    return df

def calculate_tru(user_id):
    """
    Calcul du Taux de Réactivation des Utilisateurs (TRU) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur
    :return: Taux de Réactivation des Utilisateurs (TRU)
    """
    print('Computing TRU for user: ', user_id)
    # Récupérer les recommandations envoyées
    recommendations_df = fetch_user_recommendations(user_id)
    N_recommendations = len(recommendations_df)
    
    # Récupérer les réactivations suite aux recommandations
    reactivations_df = fetch_user_reactivations(user_id)
    N_reactivés = len(reactivations_df)
    
    # Calculer le TRU
    TRU = N_reactivés / N_recommendations if N_recommendations > 0 else 0
    
    # Insertion de l'indicateur TRU dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Réactivation de l’Utilisateur Désengagé", "TRU", TRU)
    
    return TRU


# --------------------------------------------
# Stratégie 6 : Recommandations basées sur les Intérêts Récurrents
# --------------------------------------------
def calculate_air(user_id, since_date=None):
    """
    Calcule l'Analyse des Intérêts Récurrents (AIR) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les annotations et parcours sont considérés.
    :return: Valeur AIR
    """
    print('Computing AIR for user: ', user_id)
    # Récupérer toutes les annotations et tours de l'utilisateur
    annotations = fetch_annotations_by_user(user_id, since_date)
    tours = fetch_user_tours(user_id, since_date)

    # Initialiser les compteurs
    annotation_counts = defaultdict(lambda: {'frequency': 0, 'place_type': defaultdict(int)})
    tour_counts = defaultdict(lambda: {'count': 0, 'length': defaultdict(int)})

    # Compter les occurrences de chaque type d'annotation
    for _, row in annotations.iterrows():
        timing = row.get('timing', 'unknown')
        place_type = row.get('placeType', 'unknown')
        annotation_counts[timing]['frequency'] += 1
        annotation_counts[timing]['place_type'][place_type] += 1

    # Compter les occurrences de chaque type de parcours
    for _, row in tours.iterrows():
        tour_type = row.get('type', 'unknown')
        tour_length = len(json.loads(row.get('body', '[]')))  # Nombre de points dans le parcours
        tour_counts[tour_type]['count'] += 1
        tour_counts[tour_type]['length'][tour_length] += 1

    # Calculer la fréquence totale et la moyenne
    total_annotations = sum(d['frequency'] for d in annotation_counts.values())
    total_tours = sum(d['count'] for d in tour_counts.values())
    total_categories = len(annotation_counts) + len(tour_counts)
    
    if total_categories == 0:
        AIR = 0
    else:
        AIR = (total_annotations + total_tours) / total_categories
    
    # Enregistrer l'indicateur AIR
    insert_indicator(user_id, "CAT2-Engagement", "Intérêts Récurrents", "AIR", AIR)
    
    return AIR


# --------------------------------------------
# Stratégie 7 : Temps Passé sur les Annotations - TODO
# --------------------------------------------

def calculate_tpa(user_id):
    """
    Calcul du Temps Passé sur les Annotations (TPA) pour un utilisateur donné.
    """
    print('Computing TPA for user: ', user_id)
    # Période d'évaluation (ici 30 jours avant aujourd'hui)
    start_date = (datetime.utcnow() - timedelta(days=30)).isoformat()
    
    # Récupère les logs d'activité de l'utilisateur pour la période donnée
    logs = fetch_user_logs_with_time(user_id, start_date)
    
    # Calculer le temps passé sur les annotations
    total_time = 0
    count = 0
    
    for log in logs:
        begin_date = log['_source'].get('begin')
        if begin_date:
            begin_datetime = datetime.fromisoformat(begin_date)
            time_spent = (datetime.utcnow() - begin_datetime).total_seconds() / 3600  # Convertir en heures
            
            if time_spent > 0:
                total_time += time_spent
                count += 1
    
    # Calculer le TPA
    if count > 0:
        TPA = total_time / count
    else:
        TPA = 0

    # Insérer l'indicateur TPA dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Temps Passé sur les Annotations", "TPA", TPA)

# --------------------------------------------
# Stratégie 8 : Recommandations par Filtres Préférés
# --------------------------------------------
def calculate_pf(user_id, since_date=None):
    """
    Calcule les Préférences de Filtres (PF) pour un utilisateur donné en identifiant les motifs spécifiques récurrents.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les filtres sont considérés.
    :return: Tuple (PF, motifs), où PF est la valeur calculée et motifs est un dictionnaire des motifs récurrents.
    """
    print('Computing PF for user: ', user_id)
    # Récupérer les filtres appliqués par l'utilisateur
    filters = fetch_user_filters(user_id, since_date)

    # Initialiser les compteurs pour chaque motif de filtre
    institution_counts = defaultdict(int)
    nationality_counts = defaultdict(int)
    icon_counts = defaultdict(int)
    emoticon_counts = defaultdict(int)
    tag_counts = defaultdict(int)
    date_range_counts = defaultdict(int)

    # Parcourir chaque filtre appliqué
    for _, row in filters.iterrows():
        # Compter les institutions
        institution = row.get('institution')
        if isinstance(institution, list):
            for inst in institution:
                institution_counts[inst] += 1

        # Compter les nationalités
        nationality = row.get('nationality')
        if isinstance(nationality, list):
            for inst in nationality:
                nationality_counts[inst] += 1

        # Compter les icônes
        icons = row.get('icons', [])
        if isinstance(icons, list):
            for icon in icons:
                icon_counts[icon] += 1

        # Compter les émoticônes
        emoticons = row.get('emoticon', [])
        if isinstance(emoticons, list):      
            for emoticon in emoticons:
                emoticon_counts[emoticon] += 1

        # Compter les tags
        tags = row.get('tags', [])
        if isinstance(tags, list):           
            for tag in tags:
                tag_counts[tag] += 1

        # Compter les plages de dates (format standardisé de date)
        begin_date = row.get('beginDate')
        end_date = row.get('endDate')
        if begin_date and end_date:
            date_range = f"{begin_date} - {end_date}"
            date_range_counts[date_range] += 1

    # Identifier les motifs les plus fréquents
    top_institutions = [k for k, v in institution_counts.items() if v > 1]
    top_nationalities = [k for k, v in nationality_counts.items() if v > 1]
    top_icons = [k for k, v in icon_counts.items() if v > 1]
    top_emoticons = [k for k, v in emoticon_counts.items() if v > 1]
    top_tags = [k for k, v in tag_counts.items() if v > 1]
    top_date_ranges = [k for k, v in date_range_counts.items() if v > 1]

    # Consolider les motifs
    motifs = {
        'institutions': top_institutions,
        'nationalities': top_nationalities,
        'icons': top_icons,
        'emoticons': top_emoticons,
        'tags': top_tags,
        'date_ranges': top_date_ranges
    }

    # Calculer la fréquence totale et la moyenne
    total_filters = (
        sum(institution_counts.values()) +
        sum(nationality_counts.values()) +
        sum(icon_counts.values()) +
        sum(emoticon_counts.values()) +
        sum(tag_counts.values()) +
        sum(date_range_counts.values())
    )

    total_motifs = len(top_institutions) + len(top_nationalities) + len(top_icons) + len(top_emoticons) + len(top_tags) + len(top_date_ranges)

    if total_motifs == 0:
        PF = 0
    else:
        PF = total_filters / total_motifs

    # Convertir les motifs en chaîne JSON
    motifs_json = json.dumps(motifs)
    # Enregistrer l'indicateur PF
    insert_indicator(user_id, "CAT2-Engagement", "Recommandations Basées sur les Filtres Préférés", "PF", PF)
    insert_indicator(user_id, "CAT2-Engagement", "Recommandations Basées sur les Filtres Préférés", "PF_motifs", motifs_json)
    
    return PF, motifs

# --------------------------------------------
# Stratégie 9 : Optimisation des Sessions Courtes
# --------------------------------------------

def calculate_os(user_id):
    """
    Calcul de l'Optimisation des Sessions (OS) pour un utilisateur donné.
    """
    print('Computing OS for user: ', user_id)
    # Période d'évaluation (ici 30 jours avant aujourd'hui)
    start_date = (datetime.utcnow() - timedelta(days=30)).isoformat()
    
    # Reconstruire les sessions pour l'utilisateur
    sessions = reconstruct_sessions(user_id, start_date)
    
    # Calculer la durée des sessions
    session_durations = []
    for session in sessions:
        if len(session) > 1:
            start_time = datetime.fromisoformat(session[0]['_source']['begin'].replace("Z", "+00:00"))
            end_time = datetime.fromisoformat(session[-1]['_source']['begin'].replace("Z", "+00:00"))
            duration = (end_time - start_time).total_seconds() / 60  # durée en minutes
            session_durations.append(duration)
    
    # Calculer OS pour les sessions courtes
    short_sessions = [duration for duration in session_durations if duration <= 10]  # Sessions courtes
    total_sessions = len(short_sessions)
    
    if total_sessions > 0:
        os = sum(short_sessions) / total_sessions
    else:
        os = 0

    # Insérer les indicateurs OS dans la base de données
    insert_indicator(user_id, "CAT2-Engagement", "Durée des sessions courtes", "SD", os)

# --------------------------------------------
# Stratégie 10 : Contenu Populaire
# --------------------------------------------
from collections import defaultdict
import pandas as pd

def build_user_profile(user_id, since_date=None):
    """
    Construit le profil d'intérêt de l'utilisateur à partir de ses annotations.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date depuis laquelle les annotations sont considérées.
    :return: Dictionnaire représentant le profil d'intérêt de l'utilisateur.
    """
    annotations = fetch_user_annotations(user_id, since_date)

    if annotations.empty:
        return None
    
    profile = {
        'icons': defaultdict(int),
        'tags': defaultdict(int),
        'timing': defaultdict(int),
        'placeType': defaultdict(int),
        'text_length': defaultdict(int),
        'has_emoticon': 0,
        'has_image': 0
    }
    
    for _, row in annotations.iterrows():
        # Traiter les icônes
        icons = row.get('icons', '')
        if isinstance(icons, str):
            icons = icons.split(',')
        for icon in icons:
            profile['icons'][icon.strip()] += 1
        
        # Traiter les tags
        tags = row.get('tags', '')
        if isinstance(tags, str):
            tags = tags.split()
        for tag in tags:
            profile['tags'][tag.strip()] += 1
        
        # Traiter le timing
        timing = row.get('timing', 'unknown')
        profile['timing'][timing] += 1
        
        # Traiter le type de lieu
        place_type = row.get('placeType', 'unknown')
        profile['placeType'][place_type] += 1
        
        # Calculer la longueur du texte
        comment = row.get('comment', '')
        text_length = len(comment)
        profile['text_length'][text_length] += 1
        
        # Vérifier la présence d'une emoticone
        has_emoticon = 1 if row.get('emoticon') else 0
        profile['has_emoticon'] += has_emoticon
        
        # Vérifier la présence d'une image
        has_image = 1 if row.get('figure') else 0
        profile['has_image'] += has_image
    
    return profile

import pandas as pd

def calculate_annotation_affinity(profile, annotation_details):
    """
    Calcule l'affinité entre une annotation et le profil d'intérêt de l'utilisateur.
    
    :param profile: Profil d'intérêt de l'utilisateur.
    :param annotation_details: Détails de l'annotation, sous forme de DataFrame.
    :return: Score d'affinité.
    """
    # Assurer que annotation_details est un DataFrame et obtenir la première ligne
    if isinstance(annotation_details, pd.DataFrame):
        if annotation_details.empty:
            return 0  # Retourner 0 si le DataFrame est vide
        annotation_details = annotation_details.iloc[0].to_dict()

    # Poids de chaque critère
    weight_tags = 0.3
    weight_icons = 0.2
    weight_timing = 0.2
    weight_placeType = 0.15
    weight_text_length = 0.1
    weight_emoticon_image = 0.05

    # Calcul des affinités pour chaque critère

    # Tags
    annotation_tags = annotation_details.get('tags', '')
    if isinstance(annotation_tags, str):
        annotation_tags = annotation_tags.split()
    affinity_tags = sum(profile['tags'].get(tag, 0) for tag in annotation_tags) * weight_tags

    # Icons
    annotation_icons = annotation_details.get('icons', '')
    if isinstance(annotation_icons, str):
        annotation_icons = annotation_icons.split(',')
    affinity_icons = sum(profile['icons'].get(icon.strip(), 0) for icon in annotation_icons) * weight_icons

    # Timing
    timing = annotation_details.get('timing', 'unknown')
    affinity_timing = profile['timing'].get(timing, 0) * weight_timing
    
    # Type de lieu
    place_type = annotation_details.get('placeType', 'unknown')
    affinity_placeType = profile['placeType'].get(place_type, 0) * weight_placeType
    
    # Longueur du texte
    text_length = len(annotation_details.get('comment', ''))
    affinity_text_length = profile['text_length'].get(text_length, 0) * weight_text_length
    
    # Présence d'emoticônes et d'images
    has_emoticon = 1 if annotation_details.get('emoticon', '') else 0
    has_image = 1 if annotation_details.get('figure', '') else 0
    affinity_emoticon_image = (profile['has_emoticon'] * has_emoticon +
                               profile['has_image'] * has_image) * weight_emoticon_image

    # Somme des affinités pour l'annotation
    total_affinity = (affinity_tags + affinity_icons + affinity_timing +
                      affinity_placeType + affinity_text_length + affinity_emoticon_image)

    return total_affinity




def calculate_aua(user_id, popularity_data, since_date=None):
    """
    Calcule l'Affinité Utilisateur avec les Annotations Populaires (AUA) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur.
    :param popularity_data: Liste des annotations populaires avec leurs pourcentages de vues.
    :param since_date: Date depuis laquelle les annotations sont considérées (optionnel).
    :return: Score AUA et liste d'IDs des annotations avec affinité.
    """
    print('Computing AUA for user: ', user_id)
    # Construire le profil d'intérêt de l'utilisateur
    profile = build_user_profile(user_id, since_date)
    if not profile:
        return None

    aua_score = 0
    total_weight = 0
    affinity_scores = []

    for annotation in popularity_data:
        annotation_id = annotation['annotation_id']
        view_percentage = annotation['view_percentage']
        
        # Récupérer les détails de l'annotation
        annotation_details = fetch_annotations_by_id(annotation_id)
        if annotation_details.empty:
            continue
        
        # Convertir les détails en dictionnaire pour la fonction d'affinité
        annotation_dict = annotation_details.iloc[0].to_dict()
        
        # Calcul des affinités avec la fonction généralisée
        affinity = calculate_annotation_affinity(profile, annotation_dict)
        
        # Conserver les scores d'affinité pour cette annotation
        affinity_scores.append((annotation_id, affinity))
        
        aua_score += affinity * view_percentage
        total_weight += view_percentage

    # Calculer le score AUA final
    AUA = aua_score / total_weight if total_weight > 0 else 0
    
    # Créer la liste des annotations avec affinité positive
    affinity_annotations = [annotation_id for annotation_id, score in affinity_scores if score > 0]
    
    # Convertir la liste des IDs en chaîne JSON pour le stockage
    affinity_annotations_json = json.dumps(affinity_annotations)
    
    try:
        # Enregistrer l'indicateur AUA et les affinités dans la base de données
        insert_indicator(user_id, "CAT2-Engagement", "Contenu Populaire", "AUA", AUA)
        insert_indicator(user_id, "CAT2-Engagement", "Contenu Populaire", "AUA_affinities", affinity_annotations_json)
    except SQLAlchemyError as e:
        print(f"Error inserting indicators: {e}")

    return AUA

# --------------------------------------------
# Stratégie 11 : Nouveautés à Découvrir
# --------------------------------------------

def calculate_aun(user_id, new_annotations, since_date=None):
    """
    Calcule l'Affinité Utilisateur avec les Nouveautés (AUN) pour un utilisateur donné.
    
    :param user_id: ID de l'utilisateur.
    :param new_annotations: DataFrame des nouvelles annotations depuis `since_date`.
    :param since_date: Date depuis laquelle les annotations sont considérées comme nouvelles.
    :return: Valeur AUN et liste d'IDs des nouvelles annotations avec affinité.
    """
    print('Computing AUN for user: ', user_id)
    # Construire le profil d'intérêt de l'utilisateur
    profile = build_user_profile(user_id, since_date)
    if not profile:
        return None
    
    total_affinity_score = 0
    affinity_annotations = []

    for _, annotation_row in new_annotations.iterrows():
        # Convertir la ligne de DataFrame en dictionnaire
        annotation = annotation_row.to_dict()
        
        # Calcul des affinités avec la fonction généralisée
        affinity_score = calculate_annotation_affinity(profile, annotation)
        total_affinity_score += affinity_score
        
        if affinity_score > 0:
            affinity_annotations.append(annotation['post_id'])

    # Calculer l'affinité moyenne
    AUN = total_affinity_score / len(new_annotations) if new_annotations.shape[0] > 0 else 0
    
    # Convertir la liste des IDs en chaîne JSON pour le stockage
    affinity_annotations_json = json.dumps(affinity_annotations)
    
    try:
        # Enregistrer l'indicateur AUN et les affinités dans la base de données
        insert_indicator(user_id, "CAT2-Engagement", "Nouveautés", "AUN", AUN)
        insert_indicator(user_id, "CAT2-Engagement", "Nouveautés", "AUN_affinities", affinity_annotations_json)
    except SQLAlchemyError as e:
        print(f"Error inserting indicators: {e}")

    return AUN


