from datetime import datetime, timedelta
from modules.db_operations import fetch_user_annotations, fetch_user_tours, get_user_registration_date, insert_indicator
from modules.es_operations import fetch_actions_count, fetch_discovered_locations, fetch_actions, fetch_initial_shares, fetch_user_viewed_annotations, get_elasticsearch_client

# --------------------------------------------
# Catégorie 1 : Adoption et Intégration
# --------------------------------------------

# --------------------------------------------
# Stratégie 1 : Soutien Initial (Démarrage à Froid)
# --------------------------------------------

def calculate_nia(user_id):
    """
    Calcul du NIA (Number of Initial Actions) pour un utilisateur donné.
    """
    registration_date_str = get_user_registration_date(user_id)
    
    if registration_date_str:
        # Convertir la date d'inscription en objet datetime
        try:
            # Assumer que registration_date_str est au format 'YYYY-MM-DDTHH:MM:SS.sssZ'
            registration_date = datetime.strptime(registration_date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            try:
                # Essayer le format 'YYYY-MM-DD' si le premier essai échoue
                registration_date = datetime.strptime(registration_date_str, "%Y-%m-%d")
            except ValueError as e:
                print(f"Erreur de format de date pour l'utilisateur {user_id}: {e}")
                return
        
        # Obtenir la date actuelle
        today = datetime.utcnow()
        
        # Vérifier si l'utilisateur s'est enregistré dans les 10 derniers jours
        if today - registration_date <= timedelta(days=15000):
            # Convertir la date d'inscription en format 'YYYY-MM-DD' pour fetch_actions
            registration_date_str_for_query = registration_date.strftime("%Y-%m-%d")
            
            # Fetch actions performed within the first 70 days of registration
            actions = fetch_actions(user_id, registration_date_str_for_query, 1500)
            
            # Compter le nombre d'actions
            nia = len(actions)
            
            # Insérer l'indicateur NIA
            insert_indicator(user_id, "CAT1-Adoption_Integration", "Number of Initial Actions", "NIA", nia)
        else:
            print(f"L'utilisateur {user_id} ne s'est pas enregistré dans les 10 derniers jours.")


def calculate_nis(user_id):
    """
    Calcul du NIS (Number of Initial Shares) pour un utilisateur donné.
    """
    registration_date_str = get_user_registration_date(user_id)
    
    if registration_date_str:
        try:
            # Assumer que registration_date_str est au format 'YYYY-MM-DDTHH:MM:SS.sssZ'
            registration_date = datetime.strptime(registration_date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            try:
                # Essayer le format 'YYYY-MM-DD' si le premier essai échoue
                registration_date = datetime.strptime(registration_date_str, "%Y-%m-%d")
            except ValueError as e:
                print(f"Erreur de format de date pour l'utilisateur {user_id}: {e}")
                return
        
        # Obtenir la date actuelle
        today = datetime.utcnow()
        
        # Vérifier si l'utilisateur s'est enregistré dans les 10 derniers jours
        if today - registration_date <= timedelta(days=10):
            # Convertir la date d'inscription en format 'YYYY-MM-DD' pour fetch_initial_shares
            registration_date_str_for_query = registration_date.strftime("%Y-%m-%d")
            
            # Fetch shares made within the first 7 days of registration
            shares = fetch_initial_shares(user_id, registration_date_str_for_query, 1500)
            print(f'Registration date of user: {user_id} is: {registration_date} and number of shares is: {len(shares)}')
            
            # Compter le nombre de partages
            nis = len(shares)
            
            # Insérer l'indicateur NIS
            insert_indicator(user_id, "CAT1-Adoption_Integration", "Number of Initial Shares", "NIS", nis)
        else:
            print(f"L'utilisateur {user_id} ne s'est pas enregistré dans les 10 derniers jours.")


        
# --------------------------------------------
# Utilisation des Fonctionnalités ------------
# --------------------------------------------

def calculate_suf(user_id):
    """
    Calcul du Score d'Utilisation des Fonctionnalités (SUF) pour chaque catégorie : Création, Interaction, Exploration.
    """

    # Définir les poids des interactions
    weights = {
        'creation': 3,
        'interaction': 2,
        'exploration': 1
    }

    # Mappage des fonctionnalités par catégorie
    func_categories = {
        'creation': ['m:addPost', 'm:editPost', 'm:startTour','m:editTour', 'm:addMessage'],
        'interaction': ['m:movePost', 'm:editTour', 'm:moveTour', 'm:addMessage', 'm:deletePost', 'm:addEmoji', "m:addFavorite"],
        'exploration': ['m:addressSearch', 'm:Filter', 'm:deleteMessage', 'm:openPost', "m:addressSearch" , "m:selectSearchResult", "m:readNotification", "m:doNotificationAction"]
    }

    # Obtenir toutes les actions de l'utilisateur à partir de Elasticsearch
    es = get_elasticsearch_client()
    if es is None:
        print("Elasticsearch client is not disponible.")
        return

    # Fonction pour récupérer les actions par catégorie en utilisant l'API de défilement
    def get_actions_by_category(category, funcs):
        query = {
            "query": {
                "bool": {
                    "must": [
                        {"term": {"userId": user_id}}
                    ],
                    "filter": {
                        "terms": {
                            "type": funcs
                        }
                    }
                }
            },
            "size": 10000
        }
        actions = []
        response = es.search(index="mobiles", body=query, scroll='2m')
        scroll_id = response['_scroll_id']
        actions.extend(response['hits']['hits'])

        while len(response['hits']['hits']):
            response = es.scroll(scroll_id=scroll_id, scroll='2m')
            scroll_id = response['_scroll_id']
            actions.extend(response['hits']['hits'])

        return actions

    # Initialiser les scores pour chaque catégorie
    usage_scores = {
        'creation': 0,
        'interaction': 0,
        'exploration': 0
    }

    # Calculer le nombre d'actions par catégorie
    count_actions = {
        'creation': 0,
        'interaction': 0,
        'exploration': 0
    }

    # Récupérer et traiter les actions pour chaque catégorie
    for category, funcs in func_categories.items():
        actions = get_actions_by_category(category, funcs)
        for action in actions:
            action_type = action['_source']['type']
            # print(f"Categorizing {action_type} as {category}")  # Impression pour vérifier la catégorisation
            usage_scores[category] += 1  # Score sans pondération
            count_actions[category] += 1  # Compteur d'actions

    # Calculer le total des scores sans pondération
    total_score_unweighted = sum(usage_scores.values())

    # Calculer le SUF pour chaque catégorie en normalisant les scores sans pondération pour que la somme soit égale à 1
    sufs_unweighted = {}
    for category, score in usage_scores.items():
        if total_score_unweighted > 0:
            sufs_unweighted[category] = score / total_score_unweighted
        else:
            sufs_unweighted[category] = 0

    # Calculer le score SUF global de manière pondérée
    total_weighted_score = sum(weights[category] * sufs_unweighted[category] for category in weights)
    suf_global = total_weighted_score / sum(weights.values()) if sum(weights.values()) > 0 else 0

    # Insérer les indicateurs SUF dans la base de données
    for category, suf in sufs_unweighted.items():
        insert_indicator(user_id, "CAT1-Adoption_Integration", f"Score Utilisation Fonction - {category.capitalize()}", f"SUF_{category.capitalize()}", suf)
    insert_indicator(user_id, "CAT1-Adoption_Integration", "Score Utilisation Fonction - Global", "SUF_Global", suf_global)

    print(f"SUF scores: {sufs_unweighted}, SUF Global: {suf_global}")  # Impression pour vérifier les scores SUF
    return sufs_unweighted, suf_global


# --------------------------------------------
# Stratégie 2 : Fonctionnalités clés – Création et partage
# --------------------------------------------
def calculate_nca(user_id):
    """
    Calcul du NCA (Number of Created Annotations) pour un utilisateur donné.
    """
    annotations = fetch_user_annotations(user_id)
    # Compter le nombre d'annotations créées
    nca = len(annotations)
    insert_indicator(user_id, "CAT1-Adoption_Integration", "Number of Created Annotations", "NCA", nca)
    

def calculate_ncr(user_id):
    """
    Calcul du NCR (Number of Created Routes) pour un utilisateur donné.
    """
    routes = fetch_user_tours(user_id)
    ncr = len(routes)
    # Insérer l'indicateur NCR
    insert_indicator(user_id, "CAT1-Adoption_Integration", "Number of Created Routes", "NCR", ncr)



# -----------------------------------------------------------
# Stratégie 3 : Fonctionnalités clés – Exploration de Contenu
# -----------------------------------------------------------
def calculate_ndl(user_id):
    """
    Calcul du NDL (Number of Discovered Locations) pour un utilisateur donné.
    """
    locations = fetch_discovered_locations(user_id)
    ndl = len(locations)
    insert_indicator(user_id, "CAT1-Adoption_Integration", "Number of Discovered Locations", "NDL", ndl)
    

def calculate_ncoa(user_id):
    """
    Calcul du NCoA (Number of Consulted Annotations) pour un utilisateur donné.
    """

    # Obtenir la date d'enregistrement de l'utilisateur
    consulted_annotations = fetch_user_viewed_annotations(user_id)
    ncoa = len(consulted_annotations)
    insert_indicator(user_id, "CAT1-Adoption_Integration", "Number of Consulted Annotations", "NCoA", ncoa)
    

# -----------------------------------------------------------
# Stratégie 4 : Fonctionnalités clés – Interaction
# -----------------------------------------------------------
def calculate_nc(user_id):
    """
    Calcul du Nombre de Commentaires (NC) pour un utilisateur donné.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les actions sont considérées.
    :return: NC (Nombre de Commentaires).
    """
    # Récupérer le nombre de commentaires depuis la date spécifiée
    C = fetch_actions_count(user_id, 'm:addMessage')

    # Insérer le nombre de commentaires dans la base de données
    insert_indicator(
        user_id,
        "CAT1-Adoption_Integration",
        "Number of Comments",
        "NC",  # Indicateur Nombre de Commentaires
        value=C
    )

def calculate_nr(user_id):
    """
    Calcul du Nombre de Réactions (NR) pour un utilisateur donné.

    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les actions sont considérées.
    :return: NR (Nombre de Réactions).
    """
    # Récupérer le nombre de réactions depuis la date spécifiée
    R = fetch_actions_count(user_id, 'm:addEmoji')

    # Insérer le nombre de réactions dans la base de données
    insert_indicator(
        user_id,
        "CAT1-Adoption_Integration",
        "Number of Reactions",
        "NR",  # Indicateur Nombre de Réactions
        value=R
    )

