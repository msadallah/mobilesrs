from collections import defaultdict
import json
from modules.db_operations import fetch_annotations_by_id, fetch_new_annotations, fetch_tours, fetch_user_tours, fetch_user_annotations, fetch_indicators, get_indicator_value, insert_recommendation
from modules.es_operations import fetch_annotation_popularity, fetch_user_ids, fetch_user_locations, fetch_user_logs, fetch_user_viewed_annotations
from modules.db_operations import fetch_annotations,  insert_recommendation
import pandas as pd

from modules.metrics.engagement_reengagement_m import THRESHOLD_OA, calculate_min_distance_to_tour
from modules.suggester import Suggester

# Définir les seuils pour les recommandations
ENGAGED_THRESHOLD = 0.7 
DISENGAGED_THRESHOLD = 0.3
THRESHOLD_PLA = 5  # Seuil de distance moyenne (en kilomètres)
THRESHOLD_TEL = 0.5  # Seuil du taux d'exploration (50% ou autre valeur appropriée)
AIR_THRESHOLD = 5 
AUA_THRESHOLD_VALUE = 0.5
AUN_THRESHOLD_VALUE = 0.5
SEA_THRESHOLD = 0
SEP_THRESHOLD = 0

THRESHOLD_PPLA = 0.5
THRESHOLD_SPP = 0.5

## Stratégie 1 : Maintien de l'Engagement Basé sur le Profil d'Annotation
def generate_annotation_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations d'annotations pour un utilisateur donné en fonction de son profil 
    d'activité (création ou consultation d'annotations).
    
    :param user_id: ID de l'utilisateur.
    :param indicators: DataFrame contenant les indicateurs de l'utilisateur.
    """

    # Récupérer les indicateurs pour la proportion des activités de création et de consultation d'annotations
    pca = get_indicator_value(indicators, 'WPCA')  # Création d'Annotations
    pcoa = get_indicator_value(indicators, 'WPCOA')  # Consultation d'Annotations
    
    # Vérifier si les indicateurs sont présents et valides
    print('pca: ', pca)
    if pca is None or pcoa is None :
        print(f"Indicateurs manquants pour l'utilisateur {user_id}")
        return

    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')
    # Axée création : PCA > 0.7
    if pca > ENGAGED_THRESHOLD:
        insert_recommendation(
            user_id,
            "Engagement",
            "AnnotationProfileEngage",
            "Continue d'annoter !",
            "Tu es très actif dans la création d'annotations, bravo ! Pour t'aider à diversifier tes contributions, voici une annotation intéressante qui pourrait t'inspirer. Continue à enrichir la communauté !",
            suggestion,
            suggestion_type
        )

    # Axée consultation : PCOA > 0.7
    elif pcoa > ENGAGED_THRESHOLD:
        insert_recommendation(
            user_id,
            "Engagement",
            "AnnotationProfileEngage",
            "Découvre et participe !",
            "Tu consultes régulièrement des annotations, c'est super pour découvrir de nouveaux contenus ! Pourquoi ne pas aller plus loin ? Jette un œil à cette annotation pour t'inspirer et commence à annoter toi-même. Partage tes idées avec la communauté !",
            suggestion,
            suggestion_type
        )

    # Comportement mixte : 0.3 < PCA < 0.7 et 0.3 < PCOA < 0.7
    elif DISENGAGED_THRESHOLD < pca < ENGAGED_THRESHOLD and DISENGAGED_THRESHOLD < pcoa < ENGAGED_THRESHOLD:
        insert_recommendation(
            user_id,
            "Engagement",
            "AnnotationProfileEngage",
            "Explore et crée !",
            "Tu es actif dans tes contributions et tes découvertes. Voici cette annotation qui pourrait t'inspirer, que ce soit pour créer ou explorer davantage.",
            suggestion,
            suggestion_type
        )

## Stratégie 2 : Maintien de l'Engagement Basé sur la Proximité des Lieux Annotés
def generate_location_based_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations basées sur la proximité des lieux annotés pour un utilisateur donné 
    en fonction des indicateurs de proximité et de similarité.
    
    :param user_id: ID de l'utilisateur.
    :param indicators: DataFrame contenant les indicateurs de l'utilisateur.
    """

    # Récupérer les indicateurs pour la proximité des lieux annotés et la similarité pondérée des profils
    ppla = get_indicator_value(indicators, 'PLA')  # Proximité des Lieux Annotés
    
    # Vérifier si les indicateurs sont présents et valides
    if ppla is None :
        print(f"Indicateurs manquants pour l'utilisateur {user_id}")
        return
    
    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    # Proximité des lieux annotés : PPLA < seuil (par exemple 2 km)
    if ppla < THRESHOLD_PPLA:  # THRESHOLD_PPLA est le seuil défini (par ex. 2 km)
        insert_recommendation(
            user_id,
            "Engagement",
            "AnnotationLocationEngage",
            "Des trésors près des lieux que tu fréquentes !",
            "Il y a des lieux intéressants tout près de tes explorations ! L'endroit décrit dans l'annotation suivante est juste à côté de tes lieux préférés. Explore-le et découvre une nouvelle perspective locale.",
            suggestion,
            suggestion_type
        )


# Stratégie 3 : suggestion de parcours basée profil de parcours
def generate_route_based_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations de parcours basées sur les habitudes de création et de consultation de parcours 
    pour un utilisateur donné, en fonction des indicateurs de création et de consultation de parcours.

    :param user_id: ID de l'utilisateur.
    :param indicators: DataFrame contenant les indicateurs de l'utilisateur.
    """

    # Récupérer les indicateurs pour la création et la consultation de parcours
    w_pcp = get_indicator_value(indicators, 'WPCP')  # Création de Parcours
    w_pcop = get_indicator_value(indicators, 'WPCOP')  # Consultation de Parcours
    print('w_pcp: ',w_pcp)
    print('w_pcop: ',w_pcop)

    # Vérifier si les indicateurs sont présents et valides
    if w_pcp is None or w_pcop is None:
        print(f"Indicateurs WPCP ou WPCoP manquants pour l'utilisateur {user_id}")
        return
    
    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    # Axée Création : w_PCP > 0.7
    if w_pcp > ENGAGED_THRESHOLD:
        insert_recommendation(
            user_id,
            "Engagement",
            "RouteSuggest",
            "Crée encore plus de parcours captivants !",
            "Continue de partager tes explorations en créant des parcours uniques. Pour t'inspirer, voici un parcours qui pourrait te donner de nouvelles idées.",
            suggestion,
            suggestion_type
        )

    # Axée Consultation : w_PCoP > 0.7
    elif w_pcop > ENGAGED_THRESHOLD:
        insert_recommendation(
            user_id,
            "Engagement",
            "RouteSuggest",
            "Lance-toi dans la création de tes propres parcours !",
            "Tu as déjà exploré pas mal de parcours, pourquoi ne pas créer le tien ? Pour t'aider à démarrer, découvre ce parcours inspirant.",
            suggestion,
            suggestion_type
        )

    # Mixte : 0.3 < w_PCP < 0.7 et 0.3 < w_PCoP < 0.7
    elif DISENGAGED_THRESHOLD < w_pcp < ENGAGED_THRESHOLD and DISENGAGED_THRESHOLD < w_pcop < ENGAGED_THRESHOLD:
        insert_recommendation(
            user_id,
            "Engagement",
            "RouteSuggest",
            "Combine tes talents d'exploration et de création !",
            "Profite de tes découvertes pour enrichir tes créations. Voici un parcours qui pourrait parfaitement te correspondre et t'inspirer.",
            suggestion,
            suggestion_type
        )

# Stratégie 4 : Réengagement à travers les Interactions Utilisateur
def generate_reengagement_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations de réengagement pour un utilisateur basé sur son interaction avec la plateforme.
    
    :param user_id: ID de l'utilisateur.
    :param indicators: DataFrame contenant les indicateurs d'engagement de l'utilisateur.
    """

    # Récupérer les indicateurs de désengagement (ID) et les scores d'engagement dans différentes fonctionnalités
    disengagement_index = get_indicator_value(indicators, 'ID')  # Indicateur de Désengagement
    suf_creation = get_indicator_value(indicators, 'SUF_Creation')  # Engagement dans la création
    suf_interaction = get_indicator_value(indicators, 'SUF_Interaction')  # Engagement dans les interactions (commentaires, réactions)
    suf_exploration = get_indicator_value(indicators, 'SUF_Exploration')  # Engagement dans l'exploration (annotations, parcours)

    # Définir le seuil de désengagement
   
    # Vérifier si les indicateurs sont présents et valides
    if disengagement_index is None or suf_creation is None or suf_interaction is None or suf_exploration is None:
        print(f"Indicateurs manquants pour l'utilisateur {user_id}")
        return

    recommendations = []

    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    # Si ID et SUF_Création < seuil défini
    if disengagement_index < DISENGAGED_THRESHOLD and suf_creation < DISENGAGED_THRESHOLD:
        recommendations.append({
            'strategy': 'UserCreationReengage',
            "titre": "Pourquoi ne pas explorer d’avantage?!",
            "contenu": "Tu as encore plein de potentiel à exploiter dans la création d’annotations et de parcours. Pourquoi ne pas essayer de créer une nouvelle annotation aujourd’hui ? Cela enrichira ton expérience et te permettra de partager ce que tu vis avec la communauté.",
            'suggestion': suggestion,
            'suggestion_type': suggestion_type
        })

    

    # Si ID et SUF_Exploration < seuil défini
    if disengagement_index < DISENGAGED_THRESHOLD and suf_exploration < DISENGAGED_THRESHOLD:
        recommendations.append({
            'strategy': 'UserExplorationReengage',
            "titre": "Découvres de nouvelles annotations et de nouveaux parcours",
            "contenu": "Sais-tu que d’autres utilisateurs créent régulièrement des annotations et des parcours qui pourraient t’intéresser ? Prends le temps d’explorer ce que les autres partagent et inspire-toi-en.",
            'suggestion': suggestion,
            'suggestion_type': suggestion_type
        })

    # Si ID et SUF_Interaction < seuil défini
    if disengagement_index < DISENGAGED_THRESHOLD and suf_interaction < DISENGAGED_THRESHOLD:
        recommendations.append({
            'strategy': 'UserInteractionReengage',
            "titre": "Participe à la conversation !",
            "contenu": "Tu as beaucoup à apporter aux discussions. Tes idées et tes retours sont précieux pour l’expérience des autres utilisateurs et pour la tienne. N’hésite pas à réagir au contenu des autres !",
            'suggestion': suggestion,
            'suggestion_type': suggestion_type
        })

    # Si plus d'un indicateur est en dessous du seuil, ajouter une recommandation générale
    if len(recommendations) > 1:
        rec = {
            'strategy': 'UserActivityReengage',
            "titre": "Maximise ton expérience !",
            "contenu": "Tu peux découvrir tout ce que notre application a à offrir en suivant notre tutoriel. Ce guide te présentera toutes les fonctionnalités et t’aidera à tirer le meilleur parti de cette application.",
            'suggestion': suggestion,
            'suggestion_type': suggestion_type
        }
        insert_recommendation(user_id, "CAT2-Engagement", rec["strategy"], rec["titre"], rec["contenu"], rec['suggestion'], rec['suggestion_type'])
    else:        
        # Insérer les recommandations dans la base de données
        for rec in recommendations:
            insert_recommendation(user_id, "CAT2-Engagement", rec["strategy"], rec["titre"], rec["contenu"], rec['suggestion'], rec['suggestion_type'])

# Stratégie 5 : Réengagement Basé sur l'Historique des Activités
def generate_history_based_reengagement_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations de réengagement basées sur l'historique d'activités de l'utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param activity_history: DataFrame contenant l'historique des activités de l'utilisateur.
    """

    # Récupérer les indicateurs SEA (Annotations) et SEP (Parcours)
    sea = get_indicator_value(indicators, 'SEA')  # Historique d'annotations (évolution)
    sep = get_indicator_value(indicators, 'SEP')  # Historique de parcours (évolution)

    # Vérifier si les indicateurs sont présents et valides
    if sea is None or sep is None:
        print(f"Indicateurs manquants pour l'utilisateur {user_id}")
        return

    recommendations = []
    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    # Si SEA (annotations) <= 0, proposer une réactivation centrée sur les annotations
    if sea <= SEA_THRESHOLD:
        recommendations.append({
            "strategy": "AnnotationHistoryReengage",
            "titre": "Reviens partager de nouvelles expériences",
            "contenu": "Reviens et partage tes expériences en annotant ! Jette un œil à l'annotation suivante, un exemple !=> un exemple que tu vas probablement aimer.",
            'suggestion': suggestion,
            'suggestion_type':suggestion_type
        })

    # Si SEP (parcours) <= 0, proposer une réactivation centrée sur les parcours
    if sep <= SEP_THRESHOLD:
        recommendations.append({
            "strategy": "TourHistoryReengage",
            "titre": "Reviens partager tes parcours !",
            "contenu": "Relance-toi dans la création de tes trajets ! Découvre le parcours suivant, un exemple qui devrait t’intéresser.",
            'suggestion': suggestion,
            'suggestion_type':suggestion_type
        })

    # Insérer les recommandations dans la base de données
    for rec in recommendations:
        insert_recommendation(user_id, "CAT2-Engagement", rec["strategy"], rec["titre"], rec["contenu"], rec['suggestion'], rec['suggestion_type'])


# Stratégie 6 : Réengagement à Travers le Contenu de la Communauté
def generate_community_content_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations de réengagement basées sur l'interaction de l'utilisateur avec le contenu de la communauté.
    
    :param user_id: ID de l'utilisateur.
    :param indicators: DataFrame contenant les indicateurs d'engagement de l'utilisateur.
    """

    # Récupérer les scores d'engagement de l'utilisateur
    sca = get_indicator_value(indicators, 'SCA')  # Score de Consultation des Annotations
    scp = get_indicator_value(indicators, 'SCP')  # Score de Consultation des Parcours
    sis = get_indicator_value(indicators, 'SIS')  # Score d'Interaction Social

    # Définir les seuils pour l'engagement (par exemple, ici inférieur à 0.3 indique un besoin de réengagement)
    

    recommendations = []
    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    print('sca: ',sca, '- scp: ',scp,' - sis: ', sis)

    # Si SCA < seuil défini
    if sca is not None and sca < DISENGAGED_THRESHOLD:
        recommendations.append({
            "titre": "Explore les annotations les plus appréciées !",
            "contenu": "Découvre l'annotation suivante, qui a généré de nombreuses réactions. Elle pourrait enrichir ton expérience et correspondre à tes intérêts !",
            'suggestion': suggestion,
            'suggestion_type':suggestion_type
        })

    # Si SCP < seuil défini
    if scp is not None and scp < DISENGAGED_THRESHOLD:
        recommendations.append({
            "titre": "Parcours populaires à ne pas manquer !",
            "contenu": "Jette un œil au parcours suivant, qui a suscité beaucoup de réactions. Il pourrait t'offrir des perspectives nouvelles et inspirer tes prochaines explorations !",
            'suggestion': suggestion,
            'suggestion_type':suggestion_type
        })

    # Si SIS < seuil défini
    if sis is not None and sis < DISENGAGED_THRESHOLD:
        recommendations.append({
            "titre": "Participe à la conversation !",
            "contenu": "Nous avons remarqué que tu n'as pas interagi récemment. Pourquoi ne pas partager tes avis? Engage-toi avec l'annotation suivante, qui a déjà suscité beaucoup de réactions. Ton avis compte !",
            'suggestion': suggestion,
            'suggestion_type':suggestion_type
        })

    # Insérer les recommandations dans la base de données
    for rec in recommendations:
        insert_recommendation(user_id, "CAT2-Engagement", "CommunityReengage", rec["titre"], rec["contenu"], rec['suggestion'], rec['suggestion_type'])




def generate_all_engagement_recommendations():
    """
    Génère des recommandations d'engagement pour tous les utilisateurs.
    """
    user_ids = fetch_user_ids()
    
    # Identifier des lieux intéressants non annotés
    # non_annotated_places = identify_interesting_places()

    # Récupérer les annotations populaires
    popularity_data = fetch_annotation_popularity()
    new_annotations = fetch_new_annotations()
    
    
    for user in user_ids:
        # Récupérer les indicateurs pour l'utilisateur
        indicators = fetch_indicators(user)

        
        # Générer des recommandations pour chaque utilisateur
        if indicators is not None and len(indicators) > 0:
            suggester = Suggester(user_id=user)
            generate_annotation_recommendations(user, indicators, suggester)
            generate_location_based_recommendations(user, indicators, suggester)
            generate_route_based_recommendations(user, indicators, suggester)
            generate_reengagement_recommendations(user, indicators, suggester)
            generate_history_based_reengagement_recommendations(user, indicators, suggester)
            generate_community_content_recommendations(user, indicators, suggester)

            # OLD VERSION
            # generate_engagement_recommendations(user, indicators)
            # suggest_opportune_annotation_locations(user)
            # generate_contextual_tour_recommendations(user,user_location)
            # generate_tour_creation_recommendations(user)
            # generate_reactivation_recommendation(user)
            # generate_recurrent_interest_recommendations(user)
            # generate_popular_content_recommendations(user, popularity_data)
            # generate_new_content_recommendations(user, new_annotations)
        else:
            print(f'No indicators found for user: {user}')













###################  OLD VERSION
def generate_engagement_recommendations(user_id, indicators):
    """
    Génère des recommandations pour améliorer l'exploration contextuelle des lieux annotés 
    en fonction des indicateurs PLA et TEL pour un utilisateur donné.
    """
    pla =  get_indicator_value(indicators, 'PLA')
    tel =  get_indicator_value(indicators, 'TEL') 

    # Recommandations basées sur le PLA
    if pla is not None and pla > THRESHOLD_PLA:
        insert_recommendation(user_id, 
                              "Engagement", 
                              "Exploration Contextuelle des Lieux Annotés", 
                              "Découvrez des lieux uniques proches de vous, annotés par d'autres utilisateurs. Explorez ces lieux et ajoutez vos propres annotations !")

    # Recommandations basées sur le TEL
    if tel is not None and tel < THRESHOLD_TEL:
        insert_recommendation(user_id, 
                              "Engagement", 
                              "Exploration Contextuelle des Lieux Annotés", 
                              "Nous avons remarqué que vous n'avez pas encore exploré de nombreux lieux annotés par d'autres. Consultez ces lieux recommandés pour enrichir votre expérience !")


def identify_interesting_places():
    """
    Identifie des lieux intéressants non annotés en utilisant les données comportementales des utilisateurs.
    """
    # Récupérer les coordonnées des annotations existantes
    annotations = fetch_annotations(include_fields=['coords'], field_filters_to_exclude={'user_id': []})
    annotated_coords = set()
    if not annotations.empty:
        for _, annotation in annotations.iterrows():
            coords = annotation['coords']
            coords_dict = json.loads(coords)
            annotated_coords.add((coords_dict['lat'], coords_dict['lon']))

    # Récupérer les logs des utilisateurs pour identifier les lieux fréquemment visités
    user_logs = fetch_user_logs()  # Ajustez cette fonction pour récupérer les logs des utilisateurs
    place_visits = pd.DataFrame(user_logs)

    # Identifier les lieux non annotés
    non_annotated_places = place_visits[~place_visits.apply(lambda row: (row['lat'], row['lon']) in annotated_coords, axis=1)]
    
    return non_annotated_places

def suggest_opportune_annotation_locations(user_id, start_date=None, days=None):
    """
    Suggère des lieux opportuns pour l'annotation contextuelle en fonction des lieux intéressants non encore annotés et des indicateurs OA et TCA.
    """
    indicators = fetch_indicators(user_id)
    non_annotated_places =  get_indicator_value(indicators, "non_annotated_places")
    oa =  get_indicator_value(indicators, "OA")
    tca =  get_indicator_value(indicators, "TCA") 

    if oa is None:
        print(f"Indicateurs OA manquants pour l'utilisateur {user_id}. Recommandation non générée.")
        return

    # if oa > THRESHOLD_OA and tca < THRESHOLD_TCA:
    # print('oa: ',oa)
    # print('THRESHOLD_OA: ',THRESHOLD_OA)
    if oa > THRESHOLD_OA:
        for place in non_annotated_places:
            lat, lon = place['lat'], place['lon']
            recommendation = (f"Partagez votre découverte en annotant cet endroit spécial "
                              f"({lat}, {lon}) que vous avez récemment visité ! "
                              "Contribuez à enrichir notre communauté avec vos annotations uniques.")
            insert_recommendation(user_id, "Engagement", 
                                  "Annotation Contextuelle Opportune", recommendation)
    else:
        print(f"Pas de recommandations générées pour l'utilisateur {user_id}. OA={oa}, TCA={tca}")


def generate_contextual_tour_recommendations(user_id, user_location, pp_threshold=5, tep_threshold=0.1):
    """
    Génère des recommandations de parcours contextuelles pour un utilisateur donné en fonction des indicateurs calculés.
    
    :param user_id: ID de l'utilisateur
    :param user_location: Dictionnaire contenant 'lat' et 'lon' pour la localisation de l'utilisateur.
    :param pp_threshold: Seuil pour la Proximité des Parcours (PP) pour déclencher une recommandation.
    :param tep_threshold: Seuil pour le Taux d'Exploration de Parcours (TEP) pour déclencher une recommandation.
    :return: None
    """
    # Récupération des indicateurs depuis la base de données
    indicators = fetch_indicators(user_id)

    
    pp =  get_indicator_value(indicators, "PP")
    tep =  get_indicator_value(indicators, "TEP")
    
    # Vérification que les indicateurs sont présents et valides
    # if pp is None or tep is None:
    if pp is None:
        print(f"Indicateurs manquants pour l'utilisateur {user_id}. Recommandation non générée.")
        return
    
    # Vérification des conditions pour déclencher la recommandation
    # if pp < pp_threshold and tep >= tep_threshold:
    if pp < pp_threshold :
        # Sélection des parcours proches pour la recommandation
        tours = fetch_tours()
        recommended_tours = []

        for _, tour in tours.iterrows():
            coordinates = json.loads(tour['body'])
            tour_coordinates = [(coordinates[i], coordinates[i+1]) for i in range(0, len(coordinates), 2)]
            dist = calculate_min_distance_to_tour(user_location, tour_coordinates)

            
            if dist is not None and dist < pp_threshold:
                recommended_tours.append(tour['id'])  # Suppose que chaque tour a un identifiant unique
        
        # Création et stockage de la recommandation dans la base de données
        if recommended_tours:
            recommendation_text = f"Découvrez ces parcours à proximité : {', '.join(map(str, recommended_tours))}. Explorez et partagez votre expérience !"
            insert_recommendation(user_id, "Engagement", recommendation_text, recommended_tours)
            print(f"Recommandation générée pour l'utilisateur {user_id}.")
        else:
            print(f"Aucun parcours proche trouvé pour l'utilisateur {user_id}.")
    else:
        print(f"Conditions non remplies pour générer une recommandation pour l'utilisateur {user_id}.")

def generate_tour_creation_recommendations(user_id):
    """
    Génère des recommandations pour la création de parcours basées sur les opportunités identifiées et le taux de création de parcours.

    :param user_id: ID de l'utilisateur
    :return: Recommandations à envoyer à l'utilisateur
    """
    # Calcul des indicateurs pour l'utilisateur
    indicators = fetch_indicators(user_id)

    # Récupérer les indicateurs de la base de données
    ocp =  get_indicator_value(indicators, "OCP")
    tcp =  get_indicator_value(indicators, "TCP")
    
    if ocp is None:
        print(f"Indicateurs OCP manquants pour l'utilisateur {user_id}. Recommandation non générée.")
        return

    # Définir les seuils
    ocp_threshold = 10  # Ajuster selon les besoins
    tcp_threshold = 0.1  # Ajuster selon les besoins

    # Générer des recommandations si les seuils sont dépassés
    # if ocp > ocp_threshold and tcp < tcp_threshold:
    if ocp > ocp_threshold:
        recommendation = (
            "Créez un nouveau parcours dans cet endroit encore inexploré mais potentiellement très attractif pour la communauté ! "
            "Partagez votre itinéraire unique et inspirez les autres utilisateurs avec votre découverte."
        )
        insert_recommendation(user_id, recommendation)
        return recommendation
    else:
        return "Aucune recommandation à générer pour le moment."


def generate_reactivation_recommendation(user_id, id_threshold=10, tru_threshold=0.1):
    """
    Crée une recommandation personnalisée pour réengager un utilisateur désengagé et l'enregistre.
    
    :param user_id: ID de l'utilisateur
    :param id_threshold: Seuil de désengagement pour activer la recommandation
    :param tru_threshold: Seuil de performance pour le taux de réactivation
    :return: Message de recommandation
    """
    indicators = fetch_indicators(user_id)
    ID =  get_indicator_value(indicators, "ID")
    TRU =  get_indicator_value(indicators, "TRU")

    if ID is None:
        print(f"Indicateurs manquants pour l'utilisateur {user_id}. Recommandation non générée.")
        return
    
    # Déterminer si la stratégie doit être activée
    # if ID < id_threshold and TRU > tru_threshold:
    if ID < id_threshold:
        recommendation_message = "Nous avons remarqué que vous n'avez pas ajouté de nouvelles annotations récemment. Revenez explorer de nouveaux lieux et ajouter des annotations !  Découvrez ce qui a changé depuis votre dernière visite. "
        category = "Engagement"
        strategy = "Réactivation"
        insert_recommendation(user_id, category, strategy, recommendation_message)
    
    return recommendation_message


def generate_recurrent_interest_recommendations(user_id):
    """
    Génère des recommandations ciblées basées sur les intérêts récurrents de l'utilisateur 
    seulement si l'indicateur AIR dépasse un seuil prédéfini.
    
    :param user_id: ID de l'utilisateur.
    :param air: Valeur de l'indicateur AIR.
    :return: Liste des recommandations générées.
    """
    indicators = fetch_indicators(user_id)
    air =  get_indicator_value(indicators, "AIR")
    recommendations = []

    if air is None:
        print(f"Indicateurs manquants pour l'utilisateur {user_id}. Recommandation non générée.")
        return
    
    if air <= AIR_THRESHOLD:
        return recommendations  # Ne pas générer de recommandations si AIR ne dépasse pas le seuil
    
    # Récupérer les annotations et parcours de l'utilisateur
    annotations = fetch_user_annotations(user_id)
    tours = fetch_user_tours(user_id)
    
    # Compter les occurrences de chaque type d'annotation
    annotation_counts = defaultdict(lambda: {'frequency': 0, 'place_type': defaultdict(int)})
    for _, row in annotations.iterrows():
        timing = row.get('timing', 'unknown')
        place_type = row.get('placeType', 'unknown')
        annotation_counts[timing]['frequency'] += 1
        annotation_counts[timing]['place_type'][place_type] += 1
    
    # Compter les occurrences de chaque type de parcours
    tour_counts = defaultdict(lambda: {'count': 0, 'length': defaultdict(int)})
    for _, row in tours.iterrows():
        tour_type = row.get('type', 'unknown')
        tour_length = len(json.loads(row.get('body', '[]')))  # Nombre de points dans le parcours
        tour_counts[tour_type]['count'] += 1
        tour_counts[tour_type]['length'][tour_length] += 1
    
    # Générer des recommandations basées sur les intérêts récurrents
    for timing, details in annotation_counts.items():
        if details['frequency'] > 5:  # Seuil arbitraire pour l'exemple
            for place_type, count in details['place_type'].items():
                if count > 3:  # Seuil arbitraire pour l'exemple
                    recommendation = (
                        f"Découvrez plus d'annotations {timing} dans la catégorie '{place_type}'. "
                        f"Explorez des lieux intéressants similaires à ceux que vous avez souvent consultés."
                    )
                    recommendations.append(recommendation)
                    insert_recommendation(user_id, "Engagement","Intérêts Récurrents", recommendation)
    
    for tour_type, details in tour_counts.items():
        if details['count'] > 3:  # Seuil arbitraire pour l'exemple
            for length, count in details['length'].items():
                if count > 2:  # Seuil arbitraire pour l'exemple
                    recommendation = (
                        f"Explorez davantage de parcours de type '{tour_type}' avec {length} points. "
                        f"Découvrez de nouveaux tracés similaires à ceux que vous avez souvent consultés."
                    )
                    recommendations.append(recommendation)
                    insert_recommendation(user_id, "Engagement", "Intérêts Récurrents", recommendation)
    
    return recommendations

def generate_filter_based_recommendations(user_id, threshold=5):
    """
    Génère des recommandations basées sur les filtres préférés de l'utilisateur.
    
    :param user_id: ID de l'utilisateur.
    :param threshold: Seuil pour activer la génération des recommandations.
    :return: Liste de recommandations sous forme de dictionnaires.
    """
    # Calculer PF et récupérer les motifs
    indicators = fetch_indicators(user_id)
    pf_value =  get_indicator_value(indicators, "PF")
    motifs =  get_indicator_value(indicators, "PF_motifs")


    # Ne pas générer de recommandations si PF est inférieur au seuil
    if pf_value < threshold:
        return []

    # Générer des recommandations basées sur les motifs identifiés
    recommendations = []

    if motifs['institutions']:
        for institution in motifs['institutions']:
            recommendations.append({
                "type": "institution",
                "value": institution,
                "message": f"Découvrez plus d'annotations liées à l'institution '{institution}'."
            })

    if motifs['nationalities']:
        for nationality in motifs['nationalities']:
            recommendations.append({
                "type": "nationality",
                "value": nationality,
                "message": f"Explorez des annotations et parcours en relation avec la nationalité '{nationality}'."
            })

    if motifs['icons']:
        for icon in motifs['icons']:
            recommendations.append({
                "type": "icon",
                "value": icon,
                "message": f"Découvrez plus de contenus associés à l'icône '{icon}'."
            })

    if motifs['emoticons']:
        for emoticon in motifs['emoticons']:
            recommendations.append({
                "type": "emoticon",
                "value": emoticon,
                "message": f"Explorez des annotations qui expriment '{emoticon}'."
            })

    if motifs['tags']:
        for tag in motifs['tags']:
            recommendations.append({
                "type": "tag",
                "value": tag,
                "message": f"Découvrez des annotations taguées avec '{tag}'."
            })

    if motifs['date_ranges']:
        for date_range in motifs['date_ranges']:
            recommendations.append({
                "type": "date_range",
                "value": date_range,
                "message": f"Explorez des annotations créées entre {date_range}."
            })

    # Enregistrer les recommandations
    for reco in recommendations:
        insert_recommendation(user_id, "Engagement", reco['type'], reco['message'])

    return recommendations

def generate_popular_content_recommendations(user_id, popularity_data):
    """
    Génère des recommandations d'annotations populaires basées sur les affinités utilisateur enregistrées.

    :param user_id: ID de l'utilisateur.
    :param popularity_data: Liste des annotations populaires avec leurs popularités.
    :return: Texte de recommandation pour les annotations populaires.
    """
    # Récupérer les indicateurs d'affinité
    indicators = fetch_indicators(user_id)
    aua = get_indicator_value(indicators, "AUA")

    aua_affinities = get_indicator_value(indicators, "AUA_affinities")

    
    # Vérifier le seuil d'affinité utilisateur
    if aua is None or aua_affinities is None or aua <= AUA_THRESHOLD_VALUE: 
        print("Affinité utilisateur insuffisante pour les recommandations de contenus populaires.")
        return "Nous n'avons pas trouvé de contenus populaires correspondant à vos préférences pour le moment."

    # Assurer que aua_affinities est une liste d'IDs, sinon la convertir
    if isinstance(aua_affinities, str):
        aua_affinities = eval(aua_affinities)  # Convertir la chaîne en liste

    # Récupérer les annotations vues par l'utilisateur
    viewed_annotations = fetch_user_viewed_annotations(user_id)

    # Filtrer et trier les annotations populaires non vues par l'utilisateur, par affinité décroissante
    filtered_annotations = [
        (annotation['annotation_id'], annotation['view_percentage'])
        for annotation in popularity_data
        if annotation['annotation_id'] in aua_affinities and annotation['annotation_id'] not in viewed_annotations  # Exclure les annotations déjà vues
    ]
    
    # Trier par popularité (ou d'autres critères de pertinence si applicable)
    filtered_annotations.sort(key=lambda x: x[1], reverse=True)
    
    # Sélectionner les annotations les plus pertinentes
    top_annotations = filtered_annotations[:2]  # Sélectionner les deux annotations les plus pertinentes
    
    if not top_annotations:
        return "Nous n'avons pas trouvé de contenus populaires correspondant à vos préférences pour le moment."

    # Construire un texte de recommandation
    recommended_text = "Découvrez des annotations populaires qui pourraient vous intéresser :"
    for annotation_id, _ in top_annotations:
        annotation_details = fetch_annotations_by_id(annotation_id).iloc[0]  # Assumer qu'il y a une ligne pour chaque ID
        summary = annotation_details.get('summary', 'Annotation intéressante')
        comment = annotation_details.get('comment', '...')[:100]
        recommended_text += f"\n- {summary} : {comment}..."

    # Enregistrer les recommandations
    insert_recommendation(user_id, "Engagement", "Annotations Populaires", [id for id, _ in top_annotations])

    return recommended_text


def generate_new_content_recommendations(user_id, new_annotations):
    """
    Génère des recommandations d'annotations nouvelles basées sur les affinités utilisateur enregistrées.

    :param user_id: ID de l'utilisateur.
    :param new_annotations: Liste des annotations nouvelles avec leurs détails.
    :return: Texte de recommandation pour les annotations nouvelles.
    """
    # Récupérer les indicateurs d'affinité
    indicators = fetch_indicators(user_id)
    aun = get_indicator_value(indicators, "AUN")
    aun_affinities = get_indicator_value(indicators, "AUN_affinities")

    # Vérifier le seuil d'affinité utilisateur pour les nouveautés
    if aun is None or aun_affinities is None or aun <= AUN_THRESHOLD_VALUE:  # Remplacer AUN_THRESHOLD_VALUE par une valeur appropriée
        print("Affinité utilisateur insuffisante pour les recommandations de nouveautés.")
        return "Nous n'avons pas trouvé de nouveautés correspondant à vos préférences pour le moment."

    # Assurer que aun_affinities est une liste d'IDs, sinon la convertir
    if isinstance(aun_affinities, str):
        aun_affinities = eval(aun_affinities)  # Convertir la chaîne en liste

    # Récupérer les annotations vues par l'utilisateur
    viewed_annotations = fetch_user_viewed_annotations(user_id)

    # Filtrer et trier les nouvelles annotations non vues par l'utilisateur, par date de création décroissante
    filtered_annotations = [
        (annotation['post_id'], annotation['created_at'])
        for _, annotation in new_annotations.iterrows()
        if annotation['post_id'] in aun_affinities and annotation['post_id'] not in viewed_annotations  # Exclure les annotations déjà vues
    ]
    
    # Trier par date de création (ou d'autres critères de pertinence si applicable)
    filtered_annotations.sort(key=lambda x: x[1], reverse=True)

    # Sélectionner les annotations les plus pertinentes
    top_annotations = filtered_annotations[:2]  # Sélectionner les deux annotations les plus pertinentes
    
    if not top_annotations:
        return "Nous n'avons pas trouvé de nouveautés correspondant à vos préférences pour le moment."

    # Construire un texte de recommandation
    recommended_text = "Découvrez les dernières annotations qui pourraient vous intéresser :"
    for annotation_id, _ in top_annotations:
        annotation_details = fetch_annotations_by_id(annotation_id).iloc[0]  # Assumer qu'il y a une ligne pour chaque ID
        summary = annotation_details.get('summary', 'Nouvelle annotation')
        comment = annotation_details.get('comment', '...')[:100]
        recommended_text += f"\n- {summary} : {comment}..."

    # Enregistrer les recommandations
    insert_recommendation(user_id, "Engagement", "Annotations Nouvelles", [id for id, _ in top_annotations])

    return recommended_text


