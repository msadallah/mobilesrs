from modules.db_operations import fetch_annotations_around_location, fetch_indicators, fetch_user_annotations, get_indicator_value, insert_recommendation
from modules.es_operations import fetch_user_ids, fetch_visited_locations
from modules.metrics.urban_discovery_m import calculate_harv_distance, get_pois_nearby, get_unexplored_zones
from modules.suggester import Suggester


DAP_THRESHOLD = 3 # Ce seuil signifie que s'il y a moins de 3 annotations en moyenne par POI, les lieux visités par l'utilisateur sont considérés comme peu pertinents pour des recommandations. En dessous de ce seuil, l'utilisateur a visité des POI avec peu de documentation, ce qui pourrait suggérer un manque d'intérêt général.


PRL_THRESHOLD = 0.8  # Si le PRL est inférieur à 0.8, cela signifie que les POI visités sont moins populaires que la moyenne. Cela peut indiquer que l'utilisateur visite des lieux qui ne sont pas couramment visités par d'autres, donc peut-être moins pertinents pour des recommandations basées sur les tendances.

# Justification : Le seuil DMAPP (Distance Mean Average Proximity Points) est fixé à 5000 mètres (5 km).
DMAPP_THRESHOLD = 5000  # en mètres

SPC_THRESHOLD = 2.0  # Surface en km² (ajustez selon les données)


DAU_THRESHOLD = 3  # nombre d'annotations (entier sans unité)


DC_THRESHOLD = 0.8  # proportion (sans unité)


DEFAULT_SINCE_DATE = "2024-01-01"

def get_selected_annotation_id(poi):
    """
    Retourne l'ID de l'annotation sélectionnée pour un POI donné.
    
    :param poi: Dictionnaire représentant un Point d'Intérêt (POI).
    :return: ID de l'annotation sélectionnée.
    """
    # Supposons que l'ID de l'annotation est inclus dans le POI comme une liste ou un dictionnaire
    if 'annotations' in poi and poi['annotations']:
        return poi['annotations'][0]['id']  # Retourne l'ID de la première annotation
    return None


def generate_poi_recommendations(user_id, suggester):
    """
    Génère une recommandation de POI basée sur les tendances de visite de l'utilisateur et l'enregistre.
    
    :param user_id: ID de l'utilisateur.
    :return: Texte de recommandation pour les POI.
    """
    # Récupérer les indicateurs d'affinité
    indicators = fetch_indicators(user_id)
    dap = get_indicator_value(indicators, "DAP")
    prl = get_indicator_value(indicators, "PRL")
    
    if dap is None or prl is None:
        print(f"DAP ou PRL non trouvé pour l'utilisateur {user_id}.")
        return "Nous n'avons pas trouvé de POI correspondant à vos préférences pour le moment."
    
    # Calculer le score combiné basé sur les deux indicateurs
    combined_score = (dap / DAP_THRESHOLD) * 0.5 + (prl / PRL_THRESHOLD) * 0.5
    
    # Si le score combiné est inférieur à 1, ne pas recommander
    if combined_score < 1:
        print("Affinité utilisateur insuffisante pour les recommandations de POI.")
        return "Nous n'avons pas trouvé de POI correspondant à vos préférences pour le moment."
    
    # Récupérer les lieux visités et les POI à proximité
    visited_locations = fetch_visited_locations(user_id)
    nearby_pois = get_pois_nearby(visited_locations)
    
    if not nearby_pois:
        return "Aucun POI pertinent trouvé autour des lieux visités."

    # Trier les POI par nombre d'annotations décroissant
    top_pois = sorted(nearby_pois, key=lambda x: x['annotations_count'], reverse=True)[:5]  # Sélectionner les 5 POI les plus pertinents
    
    # Sélectionner une annotation à partir du meilleur POI
    selected_annotation_id = None
    if top_pois:
        selected_annotation_id = top_pois[0]['annotation_ids'][0]  # Prendre le premier ID d'annotation

    

    # Enregistrer la recommandation
    insert_recommendation(
        user_id=user_id,
        category="CAT4-Urban",
        strategy="POIDiscover",
        title="Découvre les lieux incontournables autour de toi !",
        recommendation="Plonge dans l’exploration des endroits les plus prisés proches de ceux que tu as déjà annotés près de chez toi ! Ces lieux pourraient vraiment enrichir ton expérience. Pour commencer, regarde cette annotation populaire qui pourrait t'inspirer ",
        suggestion=selected_annotation_id,
        suggestion_type="Annotation"
    )




# Stratégie 2 : Exploration Équilibrée des Zones Urbaines
def suggeste_uexplored_zone_annotation(user_id):
        """
        Récupère l'ID d'une annotation qui incite l'utilisateur à élargir sa couverture géographique.

        :param user_id: ID de l'utilisateur.
        :return: ID d'une annotation pertinente ou None.
        """
        # Récupérer les annotations existantes de l'utilisateur
        annotations = fetch_user_annotations(user_id)  # Implémentez cette fonction pour récupérer les annotations

        # Identifier les zones inexplorées
        unexplored_zones = get_unexplored_zones(user_id, annotations)

        if not unexplored_zones:
            print(f"Aucune zone inexplorée trouvée pour l'utilisateur {user_id}.")
            return None

        # Pour chaque zone inexplorée, récupérer les annotations autour
        for zone in unexplored_zones:
            location = {'lat': zone[0], 'lon': zone[1]}
            nearby_annotations = fetch_annotations_around_location(location, radius_km=5)
            
            if nearby_annotations:
                # Retourner l'ID de la première annotation trouvée à proximité
                return nearby_annotations[0][0]  # ID de l'annotation

        return None



def generate_exploration_recommendations(user_id, suggester):
    """
    Génère une recommandation pour une exploration plus équilibrée des zones urbaines et l'enregistre.
    
    :param user_id: ID de l'utilisateur.
    :param suggester: Paramètre supplémentaire pour fournir des informations sur le suggester.
    :return: Texte de recommandation pour l'exploration des zones urbaines.
    """
    indicators = fetch_indicators(user_id)
    dmapp = get_indicator_value(indicators, "DMAPP")
    spc = get_indicator_value(indicators, "SPC")

    # Vérification si les indicateurs sont disponibles
    if dmapp is None or spc is None:
        print(f"DMAPP ou SPC non trouvé pour l'utilisateur {user_id}.")
        return "Nous n'avons pas trouvé de recommandations d'exploration pour vous pour le moment."

    # Vérifier les conditions de déclenchement
    if dmapp < DMAPP_THRESHOLD and spc < SPC_THRESHOLD:
        recommended_text = (
            "Nous avons remarqué que vos explorations urbaines sont concentrées dans certaines zones. "
            "Essayez de visiter de nouvelles zones pour diversifier vos explorations urbaines."
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id=user_id,
            category="CAT4-Urban",
            strategy="CoverageExpand",
            title="Pars découvrir de nouveaux quartiers !",
            recommendation=(
                "Tes annotations se situent toujours dans les mêmes lieux. "
                "Élargis ton horizon en explorant cette annotation dans un quartier encore inexploré. "
                "C’est l’occasion parfaite de partager tes découvertes et d’enrichir ta carte !"
            ),
            suggestion=suggeste_uexplored_zone_annotation(user_id),  # Utiliser le paramètre suggester
            suggestion_type="Annotation"  # Assurez-vous que ce champ est approprié
        )



# Stratégie 3 : Diversification des Types d’Annotations Urbaines
def generate_annotation_diversity_recommendations(user_id, suggester):
    """
    Génère une recommandation pour diversifier les types d'annotations urbaines de l'utilisateur et l'enregistre.
    
    :param user_id: ID de l'utilisateur.
    :param since_date: Date à partir de laquelle les annotations sont considérées.
    :return: Texte de recommandation pour la diversification des annotations urbaines.
    """

    
    indicators = fetch_indicators(user_id)
    dau = get_indicator_value(indicators, "DAU")

    if dau is None:
        print(f"DAU non trouvé pour l'utilisateur {user_id}.")
        return "Nous n'avons pas trouvé de recommandations pour diversifier vos annotations urbaines pour le moment."

    # Vérifier les conditions de déclenchement
    if dau < DAU_THRESHOLD:
        recommended_text = (
            "Nous avons remarqué que vos annotations se concentrent sur certains types. "
            "Essayez de diversifier vos annotations en explorant différents aspects urbains."
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id=user_id,
            category="CAT4-Urban",
            strategy="Diversification des Types d’Annotations Urbaines",
            title="Ose explorer de nouveaux horizons !",
            recommendation="Il est temps d'élargir tes découvertes ! En annotant des zones encore inexplorées, tu enrichiras ton expérience et découvriras des facettes inédites de la ville. Pour commencer, jette un œil à cette annotation qui pourrait t'inspirer.",
            suggestion="suggestion",
            suggestion_type="suggestion_type"
        )



# Stratégie 4 : Réduction de la Concentration des Annotations
def generate_annotation_concentration_recommendations(user_id, suggester):
    """
    Génère une recommandation pour réduire la concentration des annotations urbaines de l'utilisateur et l'enregistre.
    
    :param user_id: ID de l'utilisateur.
    :return: Texte de recommandation pour la réduction de la concentration des annotations.
    """    
    indicators = fetch_indicators(user_id)
    dc = get_indicator_value(indicators, "DC")

    if dc is None:
        print(f"DC non trouvé pour l'utilisateur {user_id}.")
        return "Nous n'avons pas trouvé de recommandations pour réduire la concentration de vos annotations pour le moment."

    # Vérifier les conditions de déclenchement
    if dc > DC_THRESHOLD:
        recommended_text = (
            "Nous avons remarqué que vos annotations sont très concentrées dans certaines zones. "
            "Essayez de disperser vos annotations pour explorer davantage la ville."
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id=user_id,
            category="CAT4-Urban",
            strategy="ConcentrationReduce",
            recommendation=recommended_text,
            suggestion="suggestion",
            suggestion_type="suggestion_type"
        )


def generate_all_urban_recommendations():
    """
    Génère des recommandations d'engagement pour tous les utilisateurs.
    """
    user_ids = fetch_user_ids()
    for user in user_ids:
        # Récupérer les indicateurs pour l'utilisateur
        indicators = fetch_indicators(user)

        suggester = Suggester(user_id=user)
        # Générer des recommandations pour chaque utilisateur
        if indicators is not None and len(indicators) > 0:
            generate_poi_recommendations(user, suggester)
            # generate_annotation_concentration_recommendations(user, suggester)
            # generate_exploration_recommendations(user, suggester)
            # generate_annotation_diversity_recommendations(user, suggester)
        else:
            print(f'No indicators found for user: {user}')
