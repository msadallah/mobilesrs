from datetime import datetime
from modules.db_operations import fetch_indicators, get_indicator_value, get_user_registration_date, insert_recommendation
from modules.es_operations import fetch_user_ids
from modules.suggester import Suggester

# Définir les seuils pour les recommandations
# Ces seuils sont basés sur des comportements utilisateur typiques observés pendant la phase de démarrage.
THRESHOLD_NIA = 10  # Nombre d'actions initiales attendues pour un démarrage satisfaisant
THRESHOLD_NIS = 3   # Nombre de partages initiaux pour considérer un utilisateur comme engagé
THRESHOLD_NCA = 2   # Nombre minimum d'annotations créées pour un engagement initial
THRESHOLD_NC = 1   # Nombre minimum de commenntaires
THRESHOLD_NR = 1   # Nombre minimum de réactions
THRESHOLD_NCR = 1   # Nombre de routes créées pour un utilisateur créatif
THRESHOLD_NDL = 1  # Nombre de lieux découverts
SUF_THRESHOLD = 0.5  # Seuil de score d'utilisation des fonctionnalités en dessous duquel une recommandation est nécessaire

THRESHOLD_NCOA = 3 # Nombre d'annotations consultés

SUF_CREATION_THRESHOLD = 0.3
SUF_EXPLORATION_THRESHOLD = 0.5
SUF_INTERACTION_THRESHOLD = 0.2

from datetime import datetime

def days_since_registration(registration_date, today=None):
    """
    Retourne le nombre de jours écoulés depuis la date d'inscription de l'utilisateur.
    
    :param registration_date: La date d'inscription de l'utilisateur (objet datetime).
    :param today: La date actuelle (objet datetime), par défaut, la date du jour.
    :return: Le nombre de jours écoulés depuis la date d'inscription.
    """
    if today is None:
        today = datetime.today()
    
    return (today - registration_date).days

def is_in_discovery_phase(registration_date, threshold=14, today=None):
    """
    Vérifie si un utilisateur est nouveau (inscrit il y a moins de `threshold` jours).
    
    :param registration_date: La date d'inscription de l'utilisateur (objet datetime).
    :param threshold: Le nombre de jours qui définit qu'un utilisateur est encore nouveau (par défaut 14 jours).
    :param today: La date actuelle (objet datetime), par défaut, la date du jour.
    :return: True si l'utilisateur est inscrit il y a moins de `threshold` jours, False sinon.
    """
    days_since = days_since_registration(registration_date, today)
    return True
    return days_since < threshold



def generate_initial_support_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations pour le soutien initial basé sur les indicateurs NIA et NIS.
    """
    nia = get_indicator_value(indicators, 'NIA')
    nis = get_indicator_value(indicators, 'NIS')
    registration_date_str = get_user_registration_date(user_id)
    
    # Convertir la date d'inscription en objet datetime
    registration_date = datetime.strptime(registration_date_str, '%Y-%m-%d')
    today = datetime.now()

    # Vérifier si l'utilisateur vient de s'inscrire
    if (today - registration_date).days <= 3:
        insert_recommendation(
            user_id, 
            "Adoption et Intégration", 
            "InitSupport",
            "Bienvenue dans MOBILES !", 
            "Découvre comment annoter tes expériences de la ville et explorer les récits des autres avec notre guide de démarrage.",
            'https://mobiles-projet.huma-num.fr/lapplication/tutoriel-mobiles/',
            "LIEN"
        )
    
    # Si NIA < T1 après 3 jours
    if nia is not None and (today - registration_date).days > 3  and nia < THRESHOLD_NIA:
        insert_recommendation(
            user_id, 
            "Adoption et Intégration", 
            "InitSupport",
            "Explore et Interagis !", 
            "Tu as déjà fait tes premiers pas, maintenant, il est temps de t'engager pleinement ! Découvre ce que d'autres partagent et ajoute tes propres expériences. Suis notre tutoriel pour apprendre à interagir efficacement",
            'https://mobiles-projet.huma-num.fr/lapplication/tutoriel-mobiles/',
            "LIEN"
        )

    # Si NIA ≥ T1 mais NIS < T2 après 6 jours
    if nia is not None and nia >= THRESHOLD_NIA and nis is not None and (today - registration_date).days > 6 and nis < THRESHOLD_NIS:
        insert_recommendation(
            user_id, 
            "Adoption et Intégration", 
            "InitSupport",
            "Documente et partage tes expériences", 
            "MOBILES t'offre un espace unique pour rendre compte de tes expériences de la ville et les partager avec les personnes de ton choix. Consulte notre guide pour apprendre comment partager tes récits de manière simple et impactante.",
            'https://mobiles-projet.huma-num.fr/lapplication/tutoriel-mobiles/',
            "LIEN"
        )


def generate_feature_usage_recommendations(user_id, indicators, suggester):
    """
    Génère des recommandations basées sur les indicateurs pour chaque stratégie.
    """
    # Récupération des valeurs des indicateurs
    nca = get_indicator_value(indicators, 'NCA')
    ncr = get_indicator_value(indicators, 'NCR')
    suf_creation = get_indicator_value(indicators, 'SUF_Creation')
    ndl = get_indicator_value(indicators, 'NDL')
    ncoa = get_indicator_value(indicators, 'NCoA')
    suf_exploration = get_indicator_value(indicators, 'SUF_Exploration')
    nc = get_indicator_value(indicators, 'NC')
    nr = get_indicator_value(indicators, 'NR')
    suf_interaction = get_indicator_value(indicators, 'SUF_Interaction')

    

    # Stratégie 2: Fonctionnalités clés – Création et partage
    
    if suf_creation != None:
        handle_creation_and_sharing(user_id, nca, ncr, suf_creation, suggester)

    # Stratégie 3: Fonctionnalités clés – Exploration de Contenu
    if suf_exploration != None:
        handle_content_exploration(user_id, ndl, ncoa, suf_exploration, suggester)

    # Stratégie 4: Fonctionnalités clés – Interaction
    if suf_interaction != None:
        handle_interaction(user_id, nc, nr, suf_interaction, suggester)

def handle_creation_and_sharing(user_id, nca, ncr, suf_creation, suggester):
    category = "CAT1-Adoption_Integration"
    strategy = "FuncCreateShare"
    suggestion_type ='annotation'
    strategies = ['rich_annotations','annotations_based_on_interests']
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    if suf_creation < SUF_CREATION_THRESHOLD:
        # strategies = ['nearby_annotations', 'interest_based_annotations']        
        suggestion_type ='annotation'
        title = "Lance toi ! Crée tes premières annotations"
        content = f"Tu ne sais pas quoi annoter ? Regarde quelques exemples qui pourront t'inspirer."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type) 

    elif nca < THRESHOLD_NCA:
        suggestion_type ='annotation'
        title = "Partage tes découvertes !"
        content = f"Tu as certainement beaucoup d'expériences à décrire ! Raconte, documente et partage ce que tu sens ou ressens dans les différents lieux que tu fréquentes. Inspire-toi de cette annotation."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)

    elif ncr == THRESHOLD_NCR :
        suggestion_type ='tour'
        strategies = ['nearby_tours']
        suggestions = suggester.suggest(strategies)
        suggestion =suggestions[0].get('id')
        title = "As tu essayé de décrire des trajets ou des balades ?"
        content = f"Suis notre tutoriel interactif pour apprendre à créer des  parcours et découvre celui-ci pour t'inspirer."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)


def handle_content_exploration(user_id, ndl, ncoa, suf_exploration, suggester):
    category = "CAT1-Adoption_Integration"
    strategy = "FuncContentExplore"
    suggestion_type ='annotation'
    strategies = ['rich_annotations','popular_annotations','annotations_based_on_interests']
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')
    if suf_exploration < SUF_EXPLORATION_THRESHOLD:
        suggestion ='https://mobiles-projet.huma-num.fr/lapplication/tutoriel-mobiles/'
        suggestion_type ='link'
        title = "Optimise ton expérience d'exploration !"
        content = f"Pour découvrir des lieux et contenus qui correspondent vraiment à tes intérêts, essaie nos outils de filtrage et de recherche. Apprends comment maximiser tes explorations et commence dès maintenant à dénicher les trésors cachés de la ville."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)

    elif ndl < THRESHOLD_NDL:
        strategies = ['rich_annotations','uexplored_zone_annotation','popular_annotations']
        suggestions = suggester.suggest(strategies)
        suggestion =suggestions[0].get('id')
        title = "Explore des lieux inédits !"
        content = f"La ville regorge de surprises ! Il y a tant de lieux fascinants à découvrir. Plonge dans l'exploration en consultant les annotations d'autres utilisateurs sur des endroits que tu n'as pas encore vus. Voici un lieu qui pourrait vraiment te surprendre."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)

    elif ncoa < THRESHOLD_NCOA:
        strategies = ['rich_annotations']
        suggestions = suggester.suggest(strategies)
        suggestion =suggestions[0].get('id')
        title = "Découvre de nouvelles expériences partagées !"
        content = f"Les annotations de la communauté te dévoilent des histoires et des détails uniques. Enrichis ton exploration en découvrant les lieux sous un nouvel angle, grâce aux expériences des autres. Pour commencer, voici une annotation qui pourrait t'inspirer."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)


def handle_interaction(user_id, nc, nr, suf_interaction, suggester):
    category = "CAT1-Adoption_Integration"
    strategy = "FuncInteraction"
    suggestion_type ='annotation'
    strategies = ['rich_annotations', 'annotations_based_on_reactions']
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')

    if suf_interaction < SUF_INTERACTION_THRESHOLD:
        title = "Fais entendre ta voix !"
        content = f"Tes réactions et commentaires enrichissent le contenu partagé et contribuent à faire progresser les expériences de la ville. Chaque interaction apporte de nouvelles perspectives et valorise les contributions des autres. Découvre comment interagir efficacement grâce à notre guide, et fais le premier pas avec cette annotation."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)

    elif nc < THRESHOLD_NC:
        title = "Participe aux discussions !"
        content = f"Les commentaires rendent les échanges plus vivants et intéressants. Apprends comment commenter efficacement en suivant notre tutoriel. Rejoins la conversation autour de cette annotation très commentée."
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)

    elif nr < THRESHOLD_NR:
        title = "Réagis aux contributions !"
        content = f"Les réactions sont un moyen simple et puissant de montrer ce que tu ressens face aux annotations des autres. Chaque réaction contribue à l'échange et à la dynamique de la communauté. Voici une annotation qui a déjà touché de nombreux utilisateurs. Réagis, et explore notre guide pour découvrir comment enrichir tes interactions !"
        insert_recommendation(user_id, category, strategy, title, content, suggestion, suggestion_type)



def generate_adoption_integration_recommendations():
    """
    Génère des recommandations pour une liste d'utilisateurs, uniquement pour ceux inscrits depuis moins de 2 semaines.
    """
    user_ids = fetch_user_ids()
    
    for user_id in user_ids:
        # Récupérer la date d'inscription de l'utilisateur
        registration_date_str = get_user_registration_date(user_id)
        registration_date = datetime.strptime(registration_date_str, '%Y-%m-%d')
        
        # Vérifier si l'utilisateur est nouveau (inscrit depuis moins de 2 semaines)
        if not is_in_discovery_phase(registration_date):
            print(f"L'utilisateur {user_id} n'est plus nouveau, aucune recommandation générée.")
            continue  # Passer à l'utilisateur suivant s'il n'est pas nouveau
        else:
            # Récupérer les indicateurs pour l'utilisateur
            indicators = fetch_indicators(user_id)
            # print('- User ID: ',user_id, ' \n - indicators: ', indicators)
            
            # Générer des recommandations pour chaque stratégie uniquement si des indicateurs sont présents
            if indicators is not None and len(indicators) > 0:
                suggester = Suggester(user_id=user_id)
                generate_initial_support_recommendations(user_id, indicators, suggester)
                if days_since_registration(registration_date) > 7:
                    generate_feature_usage_recommendations(user_id, indicators, suggester)
            else:
                print('No indicators found for user:', user_id)