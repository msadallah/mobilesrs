from modules.db_operations import fetch_indicators, get_indicator_value, insert_recommendation
from modules.es_operations import fetch_user_ids

TAC_THRESHOLD = 0.1
TEC_THRESHOLD = 0.2
TCC_THRESHOLD = 0.8
DSA_THRESHOLD = 0.7
TRC_THRESHOLD = 0.5
TCCA_THRESHOLD = 0.1

def generate_auto_evaluation_recommendation(user_id):
    """
    Génère une recommandation pour encourager les utilisateurs à améliorer leurs contributions en révisant
    et en modifiant leurs annotations.
    
    :param user_id: ID de l'utilisateur.
    """
    # Récupérer les indicateurs d'auto-évaluation
    indicators = fetch_indicators(user_id)
    tac = get_indicator_value(indicators, "TAC")
    
    
    # Vérifier les conditions de déclenchement
    if tac < TAC_THRESHOLD:
        # Générer la recommandation
        recommendation_message = (
            "Revoyez vos annotations et apportez des améliorations pour enrichir vos contributions. "
            "Explorez les commentaires pour voir comment vous pouvez ajuster et améliorer !"
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id,
            "Interaction et réflexion",
            "Auto-Évaluation et Amélioration des Contributions",
            recommendation_message
        )
        
        print(f"Recommandation générée pour l'utilisateur {user_id}.")
    else:
        print(f"Taux d'Amélioration des Contributions suffisant pour l'utilisateur {user_id}. Pas de recommandation nécessaire.")


def generate_interaction_community_recommendation(user_id):
    """
    Génère une recommandation pour encourager les utilisateurs à commenter et répondre aux contributions des autres.
    
    :param user_id: ID de l'utilisateur.
    """
    # Récupérer les indicateurs d'engagement communautaire
    indicators = fetch_indicators(user_id)
    tec = get_indicator_value(indicators, "TEC")
    
    
    # Vérifier les conditions de déclenchement
    if tec < TEC_THRESHOLD:
        # Générer la recommandation
        recommendation_message = (
            "Participez aux discussions : commentez et répondez aux contributions des autres membres pour "
            "enrichir les échanges et améliorer l'engagement communautaire !"
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id,
            "Interaction et réflexion",
            "Interaction avec les Contributions Communautaires",
            recommendation_message
        )
        
        print(f"Recommandation générée pour l'utilisateur {user_id}.")
    else:
        print(f"Taux d'Engagement Communautaire suffisant pour l'utilisateur {user_id}. Pas de recommandation nécessaire.")

def generate_reactivity_to_comments_recommendation(user_id):
    """
    Génère une recommandation pour encourager les utilisateurs à répondre aux commentaires reçus sur leurs annotations.
    
    :param user_id: ID de l'utilisateur.
    """
    # Récupérer les indicateurs de réactivité aux commentaires
    indicators = fetch_indicators(user_id)
    trc = get_indicator_value(indicators, "TRC")
    
    
    # Vérifier les conditions de déclenchement
    if trc < TRC_THRESHOLD:
        # Générer la recommandation
        recommendation_message = (
            "Répondez aux commentaires sur vos annotations pour enrichir le dialogue et améliorer la collaboration !"
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id,
            "Interaction et réflexion",
            "Réactivité aux Commentaires sur les Contributions",
            recommendation_message
        )
        
        print(f"Recommandation générée pour l'utilisateur {user_id}.")
    else:
        print(f"Taux de Réponse aux Commentaires suffisant pour l'utilisateur {user_id}. Pas de recommandation nécessaire.")


def generate_commenting_others_recommendation(user_id):
    """
    Génère une recommandation pour encourager les utilisateurs à commenter les contributions des autres membres.
    
    :param user_id: ID de l'utilisateur.
    """
    # Récupérer les indicateurs de commentaires sur les contributions des autres
    indicators = fetch_indicators(user_id)
    tcca = get_indicator_value(indicators, "TCC")
    
    # Définir le seuil pour déclencher la recommandation
    
    
    # Vérifier les conditions de déclenchement
    if tcca < TCCA_THRESHOLD:
        # Générer la recommandation
        recommendation_message = (
            "Participez activement en commentant les contributions des autres membres pour enrichir les échanges "
            "et renforcer la communauté !"
        )
        
        # Enregistrer la recommandation
        insert_recommendation(
            user_id,
            "Interaction et réflexion",
            "Commenter les Contributions des Autres",
            recommendation_message
        )
        
        print(f"Recommandation générée pour l'utilisateur {user_id}.")
    else:
        print(f"Taux de Commentaire sur les Contributions des Autres suffisant pour l'utilisateur {user_id}. Pas de recommandation nécessaire.")


def generate_all_reflection_recommendations():
    """
    Génère des recommandations d'engagement pour tous les utilisateurs.
    """
    user_ids = fetch_user_ids()
    for user in user_ids:
        # Récupérer les indicateurs pour l'utilisateur
        indicators = fetch_indicators(user)
        # Générer des recommandations pour chaque utilisateur
        if indicators is not None and len(indicators) > 0:
            generate_auto_evaluation_recommendation(user)
            generate_interaction_community_recommendation(user)
            generate_reactivity_to_comments_recommendation(user)
            generate_commenting_others_recommendation(user)
            
        else:
            print(f'No indicators found for user: {user}')
