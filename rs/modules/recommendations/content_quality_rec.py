import pandas as pd
import ast
import seaborn as sns
import matplotlib.pyplot as plt
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler, MultiLabelBinarizer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestRegressor
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import CountVectorizer
from pandas import json_normalize
from scipy import stats
from sqlalchemy import create_engine, Column, Integer, String, Boolean, DateTime, Float, JSON, inspect, func, MetaData, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.types import Float
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime
import numpy as np
from shapely.geometry import Point, MultiPoint, Polygon
from sklearn.cluster import DBSCAN
import geopandas as gpd
import math
import warnings
import json
import math
from collections import Counter
import requests
import json
from datetime import datetime
from sqlalchemy.exc import SQLAlchemyError
from collections import Counter
import warnings

from modules.suggester import Suggester
from modules.db_operations import fetch_indicators, get_indicator_value, insert_recommendation
from modules.es_operations import fetch_user_ids


# Seuils d'exemple
SQPA_THRESHOLD = 50  # Score de qualité de profil d'annotation (sur 100)
SPA_THRESHOLD = 30   # Score de profondeur d'annotation (sur 100)
VL_THRESHOLD = 200   # Volume lexical (nombre de mots minimum)
DL_THRESHOLD = 50    # Diversité lexicale (indice de diversité)
IDI_THRESHOLD = 3    # Diversité des icônes (nombre minimum de types d'icônes différents)
IFI_THRESHOLD = 5    # Fréquence d'utilisation des icônes (nombre total d'icônes minimum)
IRT_THRESHOLD = 5    # Richesse des tags (nombre de tags différents minimum)
UE_THRESHOLD = 2     # Utilisation d'émoticônes (nombre minimum d'émoticônes)
TI_THRESHOLD = 1     # Taux d'utilisation d'image (au moins 1 image par annotation)


# Désactiver tous les avertissements
warnings.filterwarnings("ignore")

import logging
# Configurer le niveau de journalisation pour ne montrer que les erreurs et les avertissements
logging.basicConfig(level=logging.ERROR)
for logger_name in ['sqlalchemy', 'sqlalchemy.engine']:
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.ERROR)
    logger.propagate = False


nltk.download('stopwords')
nltk.download('punkt')

stop_words = set(stopwords.words('french'))


def get_annotation_location(df_traces, df_recommendations, df_indicator,annotation_id):
    annotation = df_traces[df_traces['post_id'] == annotation_id].iloc[0]
    return {'lon': annotation['position.lon'], 'lat': annotation['position.lat']}

def calculate_geo_distance(place1, place2):
    # print(place1)
    lon1, lat1 = place1['lon'], place1['lat']
    lon2, lat2 = place2['lon'], place2['lat']

    # Haversine formula
    R = 6371  # Radius of Earth in kilometers
    dlon = math.radians(lon2 - lon1)
    dlat = math.radians(lat2 - lat1)
    a = math.sin(dlat / 2) ** 2 + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.sin(dlon / 2) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = R * c

    return distance
def get_sorted_place_types(user_annotations):
    place_types = user_annotations['placeType'].tolist()
    counter = Counter(place_types)
    sorted_place_types = [pair[0] for pair in counter.most_common()]
    return sorted_place_types



def get_taux_utilisation_value(df_indicator, user_id, taux_type):
    user_condition = (df_indicator['user_id'] == user_id)
    type_condition = (df_indicator['type'] == taux_type)

    if df_indicator.loc[user_condition & type_condition].empty:
        print(f"Aucune ligne correspondante pour user_id={user_id} et taux_type={taux_type}")
        return 0

    return df_indicator.loc[user_condition & type_condition, 'value'].iloc[0]




def build_clusters(annotations, n_clusters=5):
    # Assuming 'position.lon' and 'position.lat' are separate columns
    X = annotations[['position.lon', 'position.lat']]
    
    # Assuming 'position.lon' and 'position.lat' are in degrees, converting to radians
    X['position.lon_radians'] = np.radians(X['position.lon'])
    X['position.lat_radians'] = np.radians(X['position.lat'])
    
    # Dropping the original lon and lat columns
    X = X.drop(['position.lon', 'position.lat'], axis=1)

    kmeans = KMeans(n_clusters=n_clusters, random_state=42).fit(X)
    annotations['cluster_id'] = kmeans.labels_
    
    return annotations


def get_cluster_annotations(df_traces, df_recommendations, df_indicator,cluster_id):
    # Replace this with the function to get annotations in a specific cluster
    return df_traces[df_traces['cluster_id'] == cluster_id].to_dict(orient='records')

def calculate_thresholds(df_traces, df_recommendations, df_indicator,):
    # Calculate the thresholds dynamically based on the statistics of annotations
    seuil_masstextuelle = df_traces['volume_lexical'].quantile(0.7)
    seuil_diversitetextuelle = df_traces['diversity_lexical'].quantile(0.5)
    seuil_positive_sentiment_ratio = df_traces['positive_sentiment_ratio'].quantile(0.5)

    return seuil_masstextuelle, seuil_diversitetextuelle, seuil_positive_sentiment_ratio



## # Reco texts
reco_textual = {'title': 'Affine tes commentaires', 'content' : 'Enrichis tes annotations avec des détails pour les rendre plus percutantes et informatives. ', 'category':'Recommendation'}
reco_emo = {'title':'Exprime tes émotions', 'content' : 'Anime tes annotations avec des récits personnels et des icônes émotionnelles, insufflant ainsi vie à tes émotions et souvenirs. ', 'category':'Recommendation'}
reco_expr_activ = {'title':'Diversifie tes expressions', 'content' : 'Décris-nous une de tes activités, anime chaque moment avec des descriptions détaillées et des icônes pertinentes. ', 'category':'Recommendation'}
reco_expr_emo = {'title':'Diversifie tes expressions', 'content' : 'Découvre un lieu unique, partage-le avec des icônes évocatrices, et plonge-nous dans ton expérience immersive. ', 'category':'Recommendation'}
reco_expr_sens = {'title':'Diversifie tes expressions', 'content' : 'Explore une expérience sensorielle, traduis les couleurs, les odeurs, les sons avec des mots et des icônes. ', 'category':'Recommendation'}
reco_expr_soc = {'title':'Diversifie tes expressions', 'content' : 'Partage tes expériences sociales avec des récits détaillés, enrichis-les avec des icônes sociales pour une touche visuelle engageante. ', 'category':'Recommendation'}
reco_expr_aff = {'title':'Diversifie tes expressions', 'content' : 'Partage tes moments affectifs avec des descriptions détaillées et ajoute une touche visuelle émouvante avec des icônes affectives. ', 'category':'Recommendation'}
reco_graphical = {'title':'Capture l\'instant', 'content' : 'Enrichis tes annotations avec des photos captivantes, offrant une plongée visuelle dans l\'essence de tes expériences inoubliables. ', 'category':'Recommendation'}
reco_sp_div = {'title':'Enrichis tes découvertes', 'content' : 'Élargis tes horizons géographiques en explorant une diversité de lieux et en découvrant de nouveaux territoires pour enrichir tes aventures. ', 'category':'Recommendation'}
reco_clust = {'title':'Equilibre tes explorations', 'content' : "Pour enrichir ta découverte de Lyon, maintiens un équilibre entre proximité et éloignement, en explorant des endroits proches et d'autres plus éloignés les uns des autres. ", 'category':'Recommendation'}




#  NEW VERSION
def get_user_indicators(user_id):
    indicators = fetch_indicators(user_id)
    q_indicators = {
        'SQPA': get_indicator_value(indicators, 'SQPA'),
        'SPA': get_indicator_value(indicators, 'SPA'),
        'VL': get_indicator_value(indicators, 'VL'),
        'DL': get_indicator_value(indicators, 'DL'),
        'IDI': get_indicator_value(indicators, 'IDI'),
        'IFI': get_indicator_value(indicators, 'IFI'),
        'IRT': get_indicator_value(indicators, 'IRT'),
        'UE': get_indicator_value(indicators, 'UE'),
        'TI': get_indicator_value(indicators, 'TI'),
    }
    return q_indicators



def recommend_quality_improvement(indicators, user_id, suggester):
    if indicators['SQPA'] < SQPA_THRESHOLD and indicators['SPA'] > SPA_THRESHOLD:
        strategies = ['annotations_based_on_interests']
        suggestion_type ='annotation'
        suggestions = suggester.suggest(strategies)
        suggestion =suggestions[0].get('id')
        # Enrichissement Général
        insert_recommendation(
            user_id,
            "CAT3-Quality",
            "QualityImprove",
            "Enrichissement Général",
            "Enrichis ton annotation avec plus de détails ! Ta [ANNOTATION] est déjà utile ! Pourquoi ne pas ajouter quelques anecdotes ou précisions pour la rendre encore plus complète et précieuse pour la communauté ?",
            suggestion,
            suggestion_type
        )
        # Ajoute d'autres recommandations si nécessaire...

def recommend_text_expression(indicators, user_id, suggester):
    if indicators['VL'] < VL_THRESHOLD and indicators['DL'] < DL_THRESHOLD:
        strategies = ['annotations_based_on_interests']
        suggestion_type ='annotation'
        suggestions = suggester.suggest(strategies)
        suggestion =suggestions[0].get('id')
        insert_recommendation(
            user_id,
            "CAT3-Quality",
            "TextIncentive",
            "Enrichis Améliore tes textes descriptions textuelles !",
            "Des annotations détaillées aident à mieux transmettre l'expérience. Enrichis tes contributions en ajoutant plus de précisions et de contexte.",
            suggestion,
            suggestion_type
        )

def recommend_symbolic_enrichment(indicators, user_id, suggester):
    below_threshold_count = 0
    last_below_threshold = None

    # Vérification des indicateurs sous le seuil
    if indicators['IDI'] < IDI_THRESHOLD:
        below_threshold_count += 1
        last_below_threshold = 'IDI'
        
    if indicators['IFI'] < IFI_THRESHOLD:
        below_threshold_count += 1
        last_below_threshold = 'IFI'
        
    if indicators['IRT'] < IRT_THRESHOLD:
        below_threshold_count += 1
        last_below_threshold = 'IRT'
        
    if indicators['UE'] < UE_THRESHOLD:
        below_threshold_count += 1
        last_below_threshold = 'UE'
    
    strategies = ['annotations_based_on_interests']
    suggestion_type ='annotation'
    suggestions = suggester.suggest(strategies)
    suggestion =suggestions[0].get('id')
    # Si plus de deux indicateurs sont sous les seuils -> Recommandation générale
    if below_threshold_count > 2:   
        insert_recommendation(
            user_id,
            "CAT3-Quality",
            "SymbolEnrich",
            "Améliore tes annotations avec des icônes symboles et mots-clés en utilisant toutes les formes d'expression!",
            "Tes annotations gagneraient à intégrer plus de symboles. Ajoute des d'icônes, de tags ou d'émoticônes pour enrichir ton contenu. Découvre l'annotation [ANNOTATION], un bon exemple pour t'inspirer.",
            suggestion,
            suggestion_type
        )
    # Sinon, envoie la recommandation spécifique pour l'indicateur en dessous du seuil
    else:
        if last_below_threshold == 'IDI':
            insert_recommendation(
                user_id,
                "CAT3-Quality",
                "SymbolEnrich-IconDiversity",
                "Varie les icônes dans tes annotations !",
                "Ta sélection d'icônes est limitée. Ajoute différents types d'icônes pour illustrer tes annotations. Cela apportera plus de richesse visuelle.",
                suggestion,
                suggestion_type
            )
        elif last_below_threshold == 'IFI':
            insert_recommendation(
                user_id,
                "CAT3-Quality",
                "SymbolEnrich",
                "Augmente l'impact de tes annotations avec plus d'icônes !",
                "Utilise davantage d'icônes pour améliorer la clarté et l'attrait de tes contributions.",
                suggestion,
                suggestion_type
            )
        elif last_below_threshold == 'IRT':
            insert_recommendation(
                user_id,
                "CAT3-Quality",
                "SymbolEnrich",
                "Augmente la visibilité de tes annotations avec des tags !",
                "Ajoute des tags supplémentaires pour mieux catégoriser ton contenu et le rendre plus facile à découvrir.",
                suggestion,
                suggestion_type
            )
        elif last_below_threshold == 'UE':
            insert_recommendation(
                user_id,
                "CAT3-Quality",
                "SymbolEnrich",
                "Ajoute une touche émotionnelle avec une émoticône !",
                "En intégrant des émoticônes, tu rends tes annotations plus personnelles. Humanises tes annotations et les rends plus engageantes.",
                suggestion,
                suggestion_type
            )


def recommend_graphic_expression(indicators, user_id, suggester):
    if indicators['TI'] < TI_THRESHOLD:
        strategies = ['annotations_based_on_interests']
        suggestion_type ='annotation'
        suggestions = suggester.suggest(strategies)
        suggestion =suggestions[0].get('id')
        insert_recommendation(
            user_id,
            "CAT3-Quality",
            "GraphicalExpress",
            "Intègre des images pour enrichir tes récits d'expérience !",
            "Les annotations accompagnées de photos et d'images racontent une histoire plus vivante et captivante. En ajoutant des visuels, tu peux mieux exprimer tes émotions et partager ton expérience.",
            suggestion,
            suggestion_type
        )

def generate_content_quality_recommendations():
    user_ids = fetch_user_ids()
    for user_id in user_ids:
        suggester = Suggester(user_id=user_id)
        indicators = get_user_indicators(user_id)
        if all(value < seuil for value, seuil in zip(indicators.values(), [SQPA_THRESHOLD, SPA_THRESHOLD, VL_THRESHOLD, DL_THRESHOLD, IDI_THRESHOLD, IFI_THRESHOLD, IRT_THRESHOLD, UE_THRESHOLD, TI_THRESHOLD])):
            recommend_quality_improvement(indicators, user_id, suggester)
        else:
            recommend_text_expression(indicators, user_id, suggester)
            recommend_symbolic_enrichment(indicators, user_id, suggester)
            recommend_graphic_expression(indicators, user_id, suggester)