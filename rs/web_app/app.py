# web_app/app.py

from flask import Flask

def create_app():
    app = Flask(__name__)
    app.config.from_pyfile('../config/settings.py')
   

    # Charger les routes de l'application
    from . import routes
    app.register_blueprint(routes.bp)

    return app
