import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from apscheduler.schedulers.background import BackgroundScheduler
from config.settings import Config

db = SQLAlchemy()
scheduler = BackgroundScheduler()

def create_app():
    print("create_app est appelé")
    app = Flask(__name__)
    app.config.from_object(Config)

    # Configuration de la journalisation
    app.logger.setLevel(logging.DEBUG)

    # Ajout de la clé secrète pour la gestion des sessions
    app.secret_key = 'madjidus'  # Remplacez par une clé aléatoire unique et sécurisée

    # Initialiser les extensions
    db.init_app(app)

    with app.app_context():
        # Importez tous les modèles ici
        from web_app.models import Indicator, Recommendation, Impact, ScheduledTask, ExecutionLog
        
        # Créer les tables si elles n'existent pas encore
        try:
            db.create_all()
            app.logger.info("Toutes les tables ont été créées avec succès.")
        except Exception as e:
            app.logger.error(f"Erreur lors de la création des tables: {e}")

        # Importer et enregistrer les routes après avoir initialisé db
        from web_app.routes import bp as web_bp
        app.register_blueprint(web_bp)

        # Ajouter les tâches planifiées
        from web_app.tasks import add_scheduled_jobs
        add_scheduled_jobs()

    # Démarrer le planificateur si ce n'est pas déjà fait
    if not scheduler.running:
        scheduler.start()

    return app

# Créer l'application Flask
app = create_app()
