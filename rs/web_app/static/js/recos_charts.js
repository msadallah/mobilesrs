document.addEventListener('DOMContentLoaded', function() {
    function showAlert(message) {
        document.getElementById('alertMessage').textContent = message;
        $('#alertModal').modal('show');
    }
    function loadAndInitializeModal(taskId) {
        const container = document.getElementById('recosDetailsModalContainer');

        const modalId = 'recosDetailsModal';
        
        // Supprime l'ancien modal s'il existe
        const existingModal = container.querySelector(`#${modalId}`);
        if (existingModal) {
            existingModal.remove();
        }

        if (container.querySelector('#recosDetailsModal')) {
            $('#recosDetailsModal').modal('show');
            resetModal();
            initializeModal();
            return;
        }

        fetch(`/recos_details/${taskId}`)
            .then(response => {
                if (response.status === 404) {
                    showAlert('Aucun détail à afficher pour cette tâche.');
                    return; // Sortir de la fonction
                }
                return response.text();
            })
            .then(html => {
                container.innerHTML = html;
                $('#recosDetailsModal').modal('show');
                resetModal();
                initializeModal();
            })
            .catch(error => console.error('Error loading modal content:', error));
    }

    function resetModal() {
        const recoTypeSelect = document.getElementById('recoTypeSelect');
        const recoDataTableContainer = document.getElementById('recoDataTableContainer');
        const recoUserSelect = document.getElementById('recoUserSelect');
        const recoTimeSelect = document.getElementById('recoTimeSelect');
        const customDateRange = document.getElementById('customDateRange');
        const startDate = document.getElementById('startDate');
        const endDate = document.getElementById('endDate');
    
        // Réinitialiser les sélections de type de données et de type de graphique
        if (recoTypeSelect) {
            recoTypeSelect.value = '';
        }
    
       
    
        // Réinitialiser les sélections d'utilisateurs
        if (recoUserSelect) {
            $(recoUserSelect).val(null).trigger('change'); // Utilise Select2 pour réinitialiser la sélection
        }
    
        // Réinitialiser la sélection de la période de temps
        if (recoTimeSelect) {
            recoTimeSelect.value = '';
        }
    
        // Réinitialiser les champs de date personnalisée
        if (customDateRange) {
            customDateRange.classList.add('d-none'); // Masquer les champs de date
        }
        if (startDate) {
            startDate.value = '';
        }
        if (endDate) {
            endDate.value = '';
        }
    
        // Réinitialiser l'affichage du tableau et du conteneur du graphique
        if (recoDataTableContainer) {
            recoDataTableContainer.style.display = 'none';
            const dataTable = document.getElementById('recoDataTable');
            if (dataTable) {
                dataTable.style.display = 'none';
            }
        }
    
        
    }
    

    function initializeModal() {
        console.log('Modal initialized');

        const recoTypeSelect = document.getElementById('recoTypeSelect');
        const recoTimeSelect = document.getElementById('recoTimeSelect');

        $('#recoUserSelect').select2({
            placeholder: "Utilisateurs",
            allowClear: true,
            width: '100%', // Utiliser 100% pour une largeur cohérente
            closeOnSelect: false // Garder le sélecteur ouvert après une sélection
        })
        .on('change', updateTable);;

        
        

        

        if (!recoTypeSelect ||   !recoTimeSelect) {
            console.error('Required select elements not found');
            return;
        }

        recoTypeSelect.addEventListener('change', updateTable);
        recoTimeSelect.addEventListener('change', updateTable);

        function handleDataFilter(){            
            console.log('user change')
        }

        // Charger les données JSON
        const jsonDataElement = document.getElementById('jsonDataScript');
        if (!jsonDataElement) {
            console.error('JSON data script not found.');
            return;
        }
        window.data = JSON.parse(jsonDataElement.textContent);

        document.getElementById('recoTimeSelect').addEventListener('change', function() {
            const selectedValue = this.value;
            const customDateRange = document.getElementById('customDateRange');
            
            if (selectedValue === 'custom') {
                customDateRange.classList.remove('d-none');
            } else {
                customDateRange.classList.add('d-none');
            }
        
            // Appeler la fonction de filtrage pour mettre à jour les données en fonction de la période sélectionnée
            filterRecoData();
        });

    }

 
    function handleTypeChange(event) {
        const selectedType = event.target.value;
        const recoDataTableContainer = document.getElementById('recoDataTableContainer');
        if (recoDataTableContainer) {
            recoDataTableContainer.style.display = 'none';
            const dataTable = document.getElementById('recoDataTable');
            if (dataTable) {
                dataTable.style.display = 'none';
            }
        }

        const chartContainer = document.getElementById('chartContainer');
        if (chartContainer) {
            chartContainer.style.display = 'none';
        }

        // Filtrer les données
        updateTable();
    }


    
    function updateTable() {
        const table = document.getElementById('recoDataTable');
        const data = filterRecoData();
    
        // Générer les lignes du tableau en fonction des champs des données
        const rows = data.map(item => `
            <tr>
                <td>${item.strategy || 'N/A'}</td>
                <td>${item.category || 'N/A'}</td>
                <td>${item.user_id || 'N/A'}</td>
                <td>${new Date(item.created_at).toLocaleString() || 'N/A'}</td>
                <td>${item.recommendation || 'N/A'}</td>
            </tr>
        `).join('');
    
        // Mettre à jour le contenu du tableau avec les nouvelles données
        table.innerHTML = `
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Stratégie</th>
                        <th>Catégorie</th>
                        <th>ID Utilisateur</th>
                        <th>Date de Création</th>
                        <th>Recommandation</th>
                    </tr>
                </thead>
                <tbody>
                    ${rows}
                </tbody>
            </table>
        `;
    }
    

    function filterRecoData() {
        // Récupérer la sélection multiple des utilisateurs
        const recoUserSelect = document.getElementById('recoUserSelect');
        const userFilter = $(recoUserSelect).val() || []; // Utilise jQuery pour obtenir les valeurs sélectionnées
        const recoTimeSelect = document.getElementById('recoTimeSelect').value || null;
        const type = document.getElementById('recoTypeSelect').value || null;
    
       
        const jsonDataElement = document.getElementById('jsonRecoDataScript');
        if (!jsonDataElement) {
            console.error('JSON data script not found.');
            return [];
        }
        const jsonData = JSON.parse(jsonDataElement.textContent);
    
        // Fonction utilitaire pour vérifier si une valeur est considérée comme non fournie
        function isNotProvided(value) {
            return value === null || value === undefined || value === '';
        }
    
        // Fonction pour obtenir la date de début et de fin en fonction de la sélection de période
        function getDateRange(selection) {
            const now = new Date();
            let start = new Date();
            let end = new Date();
    
            switch (selection) {
                case 'today':
                    start = end = now;
                    break;
                case 'this_week':
                    start.setDate(now.getDate() - now.getDay());
                    end.setDate(start.getDate() + 6);
                    break;
                case 'this_month':
                    start.setDate(1);
                    end = new Date(now.getFullYear(), now.getMonth() + 1, 0);
                    break;
                case 'this_year':
                    start = new Date(now.getFullYear(), 0, 1);
                    end = new Date(now.getFullYear(), 11, 31);
                    break;
                case 'custom':
                    start = new Date(document.getElementById('startDate').value);
                    end = new Date(document.getElementById('endDate').value);
                    break;
                default:
                    return { start: null, end: null };
            }
            return { start, end };
        }
    
        const { start, end } = getDateRange(recoTimeSelect);
    
        // Filtrer les données en fonction des filtres
        const filteredData = jsonData.filter(item => {
            let match = true;
            if (!isNotProvided(type)) {
                match = match && item.strategy === type;
            }
            if (userFilter.length > 0) {
                if (userFilter.includes('all')) {
                    match = match && true; // Sélectionne tous les utilisateurs
                } else {
                    match = match && userFilter.includes(String(item.user_id));
                }
            }
            if (start && end) {
                const itemDate = new Date(item.created_at);
                match = match && itemDate >= start && itemDate <= end;
            }

    
            return match;
        });
    
        if (filteredData.length === 0) {
            console.log('No data available for filtering.');
        }

        const recoDataTableContainer = document.getElementById('recoDataTableContainer');
        recoDataTableContainer.style.display = ''
        const dataTable = document.getElementById('recoDataTable');
        if (dataTable) {
            dataTable.style.display = '';
        }
        
    
        return filteredData;
    }
    
    

    document.querySelectorAll('.recos-details').forEach(button => {
        button.addEventListener('click', function() {
            const taskId = this.dataset.task;
            loadAndInitializeModal(taskId);
        });
    });
});
