from datetime import datetime

from sqlalchemy import func
from web_app import db

class Indicator(db.Model):
    __tablename__ = 'indicators'
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer)
    category = db.Column(db.String(255))
    strategy = db.Column(db.String(255))
    type = db.Column(db.String(255))
    value = db.Column(db.Text)
    date = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<Indicator {self.id}>'



class Recommendation(db.Model):
    __tablename__ = 'recommendations'
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer)
    category = db.Column(db.String(255))
    strategy = db.Column(db.String(255))
    title = db.Column(db.String(255))
    recommendation = db.Column(db.String(1020))
    suggestion = db.Column(db.String(255))
    suggestion_type = db.Column(db.String(255))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    
    # Appliquer une valeur par défaut au niveau du serveur de base de données
    status = db.Column(db.String(20), default='Created', server_default='Created')  
    
    status_updated_at = db.Column(db.DateTime, nullable=True, default=func.now())

    def __repr__(self):
        return f'<Recommendation {self.id}>'


from datetime import datetime
from flask_sqlalchemy import SQLAlchemy



class UserProfile(db.Model):
    __tablename__ = 'user_profiles'
    
    user_id = db.Column(db.Integer, primary_key=True)  # Lien vers la table des utilisateurs

    # Profils d'Annotation (PCA)
    pca_items_count  = db.Column(db.Float, default=0.0)  # Nombre d'annotations créées
    pca_avg_volume_lexical = db.Column(db.Float, default=0.0)  # Longueur moyenne du texte des annotations créées
    pca_avg_icons_per_annotation = db.Column(db.Float, default=0.0)  # Nombre moyen d'icônes par annotation
    pca_avg_tags_per_annotation = db.Column(db.Float, default=0.0)  # Nombre moyen de tags par annotation
    pca_emoticon_top3 = db.Column(db.JSON)  # Top 3 des émoticônes les plus utilisées
    pca_image_presence_percentage = db.Column(db.Float, default=0.0)  # Pourcentage d'annotations contenant des images
    pca_place_type_usage = db.Column(db.JSON)  # Proportion des types de lieux annotés
    pca_experience_frequency = db.Column(db.JSON)  # Fréquence des expériences annotées
    pca_covered_zones = db.Column(db.JSON)  # Zones couvertes par les annotations
    pca_avg_types_icons = db.Column(db.Float, default=0.0)  # Nombre moyen de types d'icônes utilisés par annotation
    pca_icon_type_ratio = db.Column(db.JSON)  # Ratio des types d'icônes utilisés
    pca_tag_type_ratio = db.Column(db.JSON)  # Ratio des types de tags utilisés

    # Profils de Consultation d'Annotation (PCoA)
    pcoa_items_count  = db.Column(db.Float, default=0.0)  # Nombre d'annotations visitées
    pcoa_avg_volume_lexical = db.Column(db.Float, default=0.0)  # Longueur moyenne du texte des annotations visitées
    pcoa_avg_icons_per_annotation = db.Column(db.Float, default=0.0)  # Nombre moyen d'icônes par annotation visitée
    pcoa_avg_tags_per_annotation = db.Column(db.Float, default=0.0)  # Nombre moyen de tags par annotation visitée
    pcoa_emoticon_top3 = db.Column(db.JSON)  # Top 3 des émoticônes les plus utilisées lors des visites
    pcoa_image_presence_percentage = db.Column(db.Float, default=0.0)  # Pourcentage d'annotations consultées contenant des images
    pcoa_place_type_usage = db.Column(db.JSON)  # Proportion des types de lieux visités
    pcoa_experience_frequency = db.Column(db.JSON)  # Fréquence des types d'expériences consultées
    pcoa_covered_zones = db.Column(db.JSON)  # Zones couvertes par les annotations visitées
    pcoa_recurrent_motifs = db.Column(db.JSON)  # Motifs de filtres récurrents lors des consultations
    pcoa_avg_types_icons = db.Column(db.Float, default=0.0)  # Nombre moyen de types d'icônes par annotation visitée
    pcoa_icon_type_ratio = db.Column(db.JSON)  # Ratio des types d'icônes consultés
    pcoa_tag_type_ratio = db.Column(db.JSON)  # Ratio des types de tags consultés

    # Profils de Création de Parcours (PCP)
    pcp_items_count  = db.Column(db.Float, default=0.0)  # Nombre de parcours créés
    pcp_avg_points = db.Column(db.Float, default=0.0)  # Nombre moyen de points par parcours créé
    pcp_point_density = db.Column(db.Float, default=0.0)  # Densité géographique des points créés
    pcp_frequent_geo_zones = db.Column(db.JSON)  # Zones géographiques fréquentes dans les parcours créés
    pcp_path_type_proportion = db.Column(db.JSON)  # Proportion des types de parcours créés (ex: GPS, manuel)
    pcp_avg_volume_lexical = db.Column(db.Float, default=0.0)  # Longueur moyenne du texte descriptif des parcours créés
    pcp_annotation_frequency = db.Column(db.JSON)  # Fréquence des annotations associées aux parcours créés
    pcp_covered_zones = db.Column(db.JSON)  # Zones couvertes par les parcours créés
    pcp_avg_types_icons = db.Column(db.Float, default=0.0)  # Nombre moyen de types d'icônes dans les parcours créés

    # Profils de Consultation de Parcours (PCoP)
    pcop_items_count  = db.Column(db.Float, default=0.0)  # Nombre de parcours visités
    pcop_avg_points = db.Column(db.Float, default=0.0)  # Nombre moyen de points par parcours visité
    pcop_point_density = db.Column(db.Float, default=0.0)  # Densité géographique des points consultés
    pcop_frequent_geo_zones = db.Column(db.JSON)  # Zones géographiques fréquentes dans les parcours visités
    pcop_path_type_proportion = db.Column(db.JSON)  # Proportion des types de parcours visités (ex: GPS, manuel)
    pcop_avg_volume_lexical = db.Column(db.Float, default=0.0)  # Longueur moyenne du texte descriptif des parcours visités
    pcop_annotation_frequency = db.Column(db.JSON)  # Fréquence des annotations associées aux parcours consultés
    pcop_covered_zones = db.Column(db.JSON)  # Zones couvertes par les parcours consultés
    pcop_avg_types_icons = db.Column(db.Float, default=0.0)  # Nombre moyen de types d'icônes dans les parcours visités

    # Métadonnées
    created_at = db.Column(db.DateTime, default=datetime.utcnow)  # Date de création du profil
    updated_at = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)  # Date de dernière mise à jour du profil

    def __repr__(self):
        return f"<UserProfile {self.user_id}: {self.to_dict()}>"
    
    def to_dict(self):
        return {column.name: getattr(self, column.name) for column in self.__table__.columns}

    
class Impact(db.Model):
    __tablename__ = 'impacts'
    
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer)
    category = db.Column(db.String(255))
    strategy = db.Column(db.String(255))
    analysis_type = db.Column(db.String(255))
    result = db.Column(db.Float)
    date = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f'<Impact {self.id}>'


class ScheduledTask(db.Model):
    __tablename__ = 'scheduled_task'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    action = db.Column(db.String(50), nullable=False)
    scheduled_time = db.Column(db.DateTime, nullable=False)
    creation_time = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    status = db.Column(db.String(20), default='Not Executed')
    last_run = db.Column(db.DateTime, nullable=True)
    recurrence = db.Column(db.String(50), nullable=True)  # Added for recurrence

    def __repr__(self):
        return f'<ScheduledTask {self.name}>'

class ExecutionLog(db.Model):
    __tablename__ = 'execution_logs'

    id = db.Column(db.Integer, primary_key=True)
    action_type = db.Column(db.String(255), nullable=False)  # Type d'action exécutée
    status = db.Column(db.String(50), nullable=False)  # Statut de l'exécution (e.g., success, failed)
    start_time = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)  # Début de l'exécution
    end_time = db.Column(db.DateTime, nullable=True)  # Fin de l'exécution
    duration = db.Column(db.Float, nullable=True)  # Durée de l'exécution en secondes
    details = db.Column(db.Text, nullable=True)  # Détails supplémentaires (e.g., erreurs, messages)

    def __init__(self, action_type, status, start_time=None, end_time=None, duration=None, details=None):
        self.action_type = action_type
        self.status = status
        self.start_time = start_time or datetime.utcnow()
        self.end_time = end_time
        self.duration = duration
        self.details = details

    def mark_complete(self, status, details=None):
        self.end_time = datetime.utcnow()
        self.duration = (self.end_time - self.start_time).total_seconds()
        self.status = status
        self.details = details

