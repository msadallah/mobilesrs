import logging
from datetime import datetime
import pytz

from flask import current_app
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.triggers.cron import CronTrigger

from modules.recommendations.adoption_integration_rec import generate_adoption_integration_recommendations
from modules.recommendations.content_quality_rec import generate_content_quality_recommendations
from modules.recommendations.engagement_reengagement_rec import generate_all_engagement_recommendations
from modules.recommendations.interaction_reflection_rec import generate_all_reflection_recommendations
from modules.recommendations.urban_discovery_rec import generate_all_urban_recommendations
from web_app import db, scheduler
from web_app.models import ScheduledTask, ExecutionLog

# Configuration du logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def create_app_context():
    """Crée et retourne un contexte d'application Flask."""
    from web_app import create_app
    app = create_app()
    return app.app_context()

def get_or_create_log(action_type):
    """
    Récupère un enregistrement de journalisation existant pour l'action spécifiée, ou en crée un nouveau.
    """
    log = ExecutionLog.query.filter_by(action_type=action_type, status='in_progress').first()
    if not log:
        log = ExecutionLog(action_type=action_type, status='in_progress')
        db.session.add(log)
        db.session.commit()
    return log

def update_log(log, status, details=None):
    """
    Met à jour l'enregistrement de journalisation avec le statut et les détails spécifiés.
    """
    log.status = status
    log.details = details
    log.end_time = datetime.utcnow()
    db.session.commit()

def execute_task_function(task_id):
    """
    Fonction exécutée par le planificateur. 
    Effectue l'action définie pour la tâche spécifiée par `task_id`.
    """
    with create_app_context() as app_context:
        task = ScheduledTask.query.get(task_id)
        if not task:
            logger.warning(f"Tâche {task_id} non trouvée.")
            return

        action_type = task.action
        log = get_or_create_log(action_type)

        try:
            # Exécute l'action associée à la tâche
            _execute_task_action(task)
            
            # Mise à jour du statut de la tâche
            task.status = 'Executed'
            task.last_run = datetime.now(pytz.utc)
            db.session.commit()
            update_log(log, status='success')
            logger.info(f"Tâche {task_id} exécutée avec succès.")
        
        except Exception as e:
            task.status = 'Failed'
            task.last_run = datetime.now(pytz.utc)
            db.session.commit()
            update_log(log, status='failed', details=str(e))
            logger.error(f"Erreur lors de l'exécution de la tâche {task_id}: {e}")

def _execute_task_action(task):
    """
    Exécute l'action spécifique associée à une tâche planifiée.
    """
    if task.action == 'fetch_raw_data':
        _execute_fetch_raw_data()
    elif task.action == 'calculate_metrics':
        _execute_calculate_metrics()
    elif task.action == 'construct_user_profiles':
        _execute_construct_user_profiles()
    # Cas pour chaque catégorie spécifique
    elif task.action == 'calculate_adoption_metrics':
        _execute_calculate_metrics(category='adoption')
    elif task.action == 'calculate_engagement_metrics':
        _execute_calculate_metrics(category='engagement')
    elif task.action == 'calculate_quality_metrics':
        _execute_calculate_metrics(category='quality')
    elif task.action == 'calculate_urban_discovery_metrics':
        _execute_calculate_metrics(category='urbain')
    elif task.action == 'calculate_interaction_reflection_metrics':
        _execute_calculate_metrics(category='reflexion') 
    elif task.action == 'generate_recommendations':
        generate_adoption_integration_recommendations()
        generate_all_engagement_recommendations()
        generate_content_quality_recommendations()
        generate_all_urban_recommendations()
        generate_all_reflection_recommendations()
    elif task.action == 'initiation_integration':
        _execute_initiation_integration_rec()
    elif task.action == 'engagement_promotion':
        _execute_engagement_promotion_rec()
    elif task.action == 'quality_improvement':
        _execute_quality_improvement_rec()
    elif task.action == 'urban_discovery':
        _execute_urban_discovery_rec()
    elif task.action == 'interaction_reflection':
        _execute_interaction_reflection_rec()
    elif task.action == 'send_notifications':
        _execute_send_notifications()
    elif task.action == 'full_process':
        _execute_full_process()
    else:
        logger.warning(f"Action non reconnue pour la tâche {task.id}: {task.action}")

# Implémentation des différentes actions
def _execute_fetch_raw_data():
    logger.info("Début de l'exécution de la tâche: Fetch and Preprocess Data.")
    from modules.db_operations import get_distant_users, get_distant_annotations
    from modules.preprocessing.preprocess_annotations import preprocess_annotations
    from modules.db_operations import save_users_to_local_database, save_annotations_to_local_database

    users = get_distant_users()
    save_users_to_local_database(users)
    annotations = get_distant_annotations()
    preprocess_annotations(annotations)
    save_annotations_to_local_database(annotations)


def _execute_calculate_metrics(category=None):
    logger.info("Début de l'exécution de la tâche: Calculate Metrics.")
    from modules.compute_metrics import calculate_metrics
    calculate_metrics()

def _execute_construct_user_profiles():
    logger.info("Début de l'exécution de la tâche: Construction des profiles.")
    from modules.db_operations import get_distant_users, get_distant_annotations
    from modules.preprocessing.preprocess_annotations import preprocess_annotations
    from modules.db_operations import save_users_to_local_database, save_annotations_to_local_database

def _execute_generate_all_recommendendations():
    logger.info("Début de l'exécution de la tâche: Generate all recommendations.")
    generate_adoption_integration_recommendations()
    generate_all_engagement_recommendations()
    generate_content_quality_recommendations()
    generate_all_urban_recommendations()
    generate_all_reflection_recommendations()

def _execute_initiation_integration_rec():
    logger.info("Début de l'exécution de la tâche: Initiation Integration.")
    generate_adoption_integration_recommendations()

def _execute_engagement_promotion_rec():
    logger.info("Début de l'exécution de la tâche: Engagement Promotion.")
    from modules.generate_recommendations import verifier_engagement_utilisateur
    generate_all_engagement_recommendations()

def _execute_quality_improvement_rec():
    logger.info("Début de l'exécution de la tâche: Quality Improvement.")
    generate_content_quality_recommendations()

def _execute_urban_discovery_rec():
    logger.info("Début de l'exécution de la tâche: Urban Discovery.")
    generate_all_urban_recommendations()

def _execute_interaction_reflection_rec():
    logger.info("Début de l'exécution de la tâche: Interaction Reflection.")
    generate_all_reflection_recommendations()

def _execute_send_notifications():
    logger.info("Début de l'exécution de la tâche: Send Notifications.")
    from modules.notification_service import send_recommendation_notifications
    send_recommendation_notifications()

def _execute_full_process():
    logger.info("Début de l'exécution de la tâche: Full Process (Récupération, Calcul, Recommandations, Notifications).")

    _execute_fetch_raw_data()
    _execute_construct_user_profiles()
    _execute_calculate_metrics(category=None)
    _execute_initiation_integration_rec()
    _execute_engagement_promotion_rec()
    _execute_quality_improvement_rec()
    _execute_urban_discovery_rec()
    # _execute_send_notifications()

    logger.info("Tâche Full Process exécutée avec succès.")

def add_scheduled_jobs():
    """Ajoute ou met à jour les tâches planifiées en fonction des enregistrements dans la table `ScheduledTask`."""
    with current_app.app_context():
        tasks = ScheduledTask.query.all()
        for task in tasks:
            trigger = _get_task_trigger(task)
            if trigger:
                scheduler.add_job(
                    func=execute_task_function,
                    trigger=trigger,
                    id=str(task.id),
                    replace_existing=True,
                    args=[task.id]
                )

def _get_task_trigger(task):
    """Retourne le trigger APScheduler correspondant à une tâche planifiée."""
    if task.recurrence == 'daily':
        return IntervalTrigger(days=1, start_date=task.scheduled_time)
    elif task.recurrence == 'weekly':
        return IntervalTrigger(weeks=1, start_date=task.scheduled_time)
    elif task.recurrence == 'monthly':
        return CronTrigger(day='1', start_date=task.scheduled_time)
    elif task.recurrence == 'custom':
        return CronTrigger.from_crontab(task.recurrence, start_date=task.scheduled_time)
    else:
        return DateTrigger(run_date=task.scheduled_time)

def reload_scheduled_jobs():
    """Recharge toutes les tâches planifiées à partir de la base de données."""
    with current_app.app_context():
        scheduler.remove_all_jobs()
        add_scheduled_jobs()

def delete_all_scheduled_tasks():
    """Supprime toutes les tâches planifiées et les enregistrements correspondants de la base de données."""
    try:
        scheduler.remove_all_jobs()
        db.session.query(ScheduledTask).delete()
        db.session.commit()
        reload_scheduled_jobs()
        return {'success': True}
    except Exception as e:
        logger.error(f"Erreur lors de la suppression de toutes les tâches planifiées: {e}")
        return {'success': False}
