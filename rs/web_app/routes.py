import ipaddress
import json
import traceback

import requests

from flask import Blueprint, jsonify, render_template, redirect, url_for, request, session, flash
from modules.compute_metrics import calculate_metrics
from modules.db_operations import fetch_category_indicators, fetch_category_recommendations, get_distant_annotations, get_distant_messages, get_distant_tours, get_distant_users, save_annotations_to_local_database, save_messages_to_local_database, save_tours_to_local_database, save_users_to_local_database
from modules.notification_service import send_recommendation_notifications
from modules.preprocessing.preprocess_annotations import preprocess_annotations
from modules.recommendations.adoption_integration_rec import generate_adoption_integration_recommendations
from modules.recommendations.content_quality_rec import generate_content_quality_recommendations
from modules.recommendations.engagement_reengagement_rec import generate_all_engagement_recommendations
from modules.recommendations.interaction_reflection_rec import generate_all_reflection_recommendations
from modules.recommendations.urban_discovery_rec import generate_all_urban_recommendations
from modules.user_profile import calculate_users_profiles
from web_app.models import ExecutionLog, ScheduledTask
from web_app import db
from datetime import datetime
import pytz
from web_app.tasks import delete_all_scheduled_tasks, execute_task_function, add_scheduled_jobs, reload_scheduled_jobs
from modules.generate_recommendations import (
                verifier_initiation_integration_utilisateur,
                verifier_engagement_utilisateur,
                verifier_qualite_contributions,
                verifier_interaction_reflexion
            )

bp = Blueprint('web_app', __name__)

def get_or_create_log(action_type):
    """
    Récupère un enregistrement de journalisation existant pour l'action spécifiée, ou en crée un nouveau.
    """
    log = ExecutionLog.query.filter_by(action_type=action_type, status='in_progress').first()
    if not log:
        log = ExecutionLog(action_type=action_type, status='in_progress')
        db.session.add(log)
        db.session.commit()
    return log

def update_log(log, status, details=None):
    """
    Met à jour l'enregistrement de journalisation avec le statut et les détails spécifiés.
    """
    log.status = status
    log.details = details
    log.end_time = datetime.utcnow()
    db.session.commit()

def execute_task_by_action(task_action):
    """
    Exécute la tâche spécifiée par `task_action` et enregistre les logs d'exécution.
    """
    log = get_or_create_log(task_action)

    try:
        if task_action == 'prepare_data':
            print('Getting users data')
            users = get_distant_users()
            save_users_to_local_database(users)

            print('Getting and preprocessing annotation data')
            annotations = get_distant_annotations()
            preprocess_annotations(annotations)
            save_annotations_to_local_database(annotations)

            print('Getting tours data')
            tours = get_distant_tours()
            save_tours_to_local_database(tours)

            print('Getting messages data')
            messages = get_distant_messages()
            save_messages_to_local_database(messages)
            # Exécute les métriques pour toutes les catégories
            calculate_users_profiles() 

        elif task_action == 'fetch_raw_data':
            print('Getting users data')
            users = get_distant_users()
            save_users_to_local_database(users)

            print('Getting and preprocessing annotation data')
            annotations = get_distant_annotations()
            preprocess_annotations(annotations)
            save_annotations_to_local_database(annotations)

            print('Getting tours data')
            tours = get_distant_tours()
            save_tours_to_local_database(tours)

            print('Getting messages data')
            messages = get_distant_messages()
            save_messages_to_local_database(messages)
        
         # Exécute les métriques pour toutes les catégories
        elif task_action == 'construct_user_profiles':
            calculate_users_profiles() 
        
         # Exécute les métriques pour toutes les catégories
        elif task_action == 'calculate_metrics':
            calculate_metrics() 
        
        # Cas pour chaque catégorie spécifique
        elif task_action == 'calculate_adoption_metrics':
            calculate_metrics(category='adoption')
        
        elif task_action == 'calculate_engagement_metrics':
            calculate_metrics(category='engagement')
        
        elif task_action == 'calculate_quality_metrics':
            calculate_metrics(category='quality')
        
        elif task_action == 'calculate_urban_discovery_metrics':
            calculate_metrics(category='urbain')
        
        elif task_action == 'calculate_interaction_reflection_metrics':
            calculate_metrics(category='reflexion')
        
        elif task_action == 'initiation_integration':
            # verifier_initiation_integration_utilisateur()
            generate_adoption_integration_recommendations()
        elif task_action == 'generate_recommendations':
            generate_adoption_integration_recommendations()
            generate_all_engagement_recommendations()
            generate_content_quality_recommendations()
            generate_all_urban_recommendations()

        
        elif task_action == 'engagement_promotion':
            generate_all_engagement_recommendations()
        
        elif task_action == 'quality_improvement':
            generate_content_quality_recommendations()
        
        elif task_action == 'urban_discovery':
            generate_all_urban_recommendations()
        
        elif task_action == 'interaction_reflection':
            generate_all_reflection_recommendations()
        
        elif task_action == 'send_notifications':
            send_recommendation_notifications()

        elif task_action == 'full_process':        
            # 1. Récupération des données
            users = get_distant_users()
            save_users_to_local_database(users)
            annotations = get_distant_annotations()
            preprocess_annotations(annotations)
            save_annotations_to_local_database(annotations)

            # 2. Calcul des métriques
            calculate_metrics()

            # 3. Génération de recommandations
            
            generate_adoption_integration_recommendations()
            generate_all_engagement_recommendations()
            generate_content_quality_recommendations()
            generate_all_urban_recommendations()

            # 4. Envoi de notifications
            send_recommendation_notifications()
        
        else:
            update_log(log, status='failed', details='Unknown task action.')
            return jsonify({'success': False, 'message': 'Unknown task action.'}), 400

        update_log(log, status='success')
        return jsonify({'success': True, 'message': 'Task executed successfully.'})
    
    except Exception as e:
        update_log(log, status='failed', details=str(e))
        print("Une erreur est survenue :")
        print(str(e))
        print("Détails de l'erreur :")
        traceback.print_exc()
        return jsonify({'success': False, 'message': str(e)}), 500


# Définir le nombre maximal de tentatives
MAX_ATTEMPTS = 3
REDIRECT_URL = 'https://mobiles-projet.huma-num.fr/' 
API_KEY = "248b457a71fcd0"
# Liste des plages IP autorisées (plages internes et privées)
ALLOWED_IP_RANGES = [
    ipaddress.ip_network('127.0.0.0/8'),  # Plage privée
    ipaddress.ip_network('10.0.0.0/8'),  # Plage privée
    ipaddress.ip_network('192.168.0.0/16'),  # Plage privée
    ipaddress.ip_network('172.16.0.0/12')  # Plage privée
]
# Fonction pour vérifier si l'adresse IP est dans les plages autorisées
def is_ip_allowed(ip):
    ip_obj = ipaddress.ip_address(ip)
    return any(ip_obj in range_ for range_ in ALLOWED_IP_RANGES)

def check_access(ip_address):
    """
    Vérifie si l'adresse IP est autorisée.
    """
    print('tentative de connexion depuis: ', ip_address)
    return is_ip_allowed(ip_address)

def full_check_access(ip_address):
    """
    Vérifie si l'adresse IP est autorisée.
    """
    response = requests.get(f'https://ipinfo.io/{ip_address}/json?token={API_KEY}')
    data = response.json()
    
    # Vérifiez le pays
    country_code = data.get('country')
    print('tentative de connexion depuis: ', data)
    return country_code == 'FR' or is_ip_allowed(ip_address)

@bp.route('/login',  methods=['GET', 'POST'])
def login():
    # Obtenez l'adresse IP de l'utilisateur
    ip_address = request.remote_addr
    # Vérifiez si l'IP est en France
    if not check_access(ip_address):
        flash('L\'accès est restreint aux membres du projet.', 'error')
        print('Accès refusée pour :', ip_address)
        return redirect(url_for('web_app.show_login'))
    code = request.form.get('code')
    
    # Vérifier si le code est correct (remplacer par la logique de validation réelle)
    if code == 'mobilespassword':  # Remplacez par la logique de validation réelle
        session['authenticated'] = True
        session.pop('attempts', None)  # Réinitialiser le compteur de tentatives
        print('Accès autorisé pour :', ip_address)
        return redirect(url_for('web_app.dashboard'))
    else:
        print('Tentative avec code erroné depuis :', ip_address,'. Code utilisé: ',code)
    # Gérer les tentatives échouées
    attempts = session.get('attempts', 0) + 1
    session['attempts'] = attempts
    
    if attempts >= MAX_ATTEMPTS:
        # Rediriger vers une URL spécifique après 3 tentatives échouées
        print('Trop de tentatives (',attempts,') avec code erroné depuis :', ip_address,'. Code utilisé: ',code)
        return redirect(REDIRECT_URL)
    
    flash('Code secret incorrect. Veuillez réessayer.', 'error')
    return redirect(url_for('web_app.show_login'))

@bp.route('/show_login')
def show_login():
    return render_template('login.html')

@bp.route('/')
def dashboard():
    if not session.get('authenticated'):
        return redirect(url_for('web_app.login'))  # Redirige vers la page de connexion si non authentifié
    
    """
    Route pour afficher le tableau de bord des tâches planifiées.
    """
    tasks = ScheduledTask.query.all()
    task_statuses = {
        'fetch_raw_data': {'status': 'Not Executed', 'last_run': 'N/A'},
        'construct_user_profiles': {'status': 'Not Executed', 'last_run': 'N/A'},
        'calculate_metrics': {'status': 'Not Executed', 'last_run': 'N/A'},
        'initiation_integration': {'status': 'Not Executed', 'last_run': 'N/A'},
        'engagement_promotion': {'status': 'Not Executed', 'last_run': 'N/A'},
        'quality_improvement': {'status': 'Not Executed', 'last_run': 'N/A'},
        'urban_discovery': {'status': 'Not Executed', 'last_run': 'N/A'},
        'interaction_reflection': {'status': 'Not Executed', 'last_run': 'N/A'},
        'send_notifications': {'status': 'Not Executed', 'last_run': 'N/A'},
    }

    # Mise à jour avec les données réelles de la base de données
    for task in tasks:
        if task.action in task_statuses:
            task_statuses[task.action]['status'] = task.status or 'Not Executed'
            task_statuses[task.action]['last_run'] = task.last_run or 'N/A'

    return render_template('dashboard.html', tasks=tasks, **task_statuses)

@bp.route('/metric_details/<task_id>', methods=['GET'])
def details(task_id):
    # Récupérer les données en fonction de task_id
    category = ''
    if task_id =='calculate_adoption_metrics':
        category = "CAT1-Adoption_Integration"
    elif task_id =='calculate_engagement_metrics':
        category = "CAT2-Engagement"
    elif task_id =='calculate_quality_metrics':
        category = "Qualité du contenu"
    elif task_id =='calculate_urban_discovery_metrics':
        category = "Découverte Urbaine"
    elif task_id =='calculate_interaction_reflection_metrics':
        category = "Interaction et réflexion"
    print(category)
    data_df = fetch_category_indicators(category)
    
    data_df = data_df[['user_id','type', 'value','date']]  
    
    data = data_df.to_dict(orient='records')

    types = data_df['type'].unique().tolist()

    users = data_df['user_id'].unique().tolist()

    if not data:
        return jsonify({'message': 'Aucun détail à afficher pour cette tâche.'}), 404

    # Afficher le template avec les données
    return render_template('action_details.html', task_id=task_id, data=data, types=types, users=users)
    

@bp.route('/recos_details/<task_id>', methods=['GET'])
def reco_details(task_id):
    # Récupérer les données en fonction de task_id
    category = ''
    if task_id =='adoption_metrics':
        category = "Adoption et Intégration"
    elif task_id =='engagement_metrics':
        category = "Engagement"
    elif task_id =='quality_metrics':
        category = "Qualité du contenu"
    elif task_id =='urban_discovery_metrics':
        category = "Découverte Urbaine"
    elif task_id =='interaction_reflection_metrics':
        category = "Interaction et réflexion"

    category = "Engagement"

    data_df = fetch_category_recommendations(category)
    print('data: ', data_df)
    
    data_df = data_df[['user_id','category', 'strategy', 'recommendation' ,'created_at']]  
    
    data = data_df.to_dict(orient='records')

    types = data_df['strategy'].unique().tolist()

    users = data_df['user_id'].unique().tolist()

    if not data:
        return jsonify({'message': 'Aucun détail à afficher pour cette tâche.'}), 404

    # Afficher le template avec les données
    return render_template('reco_details.html', task_id=task_id, data=data, types=types, users=users)

# Ajouter une route pour la déconnexion
@bp.route('/logout')
def logout():
    session.pop('authenticated', None)
    flash('Vous avez été déconnecté.', 'info')
    return redirect(url_for('web_app.login'))


@bp.route('/execute_task/<string:task_action>', methods=['POST'])
def execute_task(task_action):
    """
    Route pour exécuter une tâche spécifique.
    """
    print('Action to execute: ', task_action)
    return execute_task_by_action(task_action)

@bp.route('/execute_task_now/<int:task_id>')
def execute_task_now(task_id):
    """
    Route pour exécuter une tâche planifiée immédiatement.
    """
    task = ScheduledTask.query.get_or_404(task_id)
    execute_task_function(task.action)
    task.status = 'Executed'
    task.last_run = datetime.now(pytz.utc)
    db.session.commit()
    return redirect(url_for('web_app.dashboard'))

@bp.route('/delete_task/<int:task_id>')
def delete_task(task_id):
    """
    Route pour supprimer une tâche planifiée.
    """
    task = ScheduledTask.query.get_or_404(task_id)
    db.session.delete(task)
    db.session.commit()
    return redirect(url_for('web_app.dashboard'))

@bp.route('/edit_task/<int:task_id>', methods=['GET', 'POST'])
def edit_task(task_id):
    """
    Route pour éditer une tâche planifiée.
    """
    task = ScheduledTask.query.get_or_404(task_id)
    
    if request.method == 'POST':
        task.action = request.form['task_action']
        task.scheduled_time = datetime.fromisoformat(request.form['scheduled_time'])
        task.recurrence = request.form.get('recurrence', '')
        custom_recurrence = request.form.get('custom_recurrence', '')
        
        task.recurrence = custom_recurrence if task.recurrence == 'custom' else task.recurrence
        
        db.session.commit()
        return redirect(url_for('web_app.dashboard'))

    return render_template('edit_task.html', task=task)


@bp.route('/schedule_task', methods=['POST'])
def schedule_task():
    """
    Route pour planifier ou modifier une tâche planifiée.
    """
    task_name = request.form.get('task_name')
    task_action = request.form['task_action']
    task_id = request.form.get('task_id')
    form_mode = request.form['form_mode']
    scheduled_time = datetime.fromisoformat(request.form['scheduled_time'])
    creation_time = datetime.utcnow()
    recurrence = request.form.get('recurrence', '')
    custom_recurrence = request.form.get('custom_recurrence', '')

    if form_mode == 'edit' and task_id:
        task = ScheduledTask.query.get_or_404(task_id)
        task.action = task_action
        task.name = task_name
        task.scheduled_time = scheduled_time
        task.recurrence = recurrence if recurrence != 'custom' else custom_recurrence
        db.session.commit()
    elif form_mode == 'create':
        task = ScheduledTask(
            name=task_name,
            action=task_action,
            creation_time=creation_time,
            scheduled_time=scheduled_time,
            recurrence=recurrence if recurrence != 'custom' else custom_recurrence
        )
        db.session.add(task)
        db.session.commit()
    
    reload_scheduled_jobs()
    return redirect(url_for('web_app.dashboard'))

@bp.route('/delete_all_scheduled_tasks', methods=['POST'])
def delete_all_scheduled_tasks_route():
    """
    Route pour supprimer toutes les tâches planifiées.
    """
    result = delete_all_scheduled_tasks()
    reload_scheduled_jobs()
    # return redirect(url_for('web_app.dashboard'))
    return jsonify(result)

