import os

class Config:
    APP_PASSKEY=os.getenv('APP_PASSKEY', 'mobiles')
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}'.format(        
        DB_USERNAME=os.getenv('DB_USERNAME', 'mobiles'),
        DB_PASSWORD=os.getenv('DB_PASSWORD', 'mobilespassword'),
        DB_HOST=os.getenv('DB_HOST', 'db'),
        DB_NAME=os.getenv('DB_NAME', 'mobiles')
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ELASTIC_URI = {
        'host': os.getenv('ELASTIC_HOST', 'es'),
        'port': int(os.getenv('ELASTIC_PORT', '9200')),
        'scheme': os.getenv('ELASTIC_SCHEME', 'http'),
        'username': os.getenv('ELASTIC_USERNAME', 'elastic'),
        'password': os.getenv('ELASTIC_PASSWORD', 'mobilespassword')
    }

    MOBILES_API_URL = "DB-URL/api/"
